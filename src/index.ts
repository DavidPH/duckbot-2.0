import { ScheduledTaskRedisStrategy } from "@sapphire/plugin-scheduled-tasks/register-redis";
import { container, LogLevel, SapphireClient } from "@sapphire/framework";
import type { SlashCommandBuilder, SlashCommandSubcommandsOnlyBuilder } from "@discordjs/builders";
import type { Jail } from "./lib/utils/jail-utils";
import type { AsyncQueue } from "@sapphire/async-queue";
import { getEnv, bot_settings } from "./lib/utils/load-env";
import createDB from "./lib/postgres/CreateDB";
import "@sapphire/plugin-logger/register";
import postgres from "postgres";
import prexit from "prexit";
import Redis from "ioredis";
import fs from "fs";

class duckBot extends SapphireClient {
	constructor() {
		container.env = getEnv();
		container.redis = new Redis(process.env.REDIS_URL as string);
		container.db = postgres(process.env.DB_URL as string, {
			onnotice: (n) => {
				// Don't care about "relation ___ already exists, skipping" notice when using createDB()
				if (n.code !== "42P07") {
					container.logger.debug(n);
				}
			}
		});

		prexit(async () => {
			container.logger.info("Shutting down");
			this.destroy();
			await container.db.end({ timeout: 5 });
			await container.redis.quit();
		});

		super({
			intents: ["GUILDS", "GUILD_MESSAGES", "DIRECT_MESSAGES", "GUILD_MEMBERS", "GUILD_EMOJIS_AND_STICKERS"],
			partials: ["CHANNEL"],
			shards: "auto",
			loadDefaultErrorListeners: false,
			logger: { level: container.env.isDev ? LogLevel.Debug : LogLevel.Info },
			loadMessageCommandListeners: false,
			preventFailedToFetchLogForGuilds: true,
			tasks: {
				strategy: new ScheduledTaskRedisStrategy({
					bull: {
						connection: {
							host: container.redis.options.host,
							username: container.redis.options.username,
							port: container.redis.options.port,
							password: container.redis.options.password
						},
						defaultJobOptions: {
							removeOnComplete: true
						}
					}
				})
			}
		});
	}

	public override async login(token?: string) {
		container.signup_queue = {};
		container.emojis = JSON.parse(fs.readFileSync("resources/emojis.json", "utf-8"));
		await createDB();
		return super.login(token);
	}
}

declare module "@sapphire/framework" {
	interface Command {
		getCommandData?(): SlashCommandSubcommandsOnlyBuilder | SlashCommandBuilder;
	}
}

declare module "@sapphire/pieces" {
	interface Container {
		redis: Redis;
		db: ReturnType<typeof postgres>;
		jail: Jail;
		signup_queue: { [key: string]: [AsyncQueue, NodeJS.Timeout] | undefined };
		emojis: { [key: string]: string };
		env: bot_settings;
	}
}

process.on("unhandledRejection", (reason, p) => {
	container.logger.error("Possibly Unhandled Rejection at: Promise ", p, " reason: ", reason);
});

const client = new duckBot();
void client.login();
