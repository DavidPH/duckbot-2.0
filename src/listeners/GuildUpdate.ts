import { Listener, Events, PieceContext } from "@sapphire/framework";
import type { Guild } from "discord.js";

export class guildCreate extends Listener<typeof Events.GuildCreate> {
	constructor(context: PieceContext) {
		super(context, { event: Events.GuildCreate, name: "DBGuildUpdate" });
	}

	public async run(guild: Guild) {
		await this.container.db`
        INSERT INTO servers ${this.container.db({ server_id: guild.id, server_name: guild.name })}
        ON CONFLICT (server_id) DO UPDATE SET server_name=${guild.name}`;
	}
}

export class guildDelete extends Listener<typeof Events.GuildDelete> {
	constructor(context: PieceContext) {
		super(context, { event: Events.GuildDelete, name: "DBGuildDelete" });
	}

	public async run(guild: Guild) {
		await this.container.db`
        DELETE FROM servers WHERE server_id = ${guild.id};`;
	}
}
