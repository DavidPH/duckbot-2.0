import { Listener, Events, PieceContext } from "@sapphire/framework";
import type { jailPayload, jailSettingsPayload } from "../lib/utils/jail-utils";
import type { GuildMember } from "discord.js";

export class guildCreate extends Listener<typeof Events.GuildMemberAdd> {
	constructor(context: PieceContext) {
		super(context, { event: Events.GuildMemberAdd });
	}

	public async run(member: GuildMember) {
		const [settings] = await this.container.db<jailSettingsPayload[]>`SELECT * FROM jail_settings WHERE server_id = ${member.guild.id}`;
		if (!settings?.enforce) return;
		const query = await this.container.db<jailPayload[]>`SELECT * FROM jail WHERE server_id = ${member.guild.id} AND member_id = ${member.id}`;
		if (query.count > 0) {
			const [payload] = query;
			const roles = await this.container.jail.getSetup(member.guild.id);
			if (roles) {
				payload.role_list.push(member.guild.roles.everyone.id);
				for (const [mRole, jRole] of roles) {
					if (payload.role_list.includes(mRole.id)) {
						try {
							await member.roles.add(jRole);
						} catch {}
						break;
					}
				}
			}
		}
	}
}
