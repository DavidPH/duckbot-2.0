import { Listener, PieceContext } from "@sapphire/framework";
import { Jail } from "../lib/utils/jail-utils";

export class Ready extends Listener {
	constructor(context: PieceContext) {
		super(context, { once: true });
	}

	public async run() {
		const guilds = this.container.client.guilds.cache.map((g) => {
			return { server_id: g.id, server_name: g.name };
		});
		await this.container.db`
		INSERT INTO servers ${this.container.db(guilds)}
		ON CONFLICT (server_id) DO UPDATE SET server_name = EXCLUDED.server_name;`;

		this.container.jail = new Jail();
		this.container.client.user!.setPresence({
			status: this.container.env.isDev ? "dnd" : "online",
			activities: [{ type: "WATCHING", name: "over you." }]
		});
	}
}
