import { AutocompleteInteractionPayload, Events, Listener, PieceContext } from "@sapphire/framework";

export class CoreEvent extends Listener<typeof Events.CommandAutocompleteInteractionError> {
	constructor(context: PieceContext) {
		super(context, { event: Events.CommandAutocompleteInteractionError });
	}

	public run(error: unknown, context: AutocompleteInteractionPayload) {
		const { name, location } = context.command;
		this.container.logger.error(
			`Encountered error while handling an autocomplete run method on command "${name}" at path "${location.full}"`,
			error
		);
	}
}
