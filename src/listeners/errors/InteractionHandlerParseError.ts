import { InteractionHandlerParseError as InteractionHandlerParseErrorPayload, Events, Listener, PieceContext } from "@sapphire/framework";
import { reportError } from "../../lib/utils/errors";

export class CoreEvent extends Listener<typeof Events.InteractionHandlerParseError> {
	constructor(context: PieceContext) {
		super(context, { event: Events.InteractionHandlerParseError });
	}

	public async run(error: unknown, context: InteractionHandlerParseErrorPayload) {
		const { name, location } = context.handler;
		if (await reportError(error, context.interaction)) {
			this.container.logger.error(
				`Encountered error while handling an interaction handler parse method for interaction-handler "${name}" at path "${location.full}"`,
				error
			);
		}
	}
}
