import { Command, Events, Listener, PieceContext } from "@sapphire/framework";

export class CoreEvent extends Listener<typeof Events.CommandApplicationCommandRegistryError> {
	constructor(context: PieceContext) {
		super(context, { event: Events.CommandApplicationCommandRegistryError });
	}

	public run(error: unknown, command: Command) {
		const { name, location } = command;
		this.container.logger.error(
			`Encountered error while handling the command application command registry for command "${name}" at path "${location.full}"`,
			error
		);
	}
}
