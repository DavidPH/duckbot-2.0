import { ContextMenuCommandErrorPayload, Events, Listener, PieceContext } from "@sapphire/framework";
import { reportError } from "../../lib/utils/errors";

export class CoreEvent extends Listener<typeof Events.ContextMenuCommandError> {
	constructor(context: PieceContext) {
		super(context, { event: Events.ContextMenuCommandError });
	}

	public async run(error: unknown, context: ContextMenuCommandErrorPayload) {
		const { name, location } = context.command;
		if (await reportError(error, context.interaction)) {
			this.container.logger.error(`Encountered error on message command "${name}" at path "${location.full}"`, error);
		}
	}
}
