import { ChatInputCommandErrorPayload, Events, Listener, PieceContext } from "@sapphire/framework";
import { reportError } from "../../lib/utils/errors";

export class CoreEvent extends Listener<typeof Events.ChatInputCommandError> {
	constructor(context: PieceContext) {
		super(context, { event: Events.ChatInputCommandError });
	}

	public async run(error: unknown, context: ChatInputCommandErrorPayload) {
		const { name, location } = context.command;
		if (await reportError(error, context.interaction)) {
			this.container.logger.error(`Encountered error on chat input command "${name}" at path "${location.full}"`, error);
		}
	}
}
