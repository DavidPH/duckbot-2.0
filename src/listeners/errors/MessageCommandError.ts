import { Events, Listener, MessageCommandErrorPayload, PieceContext } from "@sapphire/framework";

export class CoreEvent extends Listener<typeof Events.MessageCommandError> {
	constructor(context: PieceContext) {
		super(context, { event: Events.MessageCommandError });
	}

	public run(error: unknown, context: MessageCommandErrorPayload) {
		const { name, location } = context.command;
		this.container.logger.error(`Encountered error on message command "${name}" at path "${location.full}"`, error);
	}
}
