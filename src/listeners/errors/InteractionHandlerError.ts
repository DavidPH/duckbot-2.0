import { Events, InteractionHandlerError, Listener, PieceContext } from "@sapphire/framework";
import { reportError } from "../../lib/utils/errors";

export class CoreEvent extends Listener<typeof Events.InteractionHandlerError> {
	constructor(context: PieceContext) {
		super(context, { event: Events.InteractionHandlerError });
	}

	public async run(error: unknown, context: InteractionHandlerError) {
		const { name, location } = context.handler;
		if (await reportError(error, context.interaction)) {
			this.container.logger.error(
				`Encountered error while handling an interaction handler run method for interaction-handler "${name}" at path "${location.full}"`,
				error
			);
		}
	}
}
