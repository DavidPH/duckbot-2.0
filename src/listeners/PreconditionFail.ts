import {
	ChatInputCommandDeniedPayload,
	container,
	ContextMenuCommandDeniedPayload,
	Events,
	Listener,
	PieceContext,
	UserError
} from "@sapphire/framework";
import { supportLinkRow } from "../lib/utils/errors";

export class ChatInputCommandDenied extends Listener<typeof Events.ChatInputCommandDenied> {
	constructor(context: PieceContext) {
		super(context, { event: Events.ChatInputCommandDenied, name: "ChatInputDenied" });
	}

	public override run({ message }: UserError, { interaction }: ChatInputCommandDeniedPayload) {
		const errorMessage = `This feature is locked behind beta.
To opt in you must have administrator permissions and type "<@${container.client.user?.id}> togglebeta"`;
		if (message === "beta") {
			return interaction.reply({
				ephemeral: true,
				content: errorMessage,
				components: [supportLinkRow]
			});
		}
	}
}

export class ContextMenuCommandDenied extends Listener<typeof Events.ContextMenuCommandDenied> {
	constructor(context: PieceContext) {
		super(context, { event: Events.ContextMenuCommandDenied, name: "ContextMenuDenied" });
	}

	public override run({ message }: UserError, { interaction }: ContextMenuCommandDeniedPayload) {
		const errorMessage = `This feature is locked behind beta.
To opt in you must have administrator permissions and type "<@${container.client.user?.id}> togglebeta"`;
		if (message === "beta") {
			return interaction.reply({
				ephemeral: true,
				content: errorMessage,
				components: [supportLinkRow]
			});
		}
	}
}
