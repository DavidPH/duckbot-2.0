import type { PieceContext } from "@sapphire/framework";
import { ScheduledTask } from "@sapphire/plugin-scheduled-tasks";
import { DuckBotError } from "../lib/utils/errors";
import type { jailPayload } from "../lib/utils/jail-utils";

export class IntervalTask extends ScheduledTask {
	constructor(context: PieceContext) {
		super(context, { interval: 2_000 });
	}

	public async run() {
		if (!this.container.client.isReady() || this.container.jail === undefined) return;
		const rows = await this.container.db<jailPayload[]>`SELECT * FROM jail WHERE NOW() >= release_at`;
		if (rows.count <= 0) return;
		for (const row of rows) {
			try {
				const released = await this.container.jail.freeFromJail(this.container.client.user, row);
				if (released instanceof DuckBotError) {
					this.container.logger.debug(`DuckBotError: ${released.ERROR_CODE}`);
				} else if (released) {
					this.container.logger.debug(`Freed ${released.member_id} from jail.`);
				}
			} catch (e) {
				this.container.logger.error(`Error when trying to auto-free ${row?.member_id} `, e);
			}
		}
	}
}
