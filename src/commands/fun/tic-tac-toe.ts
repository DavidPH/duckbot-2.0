import { Command, CommandOptionsRunTypeEnum, CommandOptions } from "@sapphire/framework";
import { isGuildMember } from "@sapphire/discord.js-utilities";
import { ApplyOptions } from "@sapphire/decorators";
import { MessageActionRow, MessageButton } from "discord.js";
import { SlashCommandBuilder } from "@discordjs/builders";

@ApplyOptions<CommandOptions>({
	name: "tic-tac-toe",
	runIn: CommandOptionsRunTypeEnum.GuildAny
})
export class TicTacToeCommand extends Command {
	public getCommandData() {
		return new SlashCommandBuilder()
			.setName("tic-tac-toe")
			.setDescription("Play a game of tic-tac-toe!")
			.addUserOption((o) => o.setName("challenge").setDescription("The player to challenge.").setRequired(true))
			.addStringOption((o) =>
				o.setName("grid-size").setDescription("The size of the board").setChoices({ name: "3x3", value: "3" }, { name: "5x5", value: "5" })
			)
			.setDMPermission(false);
	}

	public async chatInputRun(interaction: Command.ChatInputInteraction) {
		const challenged = interaction.options.getMember("challenge", true);
		if (!isGuildMember(challenged)) return;
		const rows: MessageActionRow[] = [];
		const n = interaction.options.getString("grid-size") === "5" ? 5 : 3;
		for (let i = 0; i < n * n; i++) {
			if (i % n === 0) rows.push(new MessageActionRow());
			rows[rows.length - 1].addComponents([
				new MessageButton({
					customId: `ttt;${interaction.user.id};${challenged.id};${i};x`,
					label: "\u200b",
					style: "SECONDARY"
				})
			]);
		}
		await interaction.reply({
			content: `${interaction.user.toString()} challenges ${challenged.toString()} to a game of tic-tac-toe!`,
			components: rows
		});
	}
}
