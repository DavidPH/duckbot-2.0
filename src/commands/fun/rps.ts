import { Command, CommandOptionsRunTypeEnum, CommandOptions } from "@sapphire/framework";
import { isGuildMember } from "@sapphire/discord.js-utilities";
import { SlashCommandBuilder } from "@discordjs/builders";
import { ApplyOptions } from "@sapphire/decorators";
import Embed from "../../lib/utils/custom-embed";
import { MessageActionRow, MessageButton } from "discord.js";

@ApplyOptions<CommandOptions>({
	name: "rps",
	runIn: CommandOptionsRunTypeEnum.GuildAny
})
export default class RpsCommand extends Command {
	public getCommandData() {
		return new SlashCommandBuilder()
			.setName("rps")
			.addUserOption((o) => o.setName("challenge").setDescription("The user you want to challenge").setRequired(true))
			.setDescription("Play rock, paper, scissors with someone!");
	}

	public async chatInputRun(interaction: Command.ChatInputInteraction) {
		const challenged = interaction.options.getMember("challenge", true);
		if (!isGuildMember(challenged)) return;
		const { member } = interaction;
		if (!isGuildMember(member)) return;
		if (challenged.id === member.id) {
			return interaction.reply({
				content: `${interaction.user.toString()} challenges ${challenged.toString()} to a game of rock, paper, scissors... and wins!`
			});
		}
		if (challenged.id === this.container.client.id) {
			await interaction.reply({
				content: `${interaction.user.toString()} challenges ${challenged.toString()} to a game of rock, paper, scissors!`
			});
			return setTimeout(() => interaction.followUp({ content: "I pick 🔫 and win!" }), 300);
		}

		const row = new MessageActionRow({
			components: [
				new MessageButton({ customId: `rps;${member.id}x;${challenged.id}x;0`, emoji: "✊", style: "SECONDARY" }),
				new MessageButton({ customId: `rps;${member.id}x;${challenged.id}x;1`, emoji: "✋", style: "SECONDARY" }),
				new MessageButton({ customId: `rps;${member.id}x;${challenged.id}x;2`, emoji: "✌", style: "SECONDARY" })
			]
		});

		const embed = new Embed();
		embed
			.setTitle(`${member.displayName} vs ${challenged.displayName}`)
			.addFields(
				Embed.field(member.displayName, "Choosing...", true),
				Embed.emptyField(true),
				Embed.field(challenged.displayName, "Choosing...", true),
				Embed.field("Match result: ", "<a:loading:729059787093835857> Pending...")
			);

		await interaction.reply({
			content: `${interaction.user.toString()} challenges ${challenged.toString()} to a game of rock, paper, scissors!`,
			embeds: [embed],
			components: [row]
		});
	}
}
