import { isGuildMember, isTextChannel, isMessageInstance } from "@sapphire/discord.js-utilities";
import { Command, CommandOptionsRunTypeEnum, CommandOptions } from "@sapphire/framework";
import { fetch, FetchResultTypes } from "@sapphire/fetch";
import { SlashCommandBuilder, SlashCommandSubcommandsOnlyBuilder } from "@discordjs/builders";
import { ApplyOptions } from "@sapphire/decorators";
import {
	ApplicationCommandOptionChoiceData,
	ButtonInteraction,
	Collection,
	InteractionCollector,
	Message,
	MessageActionRow,
	MessageButton,
	MessageCollector,
	Permissions,
	TextBasedChannel
} from "discord.js";
import _includes from "lodash/includes";
import _shuffle from "lodash/shuffle";
import Embed from "../../lib/utils/custom-embed";
import { GeneralErrors } from "../../lib/utils/errors";

const enum Time {
	minutes = 60,
	hours = 60 * 60
}

interface triviaCategory {
	id: number;
	name: string;
}

interface questionCount {
	total_num_of_questions: number;
	total_num_of_pending_questions: number;
	total_num_of_verified_questions: number;
	total_num_of_rejected_questions: number;
}

interface totalQuestionCount {
	overall: questionCount;
	categories: { [key: number]: questionCount | undefined };
}

interface triviaArgs {
	name: string;
	categoryID: number | null;
	count: number;
	questionAmount: number;
	mode: "default" | "blind" | null;
	timeLimit: number;
}

interface triviaQuestion {
	category: string;
	type: "multiple" | "boolean";
	question: string;
	correct_answer: string;
	difficulty: "easy" | "medium" | "hard";
	incorrect_answers: Array<string>;
}

interface triviaCollectors {
	message?: MessageCollector;
	interaction?: InteractionCollector<ButtonInteraction>;
}

@ApplyOptions<CommandOptions>({
	name: "trivia",
	runIn: CommandOptionsRunTypeEnum.GuildAny
})
export class TriviaCommand extends Command {
	public getCommandData(): SlashCommandSubcommandsOnlyBuilder {
		return new SlashCommandBuilder()
			.setDMPermission(false)
			.setName("trivia")
			.setDescription("Play a game of trivia!")
			.addSubcommand((s) => s.setName("list-categories").setDescription("List the available trivia categories, and their question count."))
			.addSubcommand((s) =>
				s
					.setName("start")
					.setDescription("Start a round of trivia questions.")
					.addStringOption((o) =>
						o.setName("category").setDescription("Chose a category for the questions.").setAutocomplete(true).setRequired(true)
					)
					.addIntegerOption((o) =>
						o.setName("questions").setDescription("Number of questions in this round.").setMinValue(1).setMaxValue(20)
					)
					.addStringOption((o) =>
						o
							.setName("mode")
							.setDescription("Whether to play in multiple choice mode, or guessing mode.")
							.setChoices({ name: "default", value: "default" }, { name: "Blind Mode (experimental)", value: "blind" })
					)
					.addIntegerOption((o) =>
						o.setName("time-limit").setDescription("Amount of seconds alloted to each question.").setMinValue(2).setMaxValue(20)
					)
			);
	}

	public async autocompleteRun(interaction: Command.AutocompleteInteraction) {
		const curr_val = String(interaction.options.getFocused()).toLowerCase();
		const [categories, count] = await this.getTriviaCategories();
		categories.sort((a, b) => {
			return (count.categories[b.id]?.total_num_of_verified_questions ?? 0) - (count.categories[a.id]?.total_num_of_verified_questions ?? 0);
		});
		const options: ApplicationCommandOptionChoiceData[] = categories.map(({ name, id }) => {
			return { name: `${name} - ${count.categories[id]?.total_num_of_verified_questions}`, value: id.toString() };
		});
		options.unshift({ name: `All Categories - ${count.overall.total_num_of_verified_questions} questions`, value: "all" });
		await interaction.respond(options.filter((e) => _includes(e.name.toLowerCase(), curr_val))).catch(() => {});
	}

	public async chatInputRun(interaction: Command.ChatInputInteraction) {
		const subCommand = interaction.options.getSubcommand();
		if (subCommand === "list-categories") {
			await this.postCategories(interaction);
		} else if (subCommand === "start") {
			const key = `trivia:ongoing:${interaction.guildId ?? ""}${interaction.channelId}`;
			if ((await this.container.redis.get(key)) !== null) {
				return interaction.reply({
					content: "This channel already has an ongoing trivia.",
					ephemeral: true
				});
			}
			await this.container.redis.setex(key, Time.minutes * 10, "");
			try {
				await this.playTrivia(interaction);
			} catch (e) {
				await this.container.redis.del(key);
				throw e;
			}
			await this.container.redis.del(key);
		}
	}

	public async playTrivia(interaction: Command.ChatInputInteraction) {
		const { member, channel } = interaction;
		if (!isGuildMember(member) || !isTextChannel(channel)) return;
		if (!channel.permissionsFor(interaction.guild!.me!).has(["SEND_MESSAGES", "VIEW_CHANNEL", "READ_MESSAGE_HISTORY"])) {
			throw GeneralErrors.BOT_MISSING_PERM;
		}
		await interaction.deferReply();
		let token = (await this.getSessionToken(interaction)) ?? "";
		const args = await this.parseArgs(interaction);
		if (!args) return interaction.followUp({ content: "Invalid category selected." });
		const startTriviaButton = new MessageButton({ customId: `${interaction.id}startTrivia`, label: "Start Trivia", style: "SUCCESS" });
		const stopTriviaButton = new MessageButton({ customId: `${interaction.id}stopTrivia`, label: "Stop Trivia", style: "DANGER" });
		const embed = new Embed({
			title: args.name,
			description: `There are ${args.count} unique questions in this category!`,
			fields: [
				{
					name: "Instructions",
					value: `
					• I will ask ${args.questionAmount} questions, whoever gets the most points wins. 
					• You can answer either by typing an answer or choosing an option with the buttons.
					• You can only answer **once** per question.
					• You have up to ${args.timeLimit} seconds to answer each question, or 1 second after the first correct answer is chosen.
					• Answers after the first correct answer will award a diminished amount of points, depending on how close they were.
					• Blind mode will remove the buttons and won't show you the options.`
				}
			]
		});
		const currentCollectors: triviaCollectors = {};
		let stop = false;
		let triviaFinished = false;

		// Send instructions
		const startMsg = await interaction.followUp({
			fetchReply: true,
			embeds: [embed],
			components: [new MessageActionRow({ components: [startTriviaButton, stopTriviaButton] })]
		});

		if (!isMessageInstance(startMsg)) return;
		// Collector to check if trivia should stop early
		startMsg
			.awaitMessageComponent({
				componentType: "BUTTON",
				time: args.questionAmount * args.timeLimit * 1000,
				filter: ({ customId, user, member }) => {
					if (triviaFinished) return false;
					if (customId !== `${interaction.id}stopTrivia`) return false;
					if (isGuildMember(member)) {
						if (member.permissions.has(Permissions.FLAGS.ADMINISTRATOR)) return true;
					}
					return user.id === interaction.user.id;
				}
			})
			.then((i) => {
				stop = true;
				currentCollectors.interaction?.stop();
				currentCollectors.message?.stop();
				startMsg.edit({ components: [] }).catch(() => {});
				return i.reply({ content: `<@${i.user.id}> stopped the trivia.`, allowedMentions: { users: [] } });
			})
			.catch(() => {});
		// Check if user clicked the start button
		const proceed = await startMsg
			.awaitMessageComponent({ time: 15_000, componentType: "BUTTON", filter: (i) => i.user.id === member.user.id })
			.then(async (i) => {
				if (i.customId === `${interaction.id}startTrivia`) return i;
				await i.update({});
			})
			.catch(() => {});
		// Only proceed if user clicked the start button
		if (!proceed || stop) return interaction.editReply({ components: [] });
		embed.addField("\u200b", "<a:loading:729059787093835857> Fetching questions...");
		await proceed.update({ embeds: [embed], components: [new MessageActionRow({ components: [stopTriviaButton] })] });
		embed.spliceFields(-1, 1); // Remove the field we just added after updating the embed with a loading emote

		const requestURL = new URL("https://opentdb.com/api.php?encode=url3986");
		requestURL.searchParams.append("token", token);
		requestURL.searchParams.append("amount", args.questionAmount.toString());
		requestURL.searchParams.append("category", args.categoryID?.toString() ?? "");
		// Try twice in case token needs to be refreshed
		let tries = 2;
		let questions: Array<triviaQuestion> = [];
		while (tries--) {
			const query = await fetch<{ response_code: number; results: Array<triviaQuestion> }>(requestURL, FetchResultTypes.JSON);
			if (query.response_code === 0) {
				questions = query.results;
				break;
			} else if (query.response_code === 4 || query.response_code === 3) {
				token = (await this.getSessionToken(interaction, true)) ?? "";
				continue;
			} else if (query.response_code === 1) return interaction.followUp({ content: "Couldn't find enough questions." });
			else return interaction.followUp({ content: "Something went wrong when fetching questions. Try again later." });
		}
		if (questions.length <= 0) return;
		// Successfully fetched questions, remove the loading emote
		await proceed.editReply({ embeds: [embed] }).catch(() => {});
		// Emojis for letters A B C D
		const emojis = ["<:a_:997965281815179264>", "<:b_:997965283065081957>", "<:c_:997966523555651634>", "<:d_:997966525124317214>"];
		// Embed color based on the difficulty
		const difficulties: { [key: string]: `#${string}` } = { easy: "#90f542", medium: "#f5e942", hard: "#f59e42" };
		// Make the buttons to choose options
		const mcqButtons: MessageButton[] = emojis.map((_, i) => new MessageButton({ customId: i.toString(), emoji: emojis[i], style: "SECONDARY" }));
		// Collection to store the total scores
		let totalPoints = new Collection<string, number>();
		// Ask each question
		for (const [j, question] of questions.entries()) {
			if (stop) break;
			this.container.logger.debug(question);
			// Make question embed
			const embed = new Embed({
				title: decodeURIComponent(question.category),
				description: `**${decodeURIComponent(question.question)}**`
			})
				.setColor(difficulties[question.difficulty])
				.setFooter(`${j + 1}/${args.questionAmount}`);
			// Shuffle the options
			const allOptions = _shuffle([question.correct_answer, ...question.incorrect_answers]);
			if (args.mode !== "blind") embed.description += `\n${allOptions.map((o, i) => `${emojis[i]} - ${decodeURIComponent(o)}`).join("\n")}`;
			embed.addField("\u200b", "<a:loading:729059787093835857> Collecting answers");
			// Send embed
			const lastReply = await interaction.followUp({
				embeds: [embed],
				components: args.mode === "blind" ? undefined : [new MessageActionRow({ components: mcqButtons.slice(0, allOptions.length) })]
			});
			if (!isMessageInstance(lastReply)) return interaction.followUp({ content: "Something went wrong. " }).catch(() => {});
			// Collect answers
			const points = await this.collectAnswers(lastReply, channel, currentCollectors, allOptions, question.correct_answer, args.timeLimit);
			// Collector finished, get a sum of the total points to see if anyone answered correctly
			const total = points.reduce((acc, val) => acc + val, 0);
			totalPoints = totalPoints.merge(
				points,
				(x) => ({ keep: true, value: x }),
				(y) => ({ keep: true, value: y }),
				(x, y) => ({ keep: true, value: x + y })
			);
			// If not stopped post this round's answers and points
			if (!stop) {
				if (total === 0) {
					await lastReply
						.reply({
							content: `Time's up!\nThe correct answer was **\`${decodeURIComponent(question.correct_answer)}\`**`
						})
						.catch(() => {});
				} else {
					await lastReply
						.reply({ content: points.map((points, id) => `<@${id}>: ${points} points`).join("\n"), allowedMentions: { users: [] } })
						.catch(() => {});
				}
			}
			embed.setFields([{ name: "\u200b", value: `**Answer:** ||\`${decodeURIComponent(question.correct_answer)}\`||` }]);
			await lastReply.edit({ components: [], embeds: [embed] }).catch(() => {});
			if (j + 1 === questions.length) triviaFinished = true;
			// Wait a bit before posting next question
			if (!stop) await new Promise((r) => setTimeout(r, 1500));
		}
		const points = totalPoints.reduce((acc, val) => acc + val, 0);
		if (points === 0) {
			return interaction.followUp("No one got any questions right... how disappointing");
		}
		const trophies = ["🥇", "🥈", "🥉"];
		totalPoints.sort((a, b) => b - a);
		let i = 0;
		const results = totalPoints.map((points, userID) => {
			return `${trophies[i++] ?? ""} <@${userID}>: ${points} points`;
		});
		if (points) embed.addField("Winners:", results.slice(0, 2).join("\n"));
		await startMsg.edit({ embeds: [embed], components: [] });
		await interaction.followUp({ content: results.join("\n"), allowedMentions: { users: [] } });
	}

	public async collectAnswers(
		message: Message,
		channel: TextBasedChannel,
		collectors: triviaCollectors,
		allOptions: string[],
		correctAnswer: string,
		timeLimit: number
	): Promise<Collection<string, number>> {
		timeLimit *= 1000;
		const players = new Collection<string, number>();
		collectors.interaction = message.createMessageComponentCollector({
			filter: ({ user }) => !players.has(user.id),
			componentType: "BUTTON",
			time: timeLimit
		});
		collectors.message = channel.createMessageCollector({ filter: ({ author }) => !players.has(author.id), time: timeLimit });

		function givePoints(id: string) {
			if (players.size === 0) {
				players.set(id, 20);
			} else if (players.size === 1) {
				players.set(id, 10);
			} else {
				players.set(id, 5);
			}
			setTimeout(() => {
				collectors.message?.stop();
				collectors.interaction?.stop();
			}, 1200);
		}

		const cleanAnswer = decodeURIComponent(correctAnswer).toLocaleLowerCase().replace(/\W/g, "");

		collectors.message.on("collect", (msg) => {
			if (msg.content.toLocaleLowerCase().replace(/\W/g, "") === cleanAnswer) {
				givePoints(msg.author.id);
			}
		});

		collectors.message.on("end", () => collectors.interaction?.stop());

		collectors.interaction.on("collect", async (i) => {
			const id = parseInt(i.customId, 10);
			const correct = correctAnswer === allOptions[id];
			await i.reply({ content: `<@${i.user.id}> answered with **\`${decodeURIComponent(allOptions[id])}\`**`, allowedMentions: { users: [] } });
			if (correct) {
				givePoints(i.user.id);
			} else {
				players.set(i.user.id, 0);
			}
		});

		return new Promise((r) => {
			collectors.interaction?.on("end", () => {
				r(players);
			});
		});
	}

	public async postCategories(interaction: Command.ChatInputInteraction) {
		await interaction.deferReply();
		const embed = new Embed();
		const [categories, count] = await this.getTriviaCategories();
		categories.sort((a, b) => {
			return (count.categories[b.id]?.total_num_of_verified_questions ?? 0) - (count.categories[a.id]?.total_num_of_verified_questions ?? 0);
		});
		embed.addField(
			"[# of Questions] Trivia Categories",
			categories
				.slice(0, categories.length / 2 + 1)
				.map((c) => `**\`[${count.categories[c.id]?.total_num_of_verified_questions}]\`** \u200b ${c.name}`)
				.join("\n"),
			true
		);
		embed.addFields(Embed.emptyField(true));
		embed.addField(
			"\u200b",
			categories
				.slice(categories.length / 2 + 1)
				.map((c) => `**\`[${count.categories[c.id]?.total_num_of_verified_questions}]\`** \u200b ${c.name}`)
				.join("\n"),
			true
		);
		await interaction.followUp({ content: "\u200b", ephemeral: true, embeds: [embed] });
	}

	private async getTriviaCategories(): Promise<[triviaCategory[], totalQuestionCount]> {
		const questionCache = await this.container.redis.get("trivia:categories");
		const countCache = await this.container.redis.get("trivia:categories:count");
		let trivia_categories: Array<triviaCategory>;
		let totalQuestions: totalQuestionCount;
		if (questionCache === null || countCache === null) {
			this.container.logger.debug("Fetching Trivia categories");
			({ trivia_categories } = await fetch<{ trivia_categories: triviaCategory[] }>(
				"https://opentdb.com/api_category.php",
				FetchResultTypes.JSON
			));
			totalQuestions = await fetch<totalQuestionCount>("https://opentdb.com/api_count_global.php", FetchResultTypes.JSON);
			await this.container.redis.setex("trivia:categories", Time.hours * 6, JSON.stringify(trivia_categories));
			await this.container.redis.setex("trivia:categories:count", Time.hours * 6, JSON.stringify(totalQuestions));
		} else {
			trivia_categories = JSON.parse(questionCache);
			totalQuestions = JSON.parse(countCache);
		}
		return [trivia_categories, totalQuestions];
	}

	private async getSessionToken(interaction: Command.ChatInputInteraction, reset = false) {
		let token = await this.container.redis.getex(`trivia:token:${interaction.guild?.id}${interaction.channel?.id}`, "EX", Time.hours * 1.5);
		if (reset && token !== null) {
			await fetch(`https://opentdb.com/api_token.php?command=reset&token=${token}`);
		}
		if (token === null) {
			let response_code;
			({ response_code, token } = await fetch<{ response_code: number; token: string }>(
				"https://opentdb.com/api_token.php?command=request",
				FetchResultTypes.JSON
			));
			if (response_code !== 0) return null;
			await this.container.redis.setex(`trivia:token:${interaction.guild?.id}${interaction.channel?.id}`, Time.hours * 1.5, token);
		}
		return token;
	}

	private async parseArgs(interaction: Command.ChatInputInteraction): Promise<triviaArgs | null> {
		const all = interaction.options.getString("category", true) === "all";
		const categoryID = all ? null : parseInt(interaction.options.getString("category", true), 10);
		if (!all && isNaN(categoryID!)) return null;
		const [categories, count] = await this.getTriviaCategories();
		let name = "All Categories";
		let numberOfQuestions = count.overall.total_num_of_verified_questions;
		if (categoryID !== null) {
			numberOfQuestions = count.categories[categoryID]?.total_num_of_verified_questions ?? 0;
			if (!numberOfQuestions) return null;
			for (const category of categories) {
				if (category.id === categoryID) {
					name = category.name;
					break;
				}
			}
		}
		let mode = interaction.options.getString("mode");
		if (mode !== "default" && mode !== "blind") {
			mode = null;
		}
		return {
			categoryID,
			name,
			count: numberOfQuestions,
			questionAmount: interaction.options.getInteger("questions") ?? 20,
			timeLimit: interaction.options.getInteger("time-limit") ?? (mode === "blind" ? 15 : 5),
			mode
		};
	}
}
