import { Command, CommandOptionsRunTypeEnum, CommandOptions } from "@sapphire/framework";
import { isGuildMember } from "@sapphire/discord.js-utilities";
import { SlashCommandBuilder } from "@discordjs/builders";
import { ApplyOptions } from "@sapphire/decorators";
import _sample from "lodash/sample";

@ApplyOptions<CommandOptions>({
	name: "slots",
	runIn: CommandOptionsRunTypeEnum.GuildAny
})
export default class SlotsCommand extends Command {
	public getCommandData() {
		return new SlashCommandBuilder().setName("slots").setDescription("Try your luck!");
	}

	public async chatInputRun(interaction: Command.ChatInputInteraction) {
		const { member } = interaction;
		if (!isGuildMember(member)) return;

		await interaction.reply({ ephemeral: false, content: rollTheSlots(member.displayName) });
	}
}

function rollTheSlots(name: string): string {
	const f1 = _sample(fruits);
	const f2 = _sample(fruits);
	const f3 = _sample(fruits);
	let content = `**${name}** rolled the slots...
**[${f1} ${f2} ${f3}]**\n`;
	switch (new Set([f1, f2, f3]).size) {
		case 1:
			content += "and won! \uD83C\uDF89";
			break;
		case 2:
			content += "and almost won (2/3)";
			break;
		case 3:
			content += "and lost...";
			break;
	}
	return content;
}

const fruits = [
	"\uD83C\uDF4F",
	"\uD83C\uDF4E ",
	"\uD83C\uDF50",
	"\uD83C\uDF4A",
	"\uD83C\uDF4B",
	"\uD83C\uDF4C",
	"\uD83C\uDF49",
	"\uD83C\uDF47",
	"\uD83C\uDF53",
	"\uD83C\uDF48",
	"\uD83C\uDF52",
	"\uD83C\uDF51"
];
