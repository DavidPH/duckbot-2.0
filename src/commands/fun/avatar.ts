import { Command, CommandOptionsRunTypeEnum, CommandOptions } from "@sapphire/framework";
import { isGuildMember } from "@sapphire/discord.js-utilities";
import { SlashCommandBuilder } from "@discordjs/builders";
import { ApplyOptions } from "@sapphire/decorators";
import { AllowedImageFormat, AllowedImageSize, MessageAttachment } from "discord.js";

@ApplyOptions<CommandOptions>({
	name: "avatar",
	runIn: CommandOptionsRunTypeEnum.GuildAny
})
export default class AvatarCommand extends Command {
	public getCommandData() {
		return new SlashCommandBuilder()
			.setName("avatar")
			.addUserOption((o) => o.setName("user").setDescription("Display this user's avatar.").setRequired(false))
			.addStringOption((o) =>
				o
					.setName("format")
					.setDescription("The format of the image.")
					.addChoices(
						{ name: "png", value: "png" },
						{ name: "jpg", value: "jpg" },
						{ name: "webp", value: "webp" },
						{ name: "gif", value: "gif" }
					)
			)
			.addIntegerOption((o) =>
				o
					.setName("size")
					.setDescription("The size of the image. Can not make it larger than the original.")
					.addChoices(
						{ name: "16", value: 16 },
						{ name: "32", value: 32 },
						{ name: "64", value: 64 },
						{ name: "128", value: 128 },
						{ name: "256", value: 256 },
						{ name: "512", value: 512 },
						{ name: "1024", value: 1024 },
						{ name: "2048", value: 2048 },
						{ name: "4096", value: 4096 }
					)
			)
			.addBooleanOption((o) => o.setName("ephemeral").setDescription("If true, it will send a message that only you can see."))
			.setDescription("Show a user's avatar(s).");
	}

	public async chatInputRun(interaction: Command.ChatInputInteraction) {
		const member = interaction.options.getMember("user") ?? interaction.member;
		const ephemeral = interaction.options.getBoolean("ephemeral");
		const format = (interaction.options.getString("format") ?? "webp") as AllowedImageFormat;
		const size = (interaction.options.getInteger("size") ?? 4096) as AllowedImageSize;

		if (!isGuildMember(member)) return;
		const urls = [member.user.avatarURL({ format, size })];
		const files: MessageAttachment[] = [];
		for (const url of urls) {
			if (url !== null) {
				files.push(new MessageAttachment(url, `${member.displayName}.${format}`));
			}
		}
		await interaction.reply({ ephemeral: ephemeral ?? false, files });
	}
}
