import { ApplicationCommandRegistry, Command, RegisterBehavior, CommandOptionsRunTypeEnum, CommandOptions } from "@sapphire/framework";
import { MessageActionRow, MessageButton, Modal, TextInputComponent } from "discord.js";
import { isMessageInstance } from "@sapphire/discord.js-utilities";
import { SignupErrors } from "../../lib/utils/errors";
import { ApplyOptions } from "@sapphire/decorators";
import { AsyncQueue } from "@sapphire/async-queue";
import Signup from "../../lib/utils/signup-utils";
import _truncate from "lodash/truncate";
import { ContextMenuCommandBuilder } from "@discordjs/builders";
import { PermissionFlagsBits, ApplicationCommandType } from "discord-api-types/v10";

@ApplyOptions<CommandOptions>({
	runIn: CommandOptionsRunTypeEnum.GuildAny
})
export class cmAddCommand extends Command {
	public override registerApplicationCommands(registry: ApplicationCommandRegistry) {
		registry.registerContextMenuCommand(
			new ContextMenuCommandBuilder()
				.setType(ApplicationCommandType.Message)
				.setName("Modify Signup users")
				.setDMPermission(false)
				.setDefaultMemberPermissions(PermissionFlagsBits.ManageEvents),
			{
				idHints: ["985217971649642527", "995456102936346844"],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
	}

	public async contextMenuRun(interaction: Command.ContextMenuInteraction) {
		if (!interaction.isMessageContextMenu()) return;
		const guild = interaction.guild!;
		const channel = interaction.channel!;
		const message = interaction.targetMessage;
		if (message.author.id !== this.container.client.user!.id) {
			throw SignupErrors.NOT_A_SIGNUP;
		}
		const rows = await this.container.db<
			{ message_index: number; message_id: string; signup_id: number; signup_json: string; signup_expire: Date }[]
		>`SELECT DISTINCT ON (m.message_index) 
                message_index, message_id, s.*
                FROM signups as s LEFT JOIN signup_messages as m
                ON s.signup_id = (SELECT signup_id FROM signup_messages WHERE message_id = ${message.id} AND server_id = ${guild.id})
                WHERE m.signup_id = s.signup_id ORDER BY m.message_index ASC`;
		if (rows.length <= 0) {
			throw SignupErrors.NOT_A_SIGNUP;
		}
		const signup: Signup = Object.assign(new Signup(), JSON.parse(rows[0].signup_json));
		const msg_index = rows.find((r) => r.message_id === message.id)?.message_index;
		if (msg_index === undefined) {
			return;
		}
		const components = [];
		for (const [i, section] of signup.sections.entries()) {
			components.push(
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: `s;${i}`,
							label: _truncate(`#${i + 1} ${section.title}`, { length: 45 }),
							style: "SHORT",
							placeholder: "Comma separated list of usernames/user ids to toggle."
						})
					]
				})
			);
		}
		const modal = new Modal({
			customId: `addUsersModal${interaction.id}`,
			title: `Add/Remove users from message #${msg_index + 1}`,
			components
		});
		await interaction.showModal(modal);
		interaction
			.awaitModalSubmit({ filter: (i) => i.customId === modal.customId, time: 90_000 })
			.then(async (i) => {
				await i.deferReply({ ephemeral: true });
				const users: { s_index: number; users: string[] }[] = [];
				const re = new RegExp(/^(?<id>\d{17,19})$/);
				for (const [
					s_index,
					{
						components: [component]
					}
				] of i.components.entries()) {
					users.push({
						s_index,
						users: (
							await Promise.all(
								component.value.split(",").map(async (e) => {
									e = e.trim();
									if (e && !re.test(e)) {
										e = (await guild.members.search({ query: e })).at(0)?.user?.id ?? e;
									}
									return e;
								})
							)
						).filter((e) => e)
					});
				}
				const key = `${guild.id}${rows[0].signup_id}`;
				let queue = this.container.signup_queue[key];
				if (queue) {
					clearTimeout(queue[1]);
					queue[1] = setTimeout(() => delete this.container.signup_queue[key], 15 * 60_000);
				} else {
					this.container.signup_queue[key] = [new AsyncQueue(), setTimeout(() => delete this.container.signup_queue[key], 15 * 60_000)];
					queue = this.container.signup_queue[key]!;
				}
				await queue[0].wait();
				const [{ signup_json }] = await this.container.db<{ signup_json: string }[]>`
                    SELECT signup_json FROM signups WHERE signup_id = ${rows[0].signup_id} AND server_id = ${guild.id}`;
				const [signup, info, editedMessages] = await Signup.toggleFromSignup(signup_json, guild, msg_index, users, true);
				if (signup) {
					const embeds = await signup.getEmbeds(guild, rows[0].signup_expire);
					const confirmMSG = await i.followUp({
						fetchReply: true,
						ephemeral: true,
						content: info,
						components: [
							new MessageActionRow({
								components: [
									new MessageButton({ customId: "yes", style: "SUCCESS", label: "YES" }),
									new MessageButton({ customId: "no", style: "DANGER", label: "NO" })
								]
							})
						]
					});
					if (!isMessageInstance(confirmMSG)) return;
					const confirmed = await confirmMSG
						.awaitMessageComponent({ componentType: "BUTTON", time: 10_000 })
						.then(async (i) => {
							if (i.customId === "yes") {
								await i.update({
									content: info
										.replace("**Add:**", "**Adding:**")
										.replace("**Remove:**", "**Removing:**")
										.replace("**Update:**", "**Updating:**"),
									components: []
								});
								return true;
							}
							await i.update({ components: [] });
							return false;
						})
						.catch(async () => {
							await i.editReply({ components: [] });
							return false;
						});
					await this.container.db`UPDATE signups SET signup_json = ${this.container.db.json(JSON.stringify(signup))}
                        WHERE signup_id = ${rows[0].signup_id} AND server_id = ${guild.id}`;
					queue[0].shift();
					if (confirmed) {
						for (const j of editedMessages) {
							// This is sometimes slow
							try {
								channel.messages
									.fetch(rows[j].message_id)
									.then((msg) => {
										msg.edit({ embeds: [embeds[j]] }).catch(() => {});
									})
									.catch(async () => {
										await i.followUp({
											ephemeral: true,
											content: `Something went wrong fetching signup message \`${rows[j]?.message_id}\``
										});
									});
							} catch {}
						}
					}
				} else {
					queue[0].shift();
				}
			})
			.catch((e) => {
				this.container.logger.error(e);
			});
	}
}
