import { ApplicationCommandRegistry, Command, RegisterBehavior, CommandOptionsRunTypeEnum, CommandOptions } from "@sapphire/framework";
import { ApplyOptions } from "@sapphire/decorators";
import { isMessageInstance } from "@sapphire/discord.js-utilities";
import {
	ApplicationCommandOptionChoiceData,
	ButtonInteraction,
	CommandInteraction,
	Formatters,
	Guild,
	Message,
	MessageActionRow,
	MessageButton,
	MessageSelectMenu,
	Modal,
	PartialModalActionRow,
	SelectMenuInteraction,
	TextChannel,
	TextInputComponent
} from "discord.js";
import { Timestamp } from "@sapphire/time-utilities";
import Embed from "../../lib/utils/custom-embed";
import CustomSignup, { signup_message, signup_section } from "../../lib/utils/signup-utils";
import _merge from "lodash/merge";
import _upperFirst from "lodash/upperFirst";
import _includes from "lodash/includes";
import { GeneralErrors, SignupErrors } from "../../lib/utils/errors";
import { SlashCommandBuilder } from "@discordjs/builders";
import { PermissionFlagsBits } from "discord-api-types/v10";

@ApplyOptions<CommandOptions>({
	runIn: CommandOptionsRunTypeEnum.GuildText
})
export class SignupCommand extends Command {
	public override registerApplicationCommands(registry: ApplicationCommandRegistry) {
		registry.registerChatInputCommand(
			new SlashCommandBuilder()
				.setName("signup")
				.setDescription("Create or post a custom signup.")
				.setDefaultMemberPermissions(PermissionFlagsBits.ManageEvents)
				.setDMPermission(false)
				.addSubcommandGroup((g) =>
					g
						.setName("format")
						.setDescription("Create, edit, and remove signup formats.")
						.addSubcommand((s) => s.setName("create").setDescription("Opens the signup creator to create a new signup format."))
						.addSubcommand((s) => s.setName("edit").setDescription("View, edit, or delete a saved format."))
				)
				.addSubcommand((g) =>
					g
						.setName("post")
						.setDescription("Post a named signup created by /signup format create.")
						.addStringOption((o) =>
							o
								.setName("name")
								.setDescription("The name of the signup made with /signup format create")
								.setRequired(true)
								.setAutocomplete(true)
						)
						.addStringOption((o) =>
							o.setName("datetime").setDescription("When the signup should close (UTC), in the format of YYYY-MM-DD HH:mm")
						)
				)
				.addSubcommand((g) =>
					g
						.setName("default")
						.setDescription("Quickly edit & post the default signup.")
						.addStringOption((o) => o.setName("title").setDescription("The title of the message."))
						.addStringOption((o) => o.setName("subtitle").setDescription("The title of the section."))
						.addStringOption((o) =>
							o.setName("datetime").setDescription("When the signup should close (UTC), in the format of YYYY-MM-DD HH:mm")
						)
				),
			{
				idHints: ["985217745178214490", "995456102021996694"],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
	}

	public async chatInputRun(interaction: Command.ChatInputInteraction) {
		const channel = interaction.channel as TextChannel;
		this.container.client.user;
		if (!channel.permissionsFor(interaction.guild!.me!).has(["SEND_MESSAGES", "VIEW_CHANNEL", "READ_MESSAGE_HISTORY", "MANAGE_MESSAGES"])) {
			throw GeneralErrors.BOT_MISSING_PERM;
		}
		const cmd_name = interaction.options.getSubcommand() as string;
		switch (cmd_name) {
			case "edit":
				await this.viewSignups(interaction);
				break;
			case "create":
				await this.createSignup(interaction);
				break;
			case "post":
				await this.postSignup(interaction, interaction.options.getString("name", true));
				break;
			case "default":
				await this.postSignup(interaction, "default", interaction.options.getString("title"), interaction.options.getString("subtitle"));
				break;
		}
	}

	public async autocompleteRun(interaction: Command.AutocompleteInteraction) {
		const curr_val = String(interaction.options.getFocused()).toLowerCase();
		const options: ApplicationCommandOptionChoiceData[] = (
			await this.container.db<{ name: string }[]>`
			SELECT format_name as name FROM signup_formats WHERE server_id = ${interaction.guild!.id}`
		)
			.map(({ name }) => {
				return { name, value: name };
			})
			.sort((a, b) => a.name.length - b.name.length);
		options.unshift({ name: "default", value: "default" });
		await interaction.respond(options.filter((e) => _includes(e.name.toLowerCase(), curr_val)));
	}

	public async postSignup(interaction: CommandInteraction, format_name: string, title?: string | null, subtitle?: string | null) {
		const guild = interaction.guild!;
		const channel = interaction.channel!;
		const date = interaction.options.getString("datetime");
		let datetime = null;
		if (date !== null && date !== "") {
			const timestamp = new Timestamp("YYYY-MM-DD HH:mm");
			datetime = Timestamp.utc(date);
			const example = timestamp.displayUTC(new Date().getTime() + 5 * 60_000);
			if (isNaN(datetime.getTime())) {
				throw SignupErrors.INVALID_DATETIME.replaceStr("description", ["$1", example]);
			}
			if (datetime.getTime() < new Date().getTime()) {
				throw SignupErrors.PAST_DATETIME.replaceStr("description", ["$1", example]);
			}
			const dformatedTime = Formatters.time(datetime, Formatters.TimestampStyles.ShortDateTime);
			const confirmMSG = await interaction.reply({
				fetchReply: true,
				ephemeral: true,
				content: `Signup set to close on: ${dformatedTime} is this correct?`,
				components: [
					new MessageActionRow({
						components: [
							new MessageButton({ label: "YES", style: "SUCCESS", customId: "yes" }),
							new MessageButton({ label: "NO", style: "DANGER", customId: "no" })
						]
					})
				]
			});
			if (!isMessageInstance(confirmMSG)) {
				return;
			}
			const confirmed = await confirmMSG
				.awaitMessageComponent({ time: 20_000 })
				.then(async (int) => {
					if (int.customId === "yes") {
						await int.update({ content: `Signup will close on: ${dformatedTime}`, components: [] });
						return true;
					}
					await int.update({ content: "cancelled", components: [] });
					return false;
				})
				.catch(async () => {
					await interaction.editReply({ content: "cancelled", components: [] });
					return false;
				});
			if (!confirmed) {
				return;
			}
		}

		let signup: CustomSignup | null = new CustomSignup();
		signup.messages[0].title = title ?? "";
		signup.sections[0].title = subtitle ?? "";
		let error = "";
		if (format_name.toLowerCase() !== "default") {
			const [row] = await this.container.db<({ format: string } | undefined)[]>`
			SELECT format FROM signup_formats 
			WHERE server_id = ${guild.id} AND format_name = ${format_name}`;
			if (row === undefined) {
				throw SignupErrors.FORMAT_NOT_FOUND.replaceStr("message", ["$1", format_name]);
			}
			const { format } = row;

			[signup, error] = await CustomSignup.import(format, guild, true);
			if (!signup) {
				throw SignupErrors.INVALID_SIGNUP_IMPORT.replaceStr("description", ["$1", error]);
			}
		}

		const [{ id }] = await this.container.db<{ id: number }[]>`
		INSERT INTO signups (server_id, signup_json, signup_expire)
		VALUES (${guild.id}, ${this.container.db.json(JSON.stringify(signup))}, ${datetime?.toISOString() ?? null})
		RETURNING signup_id as id`;
		const embeds = await signup.getEmbeds(guild, datetime);
		const { buttons } = signup;
		const message_ids: { signup_id: number; server_id: string; message_id: string; message_index: number }[] = [];
		try {
			for (const [i, embed] of embeds.entries()) {
				message_ids.push({
					signup_id: id,
					server_id: guild.id,
					message_id: (
						await channel.send({
							content: i === 0 ? signup.text || null : null,
							embeds: [embed],
							components: [new MessageActionRow({ components: buttons })]
						})
					).id,
					message_index: i
				});
			}
		} catch {
			await this.container.db`DELETE FROM signups WHERE server_id = ${guild.id} AND signup_id = ${id}`;
			throw SignupErrors.POSTING_SIGNUP;
		}
		await this.container.db`INSERT INTO signup_messages ${this.container.db(message_ids)}`;

		if (!interaction.replied) {
			await interaction.reply({ ephemeral: true, content: "No date set, signup will not end automatically." });
		}
		if (error) {
			await interaction.followUp({ ephemeral: true, content: error });
		}
	}

	public async viewSignups(interaction: CommandInteraction) {
		const formats = await this.container.db<({ format_name: string; format: string } | undefined)[]>`
		SELECT format_name, format FROM signup_formats WHERE server_id = ${interaction.guild!.id}`;
		if (formats.length === 0) {
			await interaction.reply({ ephemeral: true, content: "No formats saved. Make one using /signup format create" });
			return;
		}
		const overviewEmbed = new Embed({ title: "Select a format to edit, share, or delete." });
		const formatDict: { [key: string]: CustomSignup | undefined } = {};
		const embeds: { [key: string]: Embed | undefined } = {};
		const selectmenu = new MessageSelectMenu({ customId: "formatSelect", options: [{ label: "Overview", value: "overview", default: true }] });
		for (const { format_name, format } of formats) {
			const signup = new CustomSignup(JSON.parse(format));
			formatDict[format_name] = signup;
			selectmenu.options.push({ label: format_name, value: format_name, default: false, description: null, emoji: null });
			const message_titles = signup.messages.map((m) => `\`${m.title || "\u200b "}\``).join(", ");
			const section_titles = signup.sections.map((s) => `\`${s.title || "\u200b "}\``).join(", ");
			overviewEmbed.addField(
				`**${format_name}**`,
				`*Messages (${signup.messages.length}):* ${message_titles}
				*Sections (${signup.sections.length}):* ${section_titles}`,
				false
			);
			embeds[format_name] = new Embed({
				title: format_name,
				description: `**Export string:**\n\`\`\`\n${await signup.export()}\n\`\`\``
			}).addFields(
				Embed.field(`Messages (${signup.messages.length}):`, message_titles, false),
				Embed.field(`Sections (${signup.sections.length}):`, section_titles, false)
			);
		}
		const reply = await interaction.reply({
			fetchReply: true,
			ephemeral: true,
			embeds: [overviewEmbed],
			components: [new MessageActionRow({ components: [selectmenu] })]
		});
		if (!isMessageInstance(reply)) {
			return;
		}
		const collector = reply.createMessageComponentCollector({ idle: 150_000 });
		collector.on("collect", async (int: SelectMenuInteraction | ButtonInteraction) => {
			try {
				if (int.isSelectMenu()) {
					const [val] = int.values;
					const embed = val === "overview" ? overviewEmbed : embeds[val];
					for (const o of selectmenu.options) {
						o.default = o.value === val;
					}
					if (!embed) {
						return;
					}
					const buttons = new MessageActionRow({
						components: [
							new MessageButton({ customId: `edit;${val}`, label: "EDIT", style: "PRIMARY" }),
							new MessageButton({ customId: `delete;${val}`, label: "DELETE", style: "DANGER" })
						]
					});
					const selectRow = new MessageActionRow({ components: [selectmenu] });
					await int.update({
						components: val === "overview" ? [selectRow] : [selectRow, buttons],
						embeds: [embed]
					});
				} else if (int.isButton()) {
					const [action, val] = int.customId.split(";");
					const signup = formatDict[val];
					if (action === "edit" && signup) {
						collector.stop();
						await this.createSignup(int, signup, val);
					} else if (action === "delete") {
						await this.container.db`DELETE FROM signup_formats WHERE format_name = ${val} AND server_id = ${int.guild!.id}`;
						selectmenu.options = selectmenu.options.filter((o) => o.value !== val);
						overviewEmbed.setFields(overviewEmbed.fields.filter((f) => f.name !== `**${val}**`));
						embeds[val] = undefined;
						formatDict[val] = undefined;
						await int.update({ components: [new MessageActionRow({ components: [selectmenu] })] });
						await int.followUp({ ephemeral: true, content: `Deleted signup format: \`${val}\`` });
					}
				}
			} catch {}
		});
		collector.on("end", async () => {
			await interaction.editReply({ components: [] });
		});
	}

	private async updateSignup(channel: TextChannel, messages: Message[], signup: CustomSignup, edit_all?: boolean) {
		const embeds = await signup.getEmbeds(channel.guild, new Date(messages[0].createdAt.setHours(messages[0].createdAt.getHours() + 24)));
		const buttons = signup.buttons.map((b) => {
			b.setDisabled(true);
			return b;
		});
		const row = buttons.length > 0 ? [new MessageActionRow({ components: buttons })] : [];
		if (messages.length > signup.messages.length) {
			const diff = messages.length - signup.messages.length;
			const to_remove = messages.splice(messages.length - diff, diff);
			await channel.bulkDelete(to_remove, true);
		} else if (messages.length < signup.messages.length) {
			for (let i = messages.length; i < embeds.length; i++) {
				messages.push((await channel.send({ embeds: [embeds[i]], components: row })) as Message);
			}
		}
		if ((messages.length === signup.messages.length && !(edit_all ?? false)) || (edit_all ?? false)) {
			for (const [i, message] of messages.entries()) {
				let sameButton = true;
				let editText = false;
				let content = null;
				if (i === 0 && signup.text !== message.content) {
					editText = true;
					content = `<a:loading:729059787093835857> **Preview:**\n\n${signup.text}`;
				}
				if (message.components.length > 0) {
					const row1 = row[0];
					const row2 = message.components[0];
					for (const [i, button] of row1.components.entries()) {
						const button2 = row2.components[i] as MessageButton;
						if (
							(button2 === undefined && button !== undefined) ||
							// Checks if same IDs, but treats null and undefined as the same
							(button.emoji?.id ?? null) !== (button2.emoji?.id ?? null) ||
							((button.emoji?.id ?? "") && (button.emoji?.name ?? null) !== (button2.emoji?.name ?? null)) ||
							button.customId !== button2.customId ||
							button.label !== button2.label ||
							button.style !== button2.style
						) {
							sameButton = false;
							break;
						}
					}
				} else if (row.length > 0) {
					sameButton = false;
				}
				if (!message.embeds[0].equals(embeds[i]) || !sameButton || editText) {
					messages[i] = await message.edit({ embeds: [embeds[i]], components: row, content });
				}
			}
		}
	}

	private getSignupOptions(row: PartialModalActionRow[], guild: Guild): { error: string; options: signup_message | signup_section } {
		const options: signup_message = {
			title: row[0].components[0].value,
			limit: parseInt(row[1].components[0].value, 10) || undefined,
			requires: row[2].components[0].value.split(",").map((r) => r.replaceAll(" ", ""))
		};
		let error = "";
		if (options.requires) {
			const { roles, err } = CustomSignup.validateRoles(options.requires, guild);
			options.requires = roles;
			error += err;
		}
		// Check if section options
		if (row.length > 3) {
			const buttonFormatStr = row[3].components[0].value;
			const inline = row[4].components[0].value.toLowerCase() === "true";
			const { error: err, button } = CustomSignup.validateButtonStr(buttonFormatStr);
			error += err;
			return { error, options: _merge(options, { inline, button }) };
		}
		return { error, options };
	}

	private async createSignup(interaction: CommandInteraction | ButtonInteraction, editSignup?: CustomSignup, signup_name?: string) {
		const channel = interaction.channel as TextChannel;
		const embed = new Embed({ title: "How it works: " }).setDescription(`
• A user may only appear **once** in a signup.
• A signup is comprised of (at least) one message, up to a maximum of 5. 
• A message may have a limit on how many users are allowed to sign up on it.
• A message will contain (at least) one or more sections up to a maximum of 5.
• Section settings are the same for all messages.
• Each section can have a limit on how many people can join it. Message limit takes priority.
• Each section can have a roles required to join it. Message roles take priority.
• Each section has a button that users may click. This button needs a label and/or an emoji.
• If a section has a limit this section's button will work for the next section if the limit is met and if the next section doesn't have a button.

**Custom Button**
• Buttons format: **\`[<:emoji_name:xxxx>][button text][BUTTON-STYLE]\`**
• Leave empty brackets **\`[]\`** to omit either emoji or text (can't omit both)
• [See here for Button styles](https://discord.com/developers/docs/interactions/message-components#button-object-button-styles)
• To get the emoji name in the correct format:
\u2003 + Go to a server duckbot is in, and has the emoji you want
\u2003 + In the message box type **\`\\:emoji_name:\`**
\u2003 + Send the message and copy the result.
• Button examples: 
\u2003 + **\`[✅]\`** *Note: for 'default' emojis use the unicode, not the name*
\u2003 + **\`[][Click to sign up!]\`**
\u2003 + **\`[<:tick:729059775173754960>][][PRIMARY]\`**

**Title Formatting**
• Using **\`[total]\`** in a section title will show amount of users signed for that section (and the limit if applicable). In a message title it will show amount of users signed for all sections in that message.
• Using **\`[date]\`** will show when the signup will close, or nothing if there's no closing date set. [Supports discord's timestamp formatting.](https://discord.com/developers/docs/reference#message-formatting-timestamp-styles)
\u200b \u200b e.g: **\`[t:date]\` \`[T:date]\` \`[d:date]\` \`[D:date]\` \`[f:date]\` \`[F:date]\` \`[R:date]\` **
*Note: [date] will get replaced by a random date in the preview, the actual expiry date is set when you use /signup post*

***If you have any need or suggestions for signup customization features please use the*** **\`/feedback\`** ***command.***\n\u200b
		`);
		let signup = editSignup || new CustomSignup();
		const settingsRow = new MessageActionRow({
			components: [
				new MessageButton({ customId: "save", emoji: "<:save:976762652267278396>", style: "SUCCESS" }),
				new MessageButton({ customId: "settings", emoji: "<:settings:976764234815926282>", style: "SECONDARY" }),
				new MessageButton({ customId: "import", label: "IMPORT | EXPORT", style: "PRIMARY" }),
				new MessageButton({ customId: "fill", label: "FILL", style: "SECONDARY" }),
				new MessageButton({ customId: "cancel", label: "CANCEL", style: "DANGER" })
			]
		});
		const messageRow = new MessageActionRow({
			components: [
				new MessageButton({
					customId: "addMessage",
					label: "\u200b \u200b ADD MESSAGE \u200b \u200b ",
					style: "SUCCESS",
					disabled: signup.messages.length >= 5
				}),
				new MessageButton({ customId: "editMessage", label: "\u202F\u200b \u200b EDIT MESSAGE \u200b \u200b\u202f\u202f", style: "PRIMARY" }),
				new MessageButton({
					customId: "removeMessage",
					label: "\u200b \u200b REMOVE MESSAGE ",
					style: "DANGER",
					disabled: signup.messages.length <= 1
				})
			]
		});
		const sectionRow = new MessageActionRow({
			components: [
				new MessageButton({
					customId: "addSection",
					label: "\u200b \u200b ADD SECTION \u200b \u200b \u200b\u202f",
					style: "SUCCESS",
					disabled: signup.sections.length >= 5
				}),
				new MessageButton({
					customId: "editSection",
					label: "\u202F\u200b \u200b \u202FEDIT SECTION \u200b \u200b  \u200b\u202F\u202f",
					style: "PRIMARY"
				}),
				new MessageButton({
					customId: "removeSection",
					label: "\u200b \u200b REMOVE SECTION \u200b",
					style: "DANGER",
					disabled: signup.sections.length <= 1
				})
			]
		});
		const selectRow = new MessageActionRow({
			components: [
				new MessageSelectMenu({
					custom_id: "select",
					disabled: true,
					maxValues: 1,
					options: [{ label: "Select", description: "Make a selection.", value: "select" }]
				})
			]
		});

		const configMsg = (await interaction.reply({
			embeds: [embed],
			components: [settingsRow, messageRow, sectionRow],
			fetchReply: true
		})) as Message;
		const buttons = signup.buttons.map((b) => {
			b.setDisabled(true);
			return b;
		});
		const row = buttons.length > 0 ? [new MessageActionRow({ components: buttons })] : [];

		const preview_text = "<a:loading:729059787093835857> **Preview:**";
		const embeds = await signup.getEmbeds(interaction.guild!, new Date(configMsg.createdAt.setHours(configMsg.createdAt.getHours() + 24)));
		const replies = [
			(await interaction.followUp({
				content: `${preview_text}\n\n${signup.text}`,
				embeds: [embeds[0]],
				components: row,
				fetchReply: true
			})) as Message
		];
		if (embeds.length > 1) {
			for (const embed of embeds.slice(1)) {
				replies.push((await channel.send({ embeds: [embed], components: row })) as Message);
			}
		}

		const configCollector = configMsg.createMessageComponentCollector({
			filter: (i) => {
				return i.user.id === interaction.user.id;
			},
			idle: 150_000
		});

		const saveModal = new Modal({
			customId: "saveModal",
			title: "Save this signup.",
			components: [
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: "saveAs",
							label: "(Unique) Name",
							placeholder: "Name to use when referring to this signup in /signup post",
							style: "SHORT",
							maxLength: 15,
							required: true
						})
					]
				})
			]
		});

		const settingsModal = new Modal({
			customId: "settingsModal",
			title: "Signup Settings",
			components: [
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: "settingsMessage",
							style: "PARAGRAPH",
							label: "(optional) Signup Message",
							placeholder: "This will be sent before the signup embeds.",
							maxLength: 1000
						})
					]
				}),
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: "settingsNames",
							style: "SHORT",
							label: "Show users as nickname.",
							placeholder: "Must be 'True' or 'False'",
							value: "True",
							maxLength: 5
						})
					]
				})
			]
		});

		const exportModal = new Modal({
			customId: "exportModal",
			title: "Import or Export a signup",
			components: [
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: "modalCode",
							style: "PARAGRAPH",
							label: "Copy to export this signup.",
							placeholder: "Paste a signup here to import"
						})
					]
				})
			]
		});

		const messageModal = new Modal({
			customId: "messageModal",
			title: "Add Message",
			components: [
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: "msg_title",
							style: "SHORT",
							maxLength: 40,
							label: "Message title",
							placeholder: "(optional)"
						})
					]
				}),
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: "msg_limit",
							style: "SHORT",
							maxLength: 2,
							label: "Message limit",
							placeholder: "(optional) This has priority over section limits."
						})
					]
				}),
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: "msg_roles_req",
							style: "PARAGRAPH",
							label: "Roles required to sign onto this message.",
							placeholder: "(optional) Comma separated list of role IDs.",
							maxLength: 400
						})
					]
				})
			]
		});

		const sectionModal = new Modal({
			customId: "sectionModal",
			title: "Add Section",
			components: [
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: "section_title",
							style: "SHORT",
							maxLength: 40,
							label: "Section title",
							placeholder: "(optional)"
						})
					]
				}),
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: "section_limit",
							style: "SHORT",
							maxLength: 2,
							label: "Section limit",
							placeholder: "(optional) May conflict if any message limits are set."
						})
					]
				}),
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: "section_roles_req",
							style: "PARAGRAPH",
							label: "Roles required to sign onto this section.",
							placeholder: "(optional) Comma separated list of role IDs.",
							maxLength: 400
						})
					]
				}),
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: "section_button",
							style: "SHORT",
							label: "Custom Button",
							placeholder: "[<:emoji_name:xxxx>][button text][BUTTON-STYLE]",
							value: "[✅][][SUCCESS]",
							maxLength: 80
						})
					]
				}),
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: "section_inline",
							style: "SHORT",
							label: "Display inline",
							placeholder: "Must be 'True' or 'False'",
							value: "False",
							maxLength: 5
						})
					]
				})
			]
		});

		configCollector.on("collect", async (i) => {
			try {
				const [id, ...info] = i.customId.split(";");
				switch (id) {
					case "save":
						await (async () => {
							const modal = new Modal(saveModal);
							modal.customId = `saveModal${i.id}`;
							modal.components[0].components[0].value = signup_name ?? "";
							await i.showModal(modal);
							i.awaitModalSubmit({ filter: (interaction) => interaction.customId === modal.customId, time: 90_000 })
								.then(async (interaction) => {
									const name = interaction.components[0].components[0].value.trim();
									if (name.replaceAll(" ", "") === "" || name.toLowerCase() === "default") {
										await interaction.reply({ ephemeral: true, content: "Invalid name" });
									} else {
										const names = (
											await this.container.db<({ format_name: string } | undefined)[]>`
										SELECT format_name FROM  signup_formats WHERE server_id = ${interaction.guild!.id}`
										).map((n) => n?.format_name);
										if (names.length >= 25) {
											await interaction.reply({ ephemeral: true, content: "Can't save more than 25 formats." });
											return;
										}
										const exists = names.includes(name);
										const confirmMSG = await interaction.reply({
											fetchReply: true,
											ephemeral: true,
											content: `${
												exists
													? "Replace existing signup "
													: `Save ${signup_name === undefined ? "" : "**(new)** "}signup as `
											}**\`${name}\`** ?`,
											components: [
												new MessageActionRow({
													components: [
														new MessageButton({
															label: "YES",
															style: "SUCCESS",
															customId: `confirmSave${interaction.id}`
														}),
														new MessageButton({
															label: "NO",
															style: "DANGER",
															customId: `cancelSave${interaction.id}`
														})
													]
												})
											]
										});
										(confirmMSG as Message)
											.awaitMessageComponent({ time: 20_000 })
											.then(async (int) => {
												let message = "error";
												let save = false;
												if (!configCollector.ended) {
													if (int.customId === `confirmSave${interaction.id}`) {
														message = `${exists ? "Replaced signup " : "Saved signup as "}**\`${name}\`**`;
														save = true;
													}
													if (int.customId === `cancelSave${interaction.id}`) {
														message = "Cancelled save.";
													}
												}
												await int.update({ content: message, components: [] });
												if (save) {
													const json_opt = this.container.db.json(JSON.stringify(signup.options));
													configCollector.stop("save");
													await this.container.db`
													INSERT INTO signup_formats VALUES (${interaction.guild!.id}, ${name}, ${json_opt})
													ON CONFLICT (server_id, format_name) 
													DO UPDATE SET format = ${json_opt}`;
												}
											})
											.catch(async () => {
												await interaction.editReply({ components: [], content: "Cancelled save." });
											});
									}
								})
								.catch(() => {});
						})();
						break;
					case "settings":
						await (async () => {
							const modal = new Modal(settingsModal);
							modal.customId = `settingsModal${i.id}`;
							modal.components[0].components[0].value = signup.text;
							modal.components[1].components[0].value = signup.isNicknames ? "True" : "False";
							await i.showModal(modal);
							i.awaitModalSubmit({ filter: (interaction) => interaction.customId === modal.customId, time: 90_000 })
								.then(async (interaction) => {
									const text = interaction.components[0].components[0].value || "";
									if (text !== signup.text) {
										signup.text = text;
										await replies[0].edit({ content: `${preview_text}\n\n${text}` });
									}
									const isNicknames = interaction.components[1].components[0].value.toLowerCase() !== "false";
									await interaction.update({});
									if (isNicknames !== signup.isNicknames) {
										signup.isNicknames = isNicknames;
										await this.updateSignup(channel, replies, signup, true);
									}
								})
								.catch(() => {});
						})();
						break;
					case "import":
						await (async () => {
							exportModal.customId = `exportModal${i.id}`;
							let exportStr = await signup.export();
							if (exportStr.length > 4000) {
								exportStr = "Error";
							}
							exportModal.components[0].components[0].value = exportStr;
							await i.showModal(exportModal);
							i.awaitModalSubmit({ filter: (interaction) => interaction.customId === exportModal.customId, time: 90_000 })
								.then(async (interaction) => {
									const [newSignup, error] = await CustomSignup.import(
										interaction.components[0].components[0].value || "",
										channel.guild
									);
									if (error) {
										await interaction.reply({ ephemeral: true, content: error });
									} else {
										await interaction.update({});
									}
									if (newSignup) {
										signup = newSignup;
									} else {
										return;
									}
									await this.updateSignup(channel, replies, signup, true);
									messageRow.components[0].setDisabled(signup.messages.length >= 5);
									messageRow.components[2].setDisabled(signup.messages.length <= 1);
									sectionRow.components[0].setDisabled(signup.sections.length >= 5);
									sectionRow.components[2].setDisabled(signup.sections.length <= 1);

									await configMsg.edit({ components: [settingsRow, messageRow, sectionRow] });
								})
								.catch(() => {});
						})();
						break;
					case "empty":
					case "fill":
						await (async () => {
							const currTime = new Date().getTime();
							let count = 0;
							if (info.length > 1 && info[0] && info[1]) {
								const oldTime = new Date(parseInt(info[1], 10)).getTime();
								const diff = currTime - oldTime;
								count = parseInt(info[0], 10);
								if (
									(diff <= 3000 && count > 3) ||
									(diff <= 5000 && count > 4) ||
									(replies.length >= 4 && count > 2 && diff <= 2000)
								) {
									return;
								}
								if (diff >= 9_000) {
									count = 0;
								}
							}
							if (id === "fill") {
								settingsRow.components[3].customId = `empty;${++count};${currTime}`;
								settingsRow.components[3].setLabel("");
								settingsRow.components[3].setEmoji("<:undo:978288409200181308>");
								await signup.testFill(i.guild!);
							} else if (id === "empty") {
								settingsRow.components[3].customId = `fill;${++count};${currTime}`;
								settingsRow.components[3].setLabel("FILL");
								settingsRow.components[3].setEmoji("");
								signup.members = signup.makeArray();
							}
							setTimeout(
								async () => {
									await i.update({ components: [settingsRow, messageRow, sectionRow] });
								},
								replies.length > 2 ? 2300 : 500
							);
							await this.updateSignup(channel, replies, signup, true);
						})();
						break;
					case "cancel":
						await i.update({});
						configCollector.stop("cancel");
						break;
					case "addMessage":
						messageModal.title = `Add message #${replies.length + 1}`;
						messageModal.customId = `messageModal${i.id}`;
						await i.showModal(messageModal);
						i.awaitModalSubmit({ filter: (interaction) => interaction.customId === messageModal.customId, time: 90_000 })
							.then(async (interaction) => {
								const { error, options } = this.getSignupOptions(interaction.components, channel.guild);
								if (error) {
									await interaction.reply({ ephemeral: true, content: error });
								} else {
									await interaction.update({});
								}
								signup.addMessage(options);
								messageRow.components[0].setDisabled(signup.messages.length >= 5);
								messageRow.components[2].setDisabled(signup.messages.length <= 1);
								await configMsg.edit({ components: [settingsRow, messageRow, sectionRow] });
								await this.updateSignup(channel, replies, signup);
							})
							.catch(() => {});
						break;
					case "editMessage":
						await (async () => {
							const modal = new Modal(messageModal);
							let messageIndex = 0;
							if (replies.length > 1) {
								await i.update({});
								const row = new MessageActionRow(selectRow);
								row.components[0].customId = `selectMessage${interaction.id}`;
								const options = signup.messages.map((val: signup_message, i) => {
									return { label: `#${i + 1} ${val.title}`, value: `${i}`, description: "Select this message to edit it." };
								});
								row.components[0].setDisabled(false);
								row.components[0].setOptions(options);
								await configMsg.edit({ components: [settingsRow, messageRow, sectionRow, row] });
								const selectMenuInteraction = await configMsg
									.awaitMessageComponent({
										filter: (interaction) =>
											interaction.customId === row.components[0].customId && interaction.user.id === i.user.id,
										time: 35_000,
										componentType: "SELECT_MENU"
									})
									.catch(() => {
										return null;
									});
								await configMsg.edit({ components: [settingsRow, messageRow, sectionRow] });
								if (selectMenuInteraction) {
									messageIndex = parseInt(selectMenuInteraction.values[0], 10) || 0;
									if (messageIndex >= signup.messages.length || messageIndex < 0) {
										messageIndex = 0;
									}
									i = selectMenuInteraction;
								}
							}
							modal.components[0].components[0].value = signup.messages[messageIndex].title;
							modal.components[1].components[0].value = signup.messages[messageIndex].limit?.toString() ?? "";
							modal.components[2].components[0].value = signup.messages[messageIndex].requires?.join(", ") ?? "";
							modal.title = `Edit Message #${messageIndex + 1}`;
							modal.customId = `messageModal${i.id}`;
							if (i.replied) {
								return;
							}
							await i.showModal(modal);
							i.awaitModalSubmit({ filter: (interaction) => interaction.customId === modal.customId, time: 90_000 })
								.then(async (interaction) => {
									const { error, options } = await this.getSignupOptions(interaction.components, channel.guild);
									if (error) {
										await interaction.reply({ ephemeral: true, content: error });
									} else {
										await interaction.update({});
									}
									signup.editMessage(messageIndex, options);
									await this.updateSignup(channel, replies, signup);
								})
								.catch(() => {});
						})();
						break;
					case "removeMessage":
						if (signup.popMessage()) {
							messageRow.components[0].setDisabled(signup.messages.length >= 5);
							messageRow.components[2].setDisabled(signup.messages.length <= 1);
							await i.update({ components: [settingsRow, messageRow, sectionRow] });
							await this.updateSignup(channel, replies, signup);
						}
						break;
					case "addSection":
						sectionModal.customId = `sectionModal${i.id}`;
						if (i.replied) {
							return;
						}
						await i.showModal(sectionModal);
						i.awaitModalSubmit({ filter: (interaction) => interaction.customId === sectionModal.customId, time: 90_000 })
							.then(async (interaction) => {
								const { error, options } = this.getSignupOptions(interaction.components, channel.guild);
								if (error) {
									await interaction.reply({ ephemeral: true, content: error });
								} else {
									await interaction.update({});
								}
								signup.addSection(options as signup_section);
								sectionRow.components[0].setDisabled(signup.sections.length >= 5);
								sectionRow.components[2].setDisabled(signup.sections.length <= 1);
								await configMsg.edit({ components: [settingsRow, messageRow, sectionRow] });
								await this.updateSignup(channel, replies, signup);
							})
							.catch(() => {});
						break;
					case "editSection":
						await (async () => {
							const modal = new Modal(sectionModal);
							let sectionIndex = 0;
							if (signup.sections.length > 1) {
								await i.update({});
								const row = new MessageActionRow(selectRow);
								row.components[0].customId = `selectSection${interaction.id}`;
								const options = signup.sections.map((val: signup_message, i) => {
									return { label: `#${i + 1} ${val.title}`, value: `${i}`, description: "Select this section to edit it." };
								});
								row.components[0].setDisabled(false);
								row.components[0].setOptions(options);
								await configMsg.edit({ components: [settingsRow, messageRow, sectionRow, row] });
								const selectMenuInteraction = await configMsg
									.awaitMessageComponent({
										filter: (interaction) =>
											interaction.customId === row.components[0].customId && interaction.user.id === i.user.id,
										time: 35_000,
										componentType: "SELECT_MENU"
									})
									.catch(() => {
										return null;
									});
								await configMsg.edit({ components: [settingsRow, messageRow, sectionRow] });
								if (selectMenuInteraction) {
									sectionIndex = parseInt(selectMenuInteraction.values[0], 10) || 0;
									if (sectionIndex >= signup.sections.length || sectionIndex < 0) {
										sectionIndex = 0;
									}
									i = selectMenuInteraction;
								}
							}
							modal.components[0].components[0].value = signup.sections[sectionIndex].title;
							modal.components[1].components[0].value = signup.sections[sectionIndex].limit?.toString() ?? "";
							modal.components[2].components[0].value = signup.sections[sectionIndex].requires?.join(", ") ?? "";
							modal.components[3].components[0].value = CustomSignup.buttonToString(signup.sections[sectionIndex].button);
							modal.components[4].components[0].value = _upperFirst(String(signup.sections[sectionIndex].inline));
							modal.title = `Edit Section #${sectionIndex + 1}`;
							modal.customId = `sectionModal${i.id}`;
							if (i.replied) {
								return;
							}
							await i.showModal(modal);
							i.awaitModalSubmit({ filter: (interaction) => interaction.customId === modal.customId, time: 90_000 })
								.then(async (interaction) => {
									const { error, options } = this.getSignupOptions(interaction.components, channel.guild);
									if (error) {
										await interaction.reply({ ephemeral: true, content: error });
									} else {
										await interaction.update({});
									}
									signup.editSection(sectionIndex, options as signup_section);
									await this.updateSignup(channel, replies, signup);
								})
								.catch(() => {});
						})();
						break;
					case "removeSection":
						if (signup.popSection()) {
							sectionRow.components[2].setDisabled(signup.sections.length <= 1);
							sectionRow.components[0].setDisabled(false);
							await i.update({ components: [settingsRow, messageRow, sectionRow] });
							await this.updateSignup(channel, replies, signup);
						}
						break;
				}
			} catch {
				// TODO: log error
			}
		});

		configCollector.on("end", async (_, r) => {
			try {
				if (r === "cancel") {
					await channel.bulkDelete([...replies, configMsg], true);
				}
				if (r === "idle") {
					await interaction.editReply({ components: [] });
					await replies[0].edit({ content: "❌ **Preview:**" });
				}
				if (r === "save") {
					await channel.bulkDelete([...replies, configMsg], true);
				}
			} catch {}
		});
	}
}
