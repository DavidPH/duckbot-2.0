import { ApplicationCommandRegistry, Command, RegisterBehavior, CommandOptionsRunTypeEnum, CommandOptions } from "@sapphire/framework";
import { MessageActionRow, MessageButton } from "discord.js";
import { isMessageInstance } from "@sapphire/discord.js-utilities";
import { SignupErrors } from "../../lib/utils/errors";
import { ApplyOptions } from "@sapphire/decorators";
import { AsyncQueue } from "@sapphire/async-queue";
import { ContextMenuCommandBuilder } from "@discordjs/builders";
import { PermissionFlagsBits, ApplicationCommandType } from "discord-api-types/v10";

@ApplyOptions<CommandOptions>({
	runIn: CommandOptionsRunTypeEnum.GuildAny
})
export class cmAddCommand extends Command {
	public override registerApplicationCommands(registry: ApplicationCommandRegistry) {
		registry.registerContextMenuCommand(
			new ContextMenuCommandBuilder()
				.setType(ApplicationCommandType.Message)
				.setName("Delete signup")
				.setDMPermission(false)
				.setDefaultMemberPermissions(PermissionFlagsBits.ManageEvents),
			{
				idHints: ["985225090755080312", "995456103791997011"],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
	}

	public async contextMenuRun(interaction: Command.ContextMenuInteraction) {
		if (!interaction.isMessageContextMenu()) return;
		const guild = interaction.guild!;
		const channel = interaction.channel!;
		const message = interaction.targetMessage;
		if (message.author.id !== this.container.client.user!.id) {
			throw SignupErrors.NOT_A_SIGNUP;
		}
		const messages = await this.container.db<{ signup_id: number; message_id: string }[]>`
			SELECT signup_id, message_id FROM signup_messages WHERE signup_id = (
			SELECT signup_id FROM signup_messages 
            WHERE message_id = ${message.id} AND server_id = ${guild.id}) ORDER BY message_index ASC 
        `;
		if (messages.length <= 0) {
			await interaction.reply({ ephemeral: true, content: "Signup not found." });
			return;
		}
		await interaction.deferReply({ ephemeral: true });
		let link = "";
		try {
			link = (await channel.messages.fetch(messages[0].message_id)).url;
		} catch {
			await interaction.followUp({ ephemeral: true, content: "Couldn't find message." });
			return;
		}

		const key = `${guild.id}${messages[0].signup_id}`;
		let queue = this.container.signup_queue[key];
		if (queue) {
			clearTimeout(queue[1]);
			queue[1] = setTimeout(() => delete this.container.signup_queue[key], 15 * 60_000);
		} else {
			this.container.signup_queue[key] = [new AsyncQueue(), setTimeout(() => delete this.container.signup_queue[key], 15 * 60_000)];
			queue = this.container.signup_queue[key]!;
		}
		await queue[0].wait();
		const confirmMSG = await interaction.followUp({
			fetchReply: true,
			ephemeral: true,
			content: `Delete [this signup](<${link}>)? This can not be undone.`,
			components: [
				new MessageActionRow({
					components: [
						new MessageButton({ customId: "yes", style: "SUCCESS", label: "YES" }),
						new MessageButton({ customId: "no", style: "DANGER", label: "NO" })
					]
				})
			]
		});
		if (!isMessageInstance(confirmMSG)) return;
		const confirmed = await confirmMSG
			.awaitMessageComponent({ componentType: "BUTTON", time: 20_000 })
			.then(async (i) => {
				if (i.customId === "no") {
					await i.update({ content: "Cancelled.", components: [] });
					return false;
				}
				await i.update({ components: [] });
				return true;
			})
			.catch(async () => {
				await interaction.editReply({ content: "Cancelled", components: [] });
				return false;
			});
		if (!confirmed) {
			queue.shift();
			return;
		}
		try {
			for (const { message_id } of messages) {
				const msg = await channel.messages.fetch(message_id);
				msg.delete().catch(() => {});
			}
		} catch {
			await interaction.followUp({ ephemeral: true, content: "Something went wrong." });
		} finally {
			while (queue[0].remaining > 0) {
				queue.shift();
			}
			clearTimeout(queue[1]);
			delete this.container.signup_queue[key];

			await interaction.followUp({ ephemeral: true, content: "Deleted signup <:tick:729059775173754960>" });
			await this.container.db`DELETE FROM signups WHERE server_id = ${guild.id} and signup_id = ${messages[0].signup_id}`;
		}
	}
}
