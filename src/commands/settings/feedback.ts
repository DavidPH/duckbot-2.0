import { ApplicationCommandRegistry, Command, RegisterBehavior, CommandOptions } from "@sapphire/framework";
import { MessageActionRow, Modal, TextInputComponent } from "discord.js";
import { ApplyOptions } from "@sapphire/decorators";

@ApplyOptions<CommandOptions>({
	preconditions: ["NotIgnored"]
})
export class FeedbackCommand extends Command {
	public override registerApplicationCommands(registry: ApplicationCommandRegistry) {
		registry.registerChatInputCommand(
			{
				name: "feedback",
				description: "Send some feedback regarding DuckBot"
			},
			{
				idHints: ["987384911633711105", "995456190865747979"],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
	}

	public async chatInputRun(interaction: Command.ChatInputInteraction) {
		const modal = new Modal({
			customId: "feedback",
			title: "Send some feedback",
			components: [
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: `${interaction.id}feeedback`,
							label: "Feedback message",
							placeholder: "Feature request, bug report, or suggestion you want to make regarding DuckBot.",
							style: "PARAGRAPH"
						})
					]
				})
			]
		});
		await interaction.showModal(modal);
	}
}
