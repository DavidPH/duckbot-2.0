import { ApplicationCommandRegistry, Command, RegisterBehavior, CommandOptionsRunTypeEnum, CommandOptions } from "@sapphire/framework";
import {
	ButtonInteraction,
	Collection,
	Guild,
	MessageActionRow,
	MessageButton,
	MessageSelectMenu,
	MessageSelectOptionData,
	Modal,
	Role,
	Permissions,
	TextInputComponent
} from "discord.js";
import _truncate from "lodash/truncate";
import { isMessageInstance, isTextBasedChannel, isGuildMember } from "@sapphire/discord.js-utilities";
import { ApplyOptions } from "@sapphire/decorators";
import { codeBlock } from "@sapphire/utilities";
import Embed from "../../lib/utils/custom-embed";
import { supportLinkRow } from "../../lib/utils/errors";
import { Time } from "@sapphire/time-utilities";
import type { jailSettingsPayload } from "../../lib/utils/jail-utils";
import { SlashCommandBuilder } from "@discordjs/builders";
import { PermissionFlagsBits } from "discord-api-types/v10";
import _partition from "lodash/partition";

@ApplyOptions<CommandOptions>({
	runIn: CommandOptionsRunTypeEnum.GuildText
})
export class ConfigCommand extends Command {
	public override registerApplicationCommands(registry: ApplicationCommandRegistry) {
		registry.registerChatInputCommand(
			new SlashCommandBuilder()
				.setDefaultMemberPermissions(PermissionFlagsBits.ManageGuild)
				.setDMPermission(false)
				.setName("config")
				.setDescription("Configure the bot's settings.")
				.addSubcommand((s) => s.setName("jail").setDescription("Open the jail setup menu."))
				.addSubcommand((s) => s.setName("fun").setDescription("Enable or disable fun commands.")),
			{
				idHints: ["985217661954850856", "995456193080328212"],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
	}

	public async chatInputRun(interaction: Command.ChatInputInteraction) {
		const cmd_name = interaction.options.getSubcommand() as string;
		switch (cmd_name) {
			case "jail":
				await this.jailConfig(interaction);
				break;
			case "fun":
				await this.funConfig(interaction);
				break;
		}
	}

	private async funConfig(interaction: Command.ChatInputInteraction) {
		const { guild, member } = interaction;
		if (!guild || !isGuildMember(member)) return;
		if (!member.permissions.has(Permissions.FLAGS.ADMINISTRATOR)) {
			return interaction.reply({ content: "This command can only be run by someone with administrator permissions.", ephemeral: true });
		}

		// Collection<commandName, commandData>
		// All commands
		const funCommands = new Collection(
			this.container.stores
				.get("commands")
				.filter((cmd) => cmd.category === "fun" && typeof cmd.getCommandData === "function")
				.map((cmd) => [cmd.name, cmd.getCommandData!()])
		);

		const currentCommands = await guild.commands.fetch();
		// Only enabled commands
		const enabled = new Collection(
			currentCommands
				.map((c) => funCommands.get(c.name))
				.filter((c) => c !== undefined)
				.map((c) => [c!.name, c!])
		);

		// Make select menu for the commands
		const select = new MessageSelectMenu({ customId: `${interaction.id}addCommands`, placeholder: "Toggle Commands" });
		// Add enabled commands to select menu
		enabled.forEach((command) => {
			select.addOptions({
				label: `/${command.name}`,
				value: `removeCommand;${command.name}`,
				description: command.description,
				emoji: "🔸"
			});
		});
		// Add disabled commands to select menu
		funCommands.forEach((command) => {
			if (!enabled.has(command.name)) {
				select.addOptions({ label: `/${command.name}`, value: `addCommand;${command.name}`, description: command.description });
			}
		});

		let values: Array<string> = [];

		// Send config message
		select.setMinValues(1);
		const msg = await interaction.reply({
			ephemeral: true,
			components: [new MessageActionRow({ components: [select] })],
			fetchReply: true
		});
		const yesBtn = new MessageButton({ customId: `${interaction.id}yes`, label: "YES", style: "SUCCESS" });
		const noBtn = new MessageButton({ customId: `${interaction.id}no`, label: "NO", style: "DANGER" });
		if (!isMessageInstance(msg)) return;
		try {
			await msg.awaitMessageComponent({
				idle: 60_000,
				filter: async (i) => {
					if (i.isSelectMenu()) {
						const [a, b] = _partition(i.values, (v) => v.split(";")[0] === "removeCommand");
						const out = `${b.map((v) => `+ ${v.split(";")[1]}`).join("\n")}\n${a.map((v) => `- ${v.split(";")[1]}`).join("\n")}`;
						const embed = new Embed({
							description: `**Warning**
							Discord limits the amount of times you can add or remove a slash command from your server. Avoid using this command more than is necessary.`,
							fields: [{ name: "Command Changes:", value: `${codeBlock("diff", out)}` }]
						});
						values = i.values;
						await i.update({
							embeds: [embed],
							components: [new MessageActionRow({ components: [select] }), new MessageActionRow({ components: [yesBtn, noBtn] })]
						});
					}
					if (i.isButton()) {
						if (i.customId === `${interaction.id}yes`) {
							const embed = i.message.embeds[0];
							embed.description = "<a:loading:729059787093835857> Saving changes...";
							await i.update({ embeds: [embed], components: [] });
							return true;
						}
						await i.update({ embeds: [], components: [new MessageActionRow({ components: [select] })] });
					}
					return false;
				}
			});
		} catch {
			return interaction.editReply({ components: [], embeds: [], content: "Cancelled" });
		}

		// Update commands with user's selection

		for (const value of values) {
			const split = value.split(";");
			if (split[0] === "removeCommand") {
				enabled.delete(split[1]);
			} else if (split[0] === "addCommand") {
				const command = funCommands.get(split[1]);
				if (command) {
					enabled.set(split[1], command);
				}
			}
		}
		try {
			await guild.commands.set(enabled.map((c) => c.toJSON()));
		} catch {
			return interaction.editReply({ content: "Something went wrong." });
		}
		const message = await interaction.fetchReply();
		if (isMessageInstance(message)) {
			const embed = message.embeds[0];
			embed.description = "Success <:tick:729059775173754960>\nYou might need to refresh discord, or wait a while to see the command.";
			await interaction.editReply({ embeds: [embed] });
		}
	}

	private async jailConfig(interaction: Command.ChatInputInteraction) {
		const { guild } = interaction;
		if (!guild) return;

		let [settings] = await this.container.db<jailSettingsPayload[]>`SELECT * FROM jail_settings WHERE server_id = ${guild.id}`;
		let jailRoles = await this.container.jail.getSetup(guild.id);
		const embed = this.makeConfigEmbed(guild, settings, jailRoles);
		const msg = await interaction.reply({
			fetchReply: true,
			ephemeral: true,
			embeds: [embed],
			components: this.makeComponents(interaction, jailRoles)
		});
		if (!isMessageInstance(msg)) return;
		const collector = msg.createMessageComponentCollector({ idle: Time.Minute * 5 });
		collector.on("collect", async (i) => {
			if (i.isSelectMenu()) {
				await i.update({ components: this.makeComponents(interaction, jailRoles, i.values[0]) });
			}
			if (!i.isButton()) return;
			const opt = i.customId.split(";");
			switch (opt[0]) {
				case `${interaction.id}editMessage`:
					settings.jail_message = await this.editJailMessage(i, guild, settings, jailRoles);
					break;
				case `${interaction.id}addRole`:
					{
						const roles = await this.addJailRole(i, guild, jailRoles);
						if (roles) {
							if (!jailRoles) {
								jailRoles = [];
							}
							jailRoles.push(roles);
							const payload = `{${jailRoles.map(([r1, r2]) => `{${[r1.id, r2.id]}}`)}}`;
							await this.container.db`UPDATE jail_settings SET jail_roles = ${payload} WHERE server_id = ${guild.id}`;
						}
						await interaction.editReply({
							embeds: [this.makeConfigEmbed(guild, settings, jailRoles)],
							components: this.makeComponents(interaction, jailRoles)
						});
					}
					break;
				case `${interaction.id}remove`:
					{
						if (jailRoles) {
							const payload = `{${jailRoles.filter(([r]) => r.id !== opt[1]).map(([r1, r2]) => `{${[r1.id, r2.id]}}`)}}`;
							await this.container.db`UPDATE jail_settings SET jail_roles = ${payload} WHERE server_id = ${guild.id}`;
							jailRoles = await this.container.jail.getSetup(guild.id);
							await i.update({
								components: this.makeComponents(interaction, jailRoles),
								embeds: [this.makeConfigEmbed(guild, settings, jailRoles)]
							});
						}
					}
					break;
				case `${interaction.id}defaults`:
					settings = await this.defaultBehaviors(i, guild, settings, jailRoles);
					break;
			}
			[settings] = await this.container.db<jailSettingsPayload[]>`SELECT * FROM jail_settings WHERE server_id = ${guild.id}`;
			jailRoles = await this.container.jail.getSetup(guild.id);
		});
		collector.on("end", async () => {
			try {
				await interaction.editReply({ components: [supportLinkRow] });
			} catch {}
		});
	}

	private async defaultBehaviors(
		interaction: ButtonInteraction,
		guild: Guild,
		settings: jailSettingsPayload,
		roles: Array<[Role, Role]> | null
	): Promise<jailSettingsPayload> {
		const modal = new Modal({
			customId: `${interaction.id}defaultsModal`,
			title: "Edit default jailing behavior.",
			components: [
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: `${interaction.id}log-channel`,
							style: "SHORT",
							label: "log-channel ID",
							value: settings.log_channel ?? "",
							placeholder: `e.g. ${interaction.channelId}`,
							maxLength: 21
						})
					]
				}),
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: `${interaction.id}ephemeral`,
							style: "SHORT",
							label: "Send a message when jailing",
							placeholder: "Must be 'True' or 'False'",
							value: settings.ephemeral ? "False" : "True",
							maxLength: 5
						})
					]
				}),
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: `${interaction.id}replaceRoles`,
							style: "SHORT",
							label: "Replace roles when jailing.",
							placeholder: "Must be 'True' or 'False'",
							value: settings.replace ? "True" : "False",
							maxLength: 5
						})
					]
				}),
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: `${interaction.id}timeout`,
							style: "SHORT",
							label: "Timeout the user on jail",
							placeholder: "Must be 'True' or 'False'",
							value: settings.timeout ? "True" : "False",
							maxLength: 5
						})
					]
				}),
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: `${interaction.id}enforceJail`,
							style: "SHORT",
							label: "Enforce Jail Role on leaving/rejoining",
							placeholder: "Must be 'True' or 'False'",
							value: settings.enforce ? "True" : "False",
							maxLength: 5
						})
					]
				})
			]
		});
		await interaction.showModal(modal);
		await interaction
			.awaitModalSubmit({ filter: (i) => i.customId === modal.customId, time: Time.Minute * 3 })
			.then(async (i) => {
				let logChannelID = i.components[0].components[0].value || null;
				const logChannel = logChannelID === null ? null : guild.channels.cache.get(logChannelID);
				const ephemeral = i.components[1].components[0].value.toLowerCase() === "false";
				const replace = i.components[2].components[0].value.toLowerCase() === "true";
				const timeout = i.components[3].components[0].value.toLowerCase() === "true";
				const enforce = i.components[4].components[0].value.toLowerCase() === "true";
				let warning = "";
				if (logChannel === undefined) {
					warning = `Couldn't find a channel with an ID of ${logChannelID}`;
					logChannelID = settings.log_channel;
				}
				if (logChannelID !== settings.log_channel && logChannel) {
					if (isTextBasedChannel(logChannel)) {
						logChannel.send({ content: "Configured this channel as a log channel" }).catch(() => {
							warning = "Couldn't send a message to the log channel.";
						});
					} else {
						logChannelID = settings.log_channel;
					}
				}
				if (
					logChannelID !== settings.log_channel ||
					ephemeral !== settings.ephemeral ||
					replace !== settings.replace ||
					timeout !== settings.timeout ||
					enforce !== settings.enforce
				) {
					settings = { ...settings, ephemeral, replace, timeout, enforce, log_channel: logChannelID };
					await i.update({ embeds: [this.makeConfigEmbed(guild, settings, roles)] });
					await this.container.db`UPDATE jail_settings SET ${this.container.db(settings)} WHERE server_id = ${guild.id}`;
				} else {
					await i.update({});
				}
				if (warning) {
					await i.followUp({ content: warning, ephemeral: true });
				}
			})
			.catch(() => {});
		return settings;
	}

	private async addJailRole(interaction: ButtonInteraction, guild: Guild, roles: Array<[Role, Role]> | null): Promise<[Role, Role] | null> {
		const memRoles = roles ? roles.map(([role]) => role.id) : [];
		function getSelectMenus(filter?: string) {
			const { everyone } = guild.roles;
			const allRoles: MessageSelectOptionData[] = guild.roles.cache
				.filter((role) => {
					if (role.id === everyone.id || role.managed || (filter === undefined && memRoles.includes(role.id)) || filter === role.id) {
						return false;
					}
					return true;
				})
				.map((role) => {
					return {
						label: role.name,
						value: role.id,
						emoji: role.unicodeEmoji ?? undefined
					};
				});
			allRoles.sort((a, b) => {
				const textA = a.label.toUpperCase();
				const textB = b.label.toUpperCase();
				return textA < textB ? -1 : textA > textB ? 1 : 0;
			});
			if (filter === undefined && !memRoles.includes(everyone.id)) {
				allRoles.unshift({
					label: `${everyone.name} (no role required)`,
					value: everyone.id,
					emoji: everyone.unicodeEmoji ?? undefined,
					description: "If no other roles match, or if user has no roles."
				});
			}
			// split the roles into multiple select menus
			// TODO change this to single select once discord supports roles in select.
			const selectMenuLimit = 25;
			const selectMenus: Array<MessageSelectMenu> = [];
			let count = 0;
			for (let i = 0; i < allRoles.length && count < 5; i += selectMenuLimit, count++) {
				const chunk = allRoles.slice(i, i + selectMenuLimit);
				selectMenus.push(new MessageSelectMenu({ customId: `${interaction.id}select${i}`, placeholder: "Select a role", options: chunk }));
			}
			return selectMenus;
		}
		const selectMenus = getSelectMenus();
		const warning =
			"*Note: Due to discord limiting amount of options in select menus to 25 the roles have been split into multiple menus. Discord plans to change this at a later date.*";
		const msg = await interaction.reply({
			fetchReply: true,
			ephemeral: true,
			content: `**\`1/2\` Select a member role.**${selectMenus.length > 1 ? `\n\n${warning}` : ""}`,
			components: selectMenus.map((s) => new MessageActionRow({ components: [s] }))
		});
		if (!isMessageInstance(msg)) return null;

		const selectResponse1 = await msg.awaitMessageComponent({ componentType: "SELECT_MENU", time: Time.Minute * 2 }).catch(() => undefined);
		if (!selectResponse1) {
			try {
				await interaction.editReply({ components: [], content: "*Cancelled.*" });
			} catch {}
			return null;
		}
		const role1 = guild.roles.cache.get(selectResponse1.values[0]);
		if (!role1) return null;
		const content = `${role1.id === guild.roles.everyone.id ? "@everyone" : `<@&${role1.id}>`}`;
		await selectResponse1.update({
			content: `${content} will get [...]\n**\`2/2\` Select a jail role.**`,
			components: getSelectMenus(role1.id).map((s) => new MessageActionRow({ components: [s] }))
		});
		const selectResponse2 = await msg.awaitMessageComponent({ componentType: "SELECT_MENU", time: Time.Minute * 2 }).catch(() => undefined);
		if (!selectResponse2) {
			try {
				await interaction.editReply({ components: [], content: "*Cancelled.*" });
			} catch {}
			return null;
		}
		const role2 = guild.roles.cache.get(selectResponse2.values[0]);
		if (!role2) return null;
		await interaction.editReply({
			components: [],
			content: `*Success* <:tick:729059775173754960>\n${content} will get <@&${role2.id}> when jailed.`
		});
		return [role1, role2];
	}

	private async editJailMessage(
		interaction: ButtonInteraction,
		guild: Guild,
		settings: jailSettingsPayload,
		roles: Array<[Role, Role]> | null
	): Promise<string | null> {
		const modal = new Modal({
			customId: `${interaction.id}jailMessageModal`,
			title: "Edit Jail Message",
			components: [
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: interaction.id,
							label: "Current Jail Message",
							placeholder: "Leave empty to disable.",
							value: settings.jail_message ?? "",
							maxLength: 2500,
							style: "PARAGRAPH"
						})
					]
				})
			]
		});
		await interaction.showModal(modal);
		const newJailMessage = await interaction
			.awaitModalSubmit({ filter: (i) => i.customId === modal.customId, time: Time.Minute * 3 })
			.then(async (i) => {
				const val = i.components[0].components[0].value || null;
				if (val === settings.jail_message) {
					await i.update({});
				} else {
					settings.jail_message = val;
					await i.update({ embeds: [this.makeConfigEmbed(guild, settings, roles)] });
					await this.container.db`UPDATE jail_settings SET jail_message = ${val} WHERE server_id = ${guild.id}`;
				}
				return val;
			})
			.catch(() => undefined);
		if (newJailMessage === undefined || newJailMessage === settings.jail_message) {
			return settings.jail_message;
		}
		return newJailMessage;
	}

	private makeConfigEmbed(guild: Guild, settings: jailSettingsPayload, roles: Array<[Role, Role]> | null): Embed {
		let rolesStr = "*No jail roles configured*";
		if (roles) {
			rolesStr = roles
				.map(
					([memRole, jailRole]) =>
						`${memRole.id === guild.roles.everyone.id ? guild.roles.everyone.name : `<@&${memRole.id}>`} will get <@&${jailRole.id}>`
				)
				.join("\n");
		}
		const embed = new Embed({
			fields: [
				{
					name: "Current Jail Message",
					value:
						settings.jail_message === null
							? "*No jail message configured.*"
							: `
					The following message will be DMd to jailed users, unless the silent option is specified.
					${codeBlock("", _truncate(settings.jail_message, { length: 500 }))}
					Variables:
					\`{server}\` name of current server
					\`{time_left}\` time until user is freed automatically.
					\`{reason}\` reason given when jailed or 'N/A' if none.
					\`{jailer}\` nickname of the person that jailed the user.
					\`{user}\` nickname of jailed user.
					\u200b`
				},
				{
					name: "Jail-Roles",
					value: `
					If a user has multiple eligible roles the priority goes from top to down:
					${rolesStr}
					\u200b`
				},
				{
					name: "Default Behaviors",
					value: `
					log-channel: ${settings.log_channel ?? "" ? `<#${settings.log_channel}>` : "<:no:991161444898975824>"} 
					Send a message in channel when jailing: ${settings.ephemeral ? "<:no:991161444898975824>" : "<:tick:729059775173754960> "}
					Replace all roles with jail role: ${settings.replace ? "<:tick:729059775173754960>" : "<:no:991161444898975824>"}
					Timeout if jail duration specified: ${settings.timeout ? "<:tick:729059775173754960>" : "<:no:991161444898975824>"}
					Add jail role if a jailed user leaves the server and rejoins: ${settings.enforce ? "<:tick:729059775173754960>" : "<:no:991161444898975824>"}
					`
				}
			]
		});
		return embed;
	}

	private makeComponents(interaction: Command.ChatInputInteraction, jailRoles: Array<[Role, Role]> | null, selectedRole?: string) {
		const row1 = new MessageActionRow({
			components: [
				new MessageButton({ customId: `${interaction.id}defaults`, label: "Default Behaviors", style: "PRIMARY" }),
				new MessageButton({ customId: `${interaction.id}editMessage`, label: "Edit Jail Message", style: "PRIMARY" }),
				...supportLinkRow.components
			]
		});
		const selectRow = new MessageActionRow({
			components: [
				new MessageSelectMenu({
					customId: `${interaction.id}select`,
					placeholder: "Select a member-jail role pair.",
					options: [{ label: "\u200b", value: "\u200b" }],
					disabled: true
				})
			]
		});
		if (jailRoles) {
			jailRoles.slice(0, 15);
			selectRow.components[0].setOptions(
				jailRoles.map(([M, J]) => {
					const name = M.id === interaction.guild!.roles.everyone.id ? M.name : `@${M.name}`;
					return { label: `${name} → @${J.name}`, value: M.id, default: selectedRole !== undefined && selectedRole === M.id };
				})
			);
			selectRow.components[0].setDisabled(false);
		}
		const row2 = new MessageActionRow({
			components: [
				new MessageButton({
					customId: `${interaction.id}addRole`,
					label: "Add a Jail Role",
					style: "SUCCESS",
					disabled: jailRoles !== null && jailRoles.length >= 15
				}),
				new MessageButton({
					customId: `${interaction.id}remove;${selectedRole ?? ""}`,
					disabled: selectedRole === undefined,
					label: "Remove Role",
					style: "DANGER"
				})
			]
		});

		return [row1, selectRow, row2];
	}
}
