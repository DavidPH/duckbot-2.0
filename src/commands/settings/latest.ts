import { ApplicationCommandRegistry, Command, RegisterBehavior } from "@sapphire/framework";
import { isMessageInstance } from "@sapphire/discord.js-utilities";
import { GeneralErrors } from "../../lib/utils/errors";
import { Timestamp } from "@sapphire/time-utilities";
import Embed from "../../lib/utils/custom-embed";
import { MessageActionRow, MessageButton, Modal, TextInputComponent } from "discord.js";

export class LatestCommand extends Command {
	public override registerApplicationCommands(registry: ApplicationCommandRegistry) {
		registry.registerChatInputCommand(
			{
				name: "latest",
				description: "See the latest updates regarding DuckBot"
			},
			{
				idHints: ["986996222768345099", "995456190207242250"],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);

		registry.registerContextMenuCommand(
			{
				name: "Post as latest news.",
				type: "MESSAGE"
			},
			{
				idHints: ["986996222936117299", "995456190232412180"],
				guildIds: [this.container.env.duckbotServerID],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
	}

	public async contextMenuRun(interaction: Command.ContextMenuInteraction) {
		if (interaction.user.id !== this.container.env.ownerID) throw GeneralErrors.RESTRICTED_COMMAND;
		if (!interaction.isMessageContextMenu()) return;
		const timestamp = `Latest News - <:news:728706669617610832> ${new Timestamp("ll")}`;
		const { content } = interaction.targetMessage;
		const channel = await this.container.client.channels.fetch(this.container.env.latestChannelID);
		if (!channel || !channel.isText()) return;
		if (interaction.targetMessage.author.id === this.container.client.user?.id) {
			const message = interaction.targetMessage;
			if (!isMessageInstance(message)) return;
			const [embed] = message.embeds;
			if (embed !== null) {
				const modal = new Modal({
					customId: `${interaction.id}modal`,
					title: "Edit Latest",
					components: [
						new MessageActionRow({
							components: [
								new TextInputComponent({
									customId: `${interaction.id}title`,
									label: "Title",
									style: "SHORT",
									value: embed.title ?? ""
								})
							]
						}),
						new MessageActionRow({
							components: [
								new TextInputComponent({
									customId: `${interaction.id}description`,
									label: "Content",
									style: "PARAGRAPH",
									value: embed.description ?? ""
								})
							]
						})
					]
				});
				await interaction.showModal(modal);
				interaction
					.awaitModalSubmit({ filter: (i) => i.customId === modal.customId, time: 90000 })
					.then(async (i) => {
						await i.reply({ ephemeral: true, content: "Updated embed <:tick:729059775173754960>" });
						const title = i.components[0].components[0].value || timestamp;
						const description = i.components[1].components[0].value;
						const newEmbed = new Embed({ title, description });
						await message.edit({ embeds: [newEmbed] });
					})
					.catch(() => {});
			}
		} else {
			const embed = new Embed({ title: timestamp, description: content });
			const row = new MessageActionRow().addComponents(
				new MessageButton({
					label: "Post",
					style: "SUCCESS",
					customId: `${interaction.id}post`
				})
			);
			const msg = await interaction.reply({ content: "**Preview:**", ephemeral: true, embeds: [embed], components: [row], fetchReply: true });
			if (!isMessageInstance(msg)) return;
			msg.awaitMessageComponent({ componentType: "BUTTON", time: 15_000, filter: (i) => i.customId === `${interaction.id}post` })
				.then(async () => {
					await channel.send({ embeds: [embed] });
					await interaction.editReply({ components: [] });
				})
				.catch(async () => interaction.editReply({ components: [] }));
		}
	}

	public async chatInputRun(interaction: Command.ChatInputInteraction) {
		const channel = await this.container.client.channels.fetch(this.container.env.latestChannelID);
		if (!channel || !channel.isText()) return;
		const last_message = (await channel.messages.fetch({ limit: 1 })).at(0);
		if (!last_message || !isMessageInstance(last_message)) return;
		const supportLinkRow = new MessageActionRow().addComponents(
			new MessageButton({
				label: "Support Server",
				style: "LINK",
				url: this.container.env.duckbotServerInvite
			})
		);
		await interaction.reply({ ephemeral: true, embeds: [last_message.embeds[0]], components: [supportLinkRow] });
	}
}
