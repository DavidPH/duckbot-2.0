import { ApplicationCommandRegistry, Command, RegisterBehavior } from "@sapphire/framework";
import { DuckBotError, GeneralErrors, JailErrors, SignupErrors, supportLinkRow, UnknownErrorEmbed } from "../../lib/utils/errors";

export class InfoCommand extends Command {
	public override registerApplicationCommands(registry: ApplicationCommandRegistry) {
		registry.registerChatInputCommand(
			{
				name: "info",
				description: "Get extra information about certain aspects of DuckBot",
				options: [
					{
						name: "errorcode",
						description: "Get additional information about an error (e.g. fixed status)",
						type: "STRING",
						required: false
					}
				]
			},
			{
				idHints: ["985217660935606282", "995456192115658842"],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
	}

	public async chatInputRun(interaction: Command.ChatInputInteraction) {
		const errCode = interaction.options.getString("errorcode");
		if (errCode === null) {
			return interaction.reply({
				ephemeral: true,
				content: "Any issues? Join the support server:",
				components: [supportLinkRow]
			});
		}

		const customError = /(?<label>^[gGsSjJ])(?:[ -]*)(?<code>\d\d)$/;
		const snowFlake = /^(?<id>\d{17,19})$/;
		const labeled_code = errCode.match(customError);
		if (snowFlake.test(errCode)) {
			let fixed = true;
			let comment: string | null =
				"This is just an **example** of an unknown error. If there's any relevant updates to an error you can find it here, or in the support server.";
			if (errCode !== this.container.client.user?.id) {
				const [row] = await this.container.db<
					({ fixed: boolean; comment: string | null } | undefined)[]
				>`SELECT fixed, comment FROM errors WHERE error_id = ${errCode}`;
				if (row === undefined) {
					throw GeneralErrors.UNKNOWN_ERROR_NOT_FOUND.replaceStr("message", ["$1", errCode]);
				}
				comment = row.comment;
				fixed = row.fixed;
			}
			const [embed, row] = new UnknownErrorEmbed(errCode, fixed, comment).embed;
			await interaction.reply({ ephemeral: true, embeds: [embed], components: [row] });
			return;
		} else if (labeled_code) {
			let Errors: typeof JailErrors | typeof SignupErrors | typeof GeneralErrors | undefined;
			let err: DuckBotError | undefined = undefined;
			switch (labeled_code.groups?.label.toLowerCase()) {
				case "j":
					Errors = JailErrors;
					break;
				case "s":
					Errors = SignupErrors;
					break;
				case "g":
					Errors = GeneralErrors;
					break;
				default:
					Errors = undefined;
					break;
			}
			err = Errors?.[labeled_code.groups!.code];
			if (err) {
				const [embed, row] = err.infoEmbed;
				await interaction.reply({ ephemeral: true, embeds: [embed], components: [row] });
				return;
			}
		}
		throw GeneralErrors.ERROR_NOT_FOUND.replaceStr("message", ["$1", errCode]);
	}
}
