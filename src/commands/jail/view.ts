import { CommandInteraction, Formatters, MessageAttachment, MessageActionRow, MessageButton, MessageSelectMenu } from "discord.js";
import { ApplicationCommandRegistry, Command, RegisterBehavior, CommandOptionsRunTypeEnum, CommandOptions } from "@sapphire/framework";
import Embed, { PaginatedEmbed } from "../../lib/utils/custom-embed";
import { isMessageInstance, isGuildMember } from "@sapphire/discord.js-utilities";
import { historyPayload, makeJailPic } from "../../lib/utils/jail-utils";
import { DurationFormatter } from "@sapphire/time-utilities";
import { ApplyOptions } from "@sapphire/decorators";
import _truncate from "lodash/truncate";
import { DuckBotError } from "../../lib/utils/errors";
import { SlashCommandBuilder } from "@discordjs/builders";
import { PermissionFlagsBits } from "discord-api-types/v10";

@ApplyOptions<CommandOptions>({
	runIn: CommandOptionsRunTypeEnum.GuildText
})
export class ViewCommand extends Command {
	public override registerApplicationCommands(registry: ApplicationCommandRegistry) {
		registry.registerChatInputCommand(
			new SlashCommandBuilder()
				.setDefaultMemberPermissions(PermissionFlagsBits.ModerateMembers)
				.setDMPermission(false)
				.setName("view")
				.setDescription("View jail related information.")
				.addSubcommandGroup((g) =>
					g
						.setName("jail")
						.setDescription("View jail related information.")
						.addSubcommand((g) =>
							g
								.setName("info")
								.setDescription("View information about a currently jailed user.")
								.addUserOption((o) => o.setName("user").setDescription("The jailed user you wish to view info on.").setRequired(true))
						)
						.addSubcommand((g) =>
							g
								.setName("history")
								.setDescription("View jail history of the guild or of a user")
								.addUserOption((o) => o.setName("user").setDescription("View history of this user.").setRequired(true))
						)
				),
			{
				idHints: ["985217657303359508", "995456106094674020"],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
	}

	public async chatInputRun(interaction: Command.ChatInputInteraction) {
		const cmd_name = interaction.options.getSubcommand() as string;
		const cmd_grp = interaction.options.getSubcommandGroup() as string;
		switch (cmd_grp) {
			case "jail":
				if (cmd_name === "info") await this.jail_info(interaction);
				if (cmd_name === "history") await this.jail_hist(interaction);
				break;
		}
	}

	private async jail_info(interaction: CommandInteraction) {
		// TODO add visibility levels depending on if can jail or if can't jail.
		const member = interaction.options.getMember("user");
		if (!isGuildMember(member)) return;
		const guild = interaction.guild!;
		const inJail = await this.container.jail.isMemberJailed(member);
		const embed = new Embed().setTitle(member.displayName);
		const components = new MessageActionRow({ components: [new MessageButton({ style: "SUCCESS", customId: "history", label: "History" })] });
		const attach: MessageAttachment[] = [];

		if (!inJail.isInDB || !inJail.hasRole) {
			embed.setThumbnail(member.displayAvatarURL());
			embed.setDescription("Not in jail.");
			if (inJail.hasRole) {
				embed.addField("\u26A0 Warning", "User has a jail role but I have no memory of jailing the user.");
			}
			if (inJail.isInDB) {
				await this.container.db`DELETE FROM jail WHERE server_id = ${member.guild.id} AND member_id = ${member.id}`;
			}
		}

		if (inJail.isInDB && inJail.hasRole) {
			components.addComponents(new MessageButton({ style: "DANGER", customId: "free", label: "Release User" }));
			embed.setThumbnail("attachment://jail_image.png");
			attach.push(await makeJailPic(member));
			const formatter = new DurationFormatter();
			const now = new Date().getTime();
			const done = formatter.format(now - inJail.isInDB.jailed_at.getTime(), 3);
			const remaining = inJail.isInDB.release_at ? formatter.format(inJail.isInDB.release_at.getTime() - now, 3) : "Indefinite";
			let tag = inJail.isInDB.jailed_by;
			try {
				tag = (await guild.members.fetch(tag)).user.tag || tag;
			} catch {}
			embed.addFields(
				Embed.field("Jailed by:", tag, true),
				Embed.emptyField(true),
				Embed.field("Jailed at:", Formatters.time(inJail.isInDB.jailed_at, Formatters.TimestampStyles.ShortDateTime), true),
				Embed.field("Time done", done, true),
				Embed.emptyField(true),
				Embed.field("Time remaining", remaining, true),
				Embed.field("Reason:", inJail.isInDB.reason ?? "N/A", false),
				Embed.field("Previous roles:", inJail.isInDB.role_list.map((r) => `<@&${r}>`).join(", "), false)
			);
		}
		await interaction.reply({ ephemeral: true, embeds: [embed], files: attach, components: [components] });
		const msg = await interaction.fetchReply();
		if (!isMessageInstance(msg)) {
			return;
		}
		const collector = msg.createMessageComponentCollector({
			filter: (i) => {
				// TODO check permissions
				return i.user.id === interaction.user.id;
			},
			idle: 25_000
		});
		collector.on("collect", async (i) => {
			try {
				switch (i.customId) {
					case "free":
						components.components.at(1)!.setDisabled(true);
						await i.update({ components: [components] });
						await (async () => {
							const jailer = interaction.member;
							if (!isGuildMember(jailer)) return;
							const result = await this.container.jail.freeFromJail(jailer.user, member);
							if (result instanceof DuckBotError) {
								throw result;
							}
							await i.followUp({ ephemeral: true, content: `Released ${member} from jail.` });
						})();
						break;
					case "history":
						await i.update({ components: [], attachments: [] });
						await this.jail_hist(interaction);
				}
			} catch {}
		});

		collector.on("end", async (_, r) => {
			if (r === "idle") {
				for (const r of components.components) {
					r.setDisabled(true);
				}
				await interaction.editReply({ components: [components] });
			}
		});
	}

	private async jail_hist(interaction: CommandInteraction) {
		const member = interaction.options.getMember("user");
		if (!isGuildMember(member)) return;
		const pages = new PaginatedEmbed({ title: member.user.tag });
		pages.thumbnail = member.displayAvatarURL();
		pages.fieldsPageLimit = 4;
		const query = await this.container.db<historyPayload[]>`
		SELECT * FROM jail_history 
		WHERE server_id = ${member.guild.id} AND member_id = ${member.id}
		ORDER BY jailed_at DESC`;

		if (query.count <= 0) {
			pages.first.setDescription("No jail history");
			if (interaction.replied) {
				await interaction.editReply({ embeds: [pages.first] });
			} else {
				await interaction.reply({ ephemeral: true, embeds: [pages.first] });
			}
			return;
		}
		pages.addPage(new Embed());

		const selectMenu = new MessageSelectMenu({
			customId: "select",
			maxValues: 1,
			disabled: true
		});
		selectMenu.addOptions({ label: "Select", description: "Make a selection.", value: "select" });
		const moreInfoEmbeds: { [key: string]: Embed | undefined } = {};
		const selectMenus = [selectMenu];
		let i = 0;
		let totalJailTime = 0;
		let maxJailTime = 0;
		const formatter = new DurationFormatter();
		for (const row of query) {
			const j_at = Formatters.time(row.jailed_at, Formatters.TimestampStyles.ShortDateTime);
			const r_at = row.released_at ? Formatters.time(row.released_at, Formatters.TimestampStyles.ShortDateTime) : "N/A";

			if (i >= pages.fieldsPageLimit) {
				i = 0;
			}
			if (i === 0) {
				selectMenus.push(new MessageSelectMenu({ customId: "select", maxValues: 1 }));
				selectMenus[selectMenus.length - 1].addOptions({
					label: "Overview",
					description: "Swap to overview mode.",
					value: "overview",
					default: true
				});
			}

			pages.addField(
				`\`${i + 1}\``, // <= pages.fieldsPageLimit ? i : 1
				`**Jailed at:** ${j_at}\n` +
					`**by:** <@${row.jailed_by}>\n` +
					`${row.released_at ? `**Released at:** ${r_at}\n` : ""}` +
					`${row.released_by ?? "" ? `**by:** <@${row.released_by}>\n` : ""}` +
					`${row.reason ?? "" ? `**Reason:** ${_truncate(`${row.reason}`, { length: 30 })}` : ""}
			`,
				false
			);
			selectMenus[selectMenus.length - 1].addOptions({
				label: `${i + 1} - ${row.jailed_at.toLocaleString("en", {
					month: "long",
					day: "numeric",
					year: "numeric",
					hour: "numeric",
					minute: "numeric"
				})}`,
				description: "Date is in UTC format.",
				value: row.jailed_at.getTime().toString()
			});
			i++;
			const embed = new Embed({ title: j_at });
			if (row.released_at) {
				const dur = row.released_at.getTime() - row.jailed_at.getTime();
				totalJailTime += dur;
				maxJailTime = dur > maxJailTime ? dur : maxJailTime;
			}
			const done = row.released_at ? formatter.format(row.released_at.getTime() - row.jailed_at.getTime(), 3) : "N/A";
			let tag = row.jailed_by;
			try {
				tag = (await member.guild.members.fetch(row.jailed_by)).user.tag || "";
			} catch {}
			embed.addFields(
				Embed.field("Jailed by:", tag || row.jailed_by),
				Embed.field("Released by:", row.released_by ?? "" ? tag || "N/A" : "N/A"),
				Embed.field("Released at:", r_at),
				Embed.field("Time done: ", done),
				Embed.field("Reason:", row.reason ?? "N/A")
			);
			moreInfoEmbeds[row.jailed_at.getTime()] = embed;
		}

		pages.first.setDescription(`
		**Amount of times jailed: **${query.length}
		**Avg jail duration:** ${formatter.format(totalJailTime / query.length, 2)}
		**Longest jail duration:** ${formatter.format(maxJailTime, 2)}
		**Most recent jail:** ${Formatters.time(query[0].jailed_at, Formatters.TimestampStyles.RelativeTime)}
		`);

		await pages.paginate(interaction, {
			ephemeral: true,
			midBtn: { btn: new MessageButton({ customId: "delete", emoji: "<:delete:915583498956443648>", style: "PRIMARY" }), index: 2 },
			rows: [new MessageActionRow().setComponents([selectMenus[0]])],
			timeout: 90_000,
			callback: async (menu_int, { controlsRow, page }) => {
				switch (menu_int.customId) {
					case "delete":
						await menu_int.reply({
							content: `This will delete **ALL** of ${member}'s jail history.\nThis **can not** be undone. Are you sure?`,
							components: [
								new MessageActionRow({
									components: [new MessageButton({ style: "DANGER", label: "Yes, Delete", customId: "confirm" })]
								})
							],
							ephemeral: true
						});
						void menu_int.fetchReply().then((reply) => {
							if (!isMessageInstance(reply)) return;
							reply
								.awaitMessageComponent({
									filter: (int) => {
										return int.customId === "confirm" && int.user.id === interaction.user.id; // TODO: check for permissions
									},
									time: 10_000
								})
								.then(async (int) => {
									controlsRow.components.at(2)?.setDisabled(true);
									await this.container.db`
									DELETE FROM jail_history WHERE server_id = ${member.guild.id} AND member_id = ${member.id}`;
									await int.update({ components: [], content: `Deleted ${member.user.tag}'s jail history.` });
								})
								.catch(async () => {
									await menu_int.editReply({ components: [], content: "Cancelled." });
								});
						});
						return null;
					case "select":
						if (menu_int.isSelectMenu()) {
							const [value] = menu_int.values;
							for (const [, option] of selectMenus[page].options.entries()) {
								option.default = value === option.value;
							}
							const embed = moreInfoEmbeds[value] || pages.embeds[page];
							return {
								components: [new MessageActionRow({ components: [selectMenus[page]] }), controlsRow],
								embeds: [embed]
							};
						}
						return null;
					default:
						return { components: [new MessageActionRow({ components: [selectMenus[page]] }), controlsRow] };
				}
			}
		});
	}
}
