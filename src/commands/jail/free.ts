import { ApplicationCommandRegistry, Command, RegisterBehavior, CommandOptionsRunTypeEnum, CommandOptions } from "@sapphire/framework";
import { DurationFormatter } from "@sapphire/time-utilities";
import { isGuildMember } from "@sapphire/discord.js-utilities";
import { ApplyOptions } from "@sapphire/decorators";
import { DuckBotError } from "../../lib/utils/errors";
import { SlashCommandBuilder } from "@discordjs/builders";
import { PermissionFlagsBits } from "discord-api-types/v10";
import Embed from "../../lib/utils/custom-embed";

@ApplyOptions<CommandOptions>({
	runIn: CommandOptionsRunTypeEnum.GuildText
})
export class FreeCommand extends Command {
	public override registerApplicationCommands(registry: ApplicationCommandRegistry) {
		registry.registerChatInputCommand(
			new SlashCommandBuilder()
				.setName("free")
				.setDescription("Release someone from jail and restore their roles.")
				.addUserOption((option) => option.setName("user").setDescription("The user which you wish to release from jail.").setRequired(true))
				.addBooleanOption((option) => option.setName("silent").setDescription("Send a message in channel. Change default with /config"))
				.setDefaultMemberPermissions(PermissionFlagsBits.ModerateMembers)
				.setDMPermission(false),
			{
				idHints: ["985217658293194783", "995456188831506503"],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
	}

	public async chatInputRun(interaction: Command.ChatInputInteraction) {
		const jailer = interaction.member;
		const member = interaction.options.getMember("user", true);
		const silent = interaction.options.getBoolean("silent");
		if (!isGuildMember(member) || !isGuildMember(jailer)) return;
		const dFormatter = new DurationFormatter();
		const result = await this.container.jail.freeFromJail(jailer.user, member);
		if (result instanceof DuckBotError) {
			throw result;
		}

		// Make Embed
		const now = new Date();
		const embed = new Embed().setThumbnail(member.displayAvatarURL()).setTitle(`Released ${member.displayName} from jail.`.padEnd(40, " \u200b"));
		if (result) {
			embed
				.addFields(Embed.field("Time spent in jail:", dFormatter.format(now.getTime() - result.jailed_at.getTime(), 2)))
				.setFooter(`See /view jail history @${member.displayName} for more.`);
		} else {
			embed
				.addFields(Embed.field("\u26A0", "User had a jail role but no roles saved, removing jail role only."))
				.setFooter("This is caused by jailing without using the bot.");
		}
		if (result?.release_at) {
			if (now < result.release_at) {
				const diff = dFormatter.format(result.release_at.getTime() - now.getTime(), 2);
				embed.setDescription(`${diff} early`);
			}
		}
		const [{ ephemeral }] = await this.container.db<
			{ ephemeral: boolean }[]
		>`SELECT ephemeral FROM jail_settings WHERE server_id = ${member.guild.id}`;
		await interaction.reply({ ephemeral: silent ?? ephemeral, embeds: [embed] });
	}
}
