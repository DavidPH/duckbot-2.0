import { ApplicationCommandRegistry, Command, RegisterBehavior, CommandOptionsRunTypeEnum, CommandOptions } from "@sapphire/framework";
import { DuckBotError, JailErrors } from "../../lib/utils/errors";
import { isGuildMember } from "@sapphire/discord.js-utilities";
import { Duration, Time } from "@sapphire/time-utilities";
import { ApplyOptions } from "@sapphire/decorators";
import { Constants, TextChannel } from "discord.js";
import { SlashCommandBuilder } from "@discordjs/builders";
import { PermissionFlagsBits } from "discord-api-types/v10";
import _take from "lodash/take";

@ApplyOptions<CommandOptions>({
	runIn: CommandOptionsRunTypeEnum.GuildText
})
export class JailCommand extends Command {
	public override registerApplicationCommands(registry: ApplicationCommandRegistry) {
		registry.registerChatInputCommand(
			new SlashCommandBuilder()
				.setName("jail")
				.setDescription("Send someone to jail, or update someone's jail time.")
				.addUserOption((o) => o.setName("user").setDescription("The user which you wish to send to jail.").setRequired(true))
				.addStringOption((o) =>
					o.setName("duration").setDescription("Amount of time the user will be jailed. E.g., '1d 3h 45m' or '3h 45m' or '45m'")
				)
				.addIntegerOption((o) => o.setName("purge").setDescription("Delete X number of messages sent by the user in this channel."))
				.addStringOption((o) =>
					o.setName("reason").setDescription("Specify a reason for the jail. To be displayed in history and (optionally) to the user.")
				)
				.addBooleanOption((o) =>
					o.setName("silent").setDescription("Won't notify the user, or send a message in the channel. (Change default with /config)")
				)
				.setDefaultMemberPermissions(PermissionFlagsBits.ModerateMembers)
				.setDMPermission(false),
			{
				idHints: ["985217659815731240", "995456104819593226"],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
	}

	public async chatInputRun(interaction: Command.ChatInputInteraction) {
		const jailer = interaction.member;
		const member = interaction.options.getMember("user");
		const silent = interaction.options.getBoolean("silent");
		const reason = interaction.options.getString("reason");
		const channel = interaction.channel as TextChannel;
		const durStr = interaction.options.getString("duration");
		let purge = interaction.options.getInteger("purge");

		if (!isGuildMember(jailer) || !isGuildMember(member)) return;
		if (!member.manageable) {
			throw JailErrors.NOT_MANAGEABLE;
		}

		const duration = durStr === null ? null : new Duration(durStr);
		if (duration) {
			if (isNaN(duration.offset)) {
				throw JailErrors.INVALID_DURATION;
			}
			if (duration.offset >= Time.Year) {
				throw JailErrors.MAX_DURATION.replaceStr("message", ["$1", "1 year"]);
			}
			if (duration.offset < Time.Minute) {
				throw JailErrors.MIN_DURATION.replaceStr("message", ["$1", "1 minute"]);
			}
		}
		const result = await this.container.jail.sendToJail(jailer, member, duration?.offset ?? null, reason);
		if (result instanceof DuckBotError) {
			throw result;
		}
		const { settings } = result;
		await interaction.reply({ embeds: [result.jailEmbed], files: [result.jailPic], ephemeral: silent ?? settings.ephemeral });

		// Send jail DM
		if (!(silent ?? false) && result.type === "INSERT") {
			// Null if DM is disabled
			if (result.jailDM !== null && result.jailDM !== "") {
				await member.send(result.jailDM).catch((error) => {
					// Ignore error if message can't be sent (user blocked bot) otherwise let it bubble up.
					if (error.code !== Constants.APIErrors.CANNOT_MESSAGE_USER) {
						throw error;
					}
				});
			}
		}

		// Purge messages if specified
		if (purge !== null) {
			purge = Math.min(Math.max(purge, 1), 100);
			const messages = _take(
				(await channel!.messages.fetch({ limit: 100 }))
					.filter((msg) => msg.author.id === member.id) // Only messages sent by jailed user.
					.map((msg) => msg.id), // Convert to array
				purge
			);
			const purge_count = messages.length;
			if (purge_count > 0) {
				await channel.bulkDelete(messages, true);
				await interaction.followUp({ ephemeral: true, content: `Found and deleted ${purge_count} messages sent by ${member.user}` });
			} else {
				// TODO: Change this to an error?
				await interaction.followUp({
					ephemeral: true,
					content: `No messages deleted.\nThis channel's last 100 messages don't have any sent by ${member.user}.`
				});
			}
		}
	}
}
