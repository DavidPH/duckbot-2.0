import { ApplicationCommandRegistry, Command, RegisterBehavior, CommandOptions, container } from "@sapphire/framework";
import { Message, MessageActionRow, Modal, ModalSubmitInteraction, TextInputComponent } from "discord.js";
import { isMessageInstance } from "@sapphire/discord.js-utilities";
import { codeBlock, isThenable } from "@sapphire/utilities";
import { ApplyOptions } from "@sapphire/decorators";
import { setTimeout } from "node:timers/promises";
import { Stopwatch } from "@sapphire/stopwatch";
import { inspect } from "node:util";

export interface eval_opts {
	f: string; // Filename
	e: boolean; // Ephemeral
	t: number; // timeout
	d: number; // depth
	h: boolean; // hidden
	j: boolean; // json
}

@ApplyOptions<CommandOptions>({
	preconditions: ["OwnerOnly"]
})
export class EvalCommand extends Command {
	public override registerApplicationCommands(registry: ApplicationCommandRegistry) {
		registry.registerChatInputCommand(
			{
				name: "eval",
				description: "Evaluates js or sql code.",
				options: [
					{
						name: "js",
						description: "Evaluates as JavaScript code.",
						type: "SUB_COMMAND",
						options: [
							{
								name: "filename",
								description: "Output as a file",
								type: "STRING",
								required: false
							},
							{
								name: "ephemeral",
								description: "Reply as ephemeral message, defaults to true.",
								type: "BOOLEAN",
								required: false
							},
							{
								name: "timeout",
								description: "Timeout after X seconds",
								type: "NUMBER",
								minValue: 0,
								required: false
							},
							{
								name: "depth",
								description: "Number of levels to recurse into returned object.",
								type: "INTEGER",
								minValue: 1,
								required: false
							},
							{
								name: "showhidden",
								description: "Show non-enumerable properties on returned object.",
								type: "BOOLEAN",
								required: false
							},
							{
								name: "json",
								description: "Output using JSON.stringify instead of utils.inspect",
								type: "BOOLEAN",
								required: false
							}
						]
					},
					{
						name: "sql",
						description: "Evalutes as a PostgreSQL query.",
						type: "SUB_COMMAND",
						options: [
							{
								name: "ephemeral",
								description: "Reply as ephemeral message, defaults to true.",
								type: "BOOLEAN",
								required: false
							}
						]
					}
				]
			},
			{
				idHints: ["985927977965605004", "995456193738842113"],
				guildIds: [this.container.env.duckbotServerID],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
		registry.registerContextMenuCommand(
			{
				name: "Eval codeblock.",
				type: "MESSAGE"
			},
			{
				idHints: ["986728014559727646", "995456195248787486"],
				guildIds: [this.container.env.duckbotServerID],
				behaviorWhenNotIdentical: RegisterBehavior.Overwrite
			}
		);
	}

	public async contextMenuRun(interaction: Command.ContextMenuInteraction) {
		if (!interaction.isMessageContextMenu()) return;
		await interaction.deferReply();
		const message = interaction.targetMessage;
		if (!isMessageInstance(message)) return;
		const { code, isSQL } = EvalCommand.stripCodeBlock(message.content);

		if (isSQL) {
			const { result, success, time } = await EvalCommand.sql(code);
			await interaction.followUp(EvalCommand.formatOutput(isSQL, false, false, code, success, result, time, "", message));
		} else {
			const { result, success, time } = await EvalCommand.timedEval(
				code,
				{ f: "", e: false, t: 10, d: Infinity, h: false, j: false },
				10,
				undefined,
				message
			);
			await interaction.followUp(EvalCommand.formatOutput(isSQL, false, false, code, success, result, time, "", message));
		}
	}

	public async chatInputRun(interaction: Command.ChatInputInteraction) {
		const opts: eval_opts = {
			f: interaction.options.getString("filename") ?? "",
			e: interaction.options.getBoolean("ephemeral") ?? true,
			t: interaction.options.getNumber("timeout") ?? 10,
			d: interaction.options.getInteger("depth") ?? Infinity,
			h: interaction.options.getBoolean("showhidden") ?? false,
			j: interaction.options.getBoolean("json") ?? false
		};
		const subCommand = interaction.options.getSubcommand(true);
		await interaction.showModal(
			new Modal({
				customId: `eval;${subCommand};${JSON.stringify(opts)}`,
				title: "Eval",
				components: [
					new MessageActionRow({
						components: [
							new TextInputComponent({
								label: "string to eval",
								style: "PARAGRAPH",
								customId: "evalInput",
								value: (await this.container.redis.get(`eval${subCommand}`)) ?? ""
							})
						]
					})
				]
			})
		);
	}

	public static async sql(query: string) {
		const stopwatch = new Stopwatch();
		let success: boolean;
		let time: string;
		let result: unknown;

		try {
			const q = await container.db.unsafe(query);
			result = q;
			time = stopwatch.toString();
			try {
				if (q.length === 0) {
					result = `${q.command} ${q.count}`;
				}
			} catch {}
			success = true;
		} catch (error) {
			time = stopwatch.toString();
			if (error instanceof Error) result = error.stack;
			else result = inspect(error, { depth: Infinity });
			success = false;
		}
		stopwatch.stop();
		return {
			success,
			time: EvalCommand.formatTime(time),
			result: typeof result === "string" ? result : JSON.stringify(result, null, 2)
		};
	}

	public static async timedEval(code: string, opts: eval_opts, time: number, interaction?: ModalSubmitInteraction, message?: Message) {
		if (time === Infinity || time === 0) return this.eval(code, opts, interaction, message);
		time *= 1000;
		return Promise.race([
			setTimeout(time).then(() => ({
				success: false,
				time: "",
				result: `Timed out after ${time}ms`
			})),
			EvalCommand.eval(code, opts, interaction, message)
		]);
	}

	public static async eval(code: string, opts: eval_opts, interaction?: ModalSubmitInteraction, message?: Message) {
		const stopwatch = new Stopwatch();
		let success: boolean;
		let syncTime = "";
		let asyncTime = "";
		let result: unknown;
		let thenable = false;

		if (code.match("await")) {
			code = `(async () => {\n${code}\n})();`;
		}
		try {
			// eslint-disable-next-line @typescript-eslint/no-unused-vars
			const _int = interaction;
			// eslint-disable-next-line @typescript-eslint/no-unused-vars
			const _msg = message;
			// eslint-disable-next-line @typescript-eslint/no-unused-vars
			const _mem = (interaction || message)?.member;
			// eslint-disable-next-line @typescript-eslint/no-unused-vars
			const _user = interaction?.user || message?.author;
			// eslint-disable-next-line @typescript-eslint/no-unused-vars
			const _client = container.client;
			// eslint-disable-next-line @typescript-eslint/no-unused-vars
			const _container = container;
			// eslint-disable-next-line @typescript-eslint/no-unused-vars
			const _guild = (interaction || message)?.guild;
			// eslint-disable-next-line @typescript-eslint/no-unused-vars
			const _db = container.db;
			// eslint-disable-next-line no-eval
			result = eval(code);
			(await _guild?.members.fetch())?.map((m) => m.displayName);

			syncTime = stopwatch.toString();
			if (isThenable(result)) {
				thenable = true;
				stopwatch.restart();
				result = await result;
				asyncTime = stopwatch.toString();
			}
			success = true;
		} catch (error) {
			if (!syncTime.length) syncTime = stopwatch.toString();
			if (thenable && !asyncTime.length) asyncTime = stopwatch.toString();
			result = error;
			success = false;
		}
		stopwatch.stop();
		if (typeof result !== "string") {
			if (result instanceof Error) {
				result = result.stack;
			} else {
				result = opts.j
					? JSON.stringify(result, null, 4)
					: inspect(result, { depth: opts.d, showHidden: opts.h, maxArrayLength: null, maxStringLength: null });
			}
		}
		return {
			success,
			time: this.formatTime(syncTime, asyncTime ?? ""),
			result: result as string
		};
	}

	public static formatOutput(
		isSQL: boolean,
		ephemeral: boolean,
		isSlashCMD: boolean,
		code: string,
		success: boolean,
		result: string,
		time: string,
		filename: string,
		message?: Message
	) {
		const emoji = success ? "<:tick:729059775173754960>" : "❌";
		const msg_url = message?.url ?? "" ? `[jump to message](<${message?.url}>)\n` : "";
		code = isSlashCMD ? `**Ran:**${codeBlock(isSQL ? "postgresql" : "js", code)}\n` : msg_url;
		if ((result && result.length > 2000) || filename !== "") {
			return {
				content: `${code}**Output(${emoji}):**\n${time}`,
				ephemeral,
				files: [{ attachment: Buffer.from(result), name: filename || "output.js" }]
			};
		}
		return {
			content: result ? `${code}**Output (${emoji}):**\n${codeBlock("js", result)}\n${time}` : `(${emoji})${time} ${code}`,
			ephemeral
		};
	}

	private static formatTime(syncTime: string, asyncTime?: string) {
		return asyncTime ?? "" ? `⏱ ${asyncTime}<${syncTime}>` : `⏱ ${syncTime}`;
	}

	private static stripCodeBlock(message: string): { code: string; isSQL: boolean } {
		const re = /(?<!\\)(?<start>```)(?<=```)(?:(?<lang>[a-z][a-z0-9]*)\s)?(?<content>.*?)(?<!\\)(?=```)(?<end>(?:\\\\)*```)/s;
		const match = message.match(re);
		let code = message;
		let isSQL = false;
		if (match) {
			code = match.groups?.content ?? "";
			isSQL = (match.groups?.lang?.toLowerCase()?.match("sql") ?? null) !== null;
			if (!isSQL) {
				const isError = (match.groups?.lang?.toLowerCase()?.match("error") ?? null) !== null;
				if (isError) throw new Error(code);
			}
		}
		return { code, isSQL };
	}
}
