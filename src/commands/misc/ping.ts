import { ApplicationCommandRegistry, Command, RegisterBehavior, CommandOptionsRunTypeEnum, CommandOptions } from "@sapphire/framework";
import { ApplyOptions } from "@sapphire/decorators";
import { isMessageInstance } from "@sapphire/discord.js-utilities";

@ApplyOptions<CommandOptions>({
	runIn: CommandOptionsRunTypeEnum.GuildAny
})
export class PingCommand extends Command {
	public override registerApplicationCommands(registry: ApplicationCommandRegistry) {
		registry.registerChatInputCommand(
			{
				name: "ping",
				description: "Pongs when pinged."
			},
			{
				idHints: ["985217744309981254"],
				registerCommandIfMissing: this.container.env.isDev,
				behaviorWhenNotIdentical: RegisterBehavior.LogToConsole
			}
		);
	}

	public async chatInputRun(interaction: Command.ChatInputInteraction) {
		const msg = await interaction.reply({ content: "Ping?", ephemeral: true, fetchReply: true });
		if (isMessageInstance(msg)) {
			const diff = msg.createdTimestamp - interaction.createdTimestamp;
			const ping = Math.round(this.container.client.ws.ping);
			return interaction.editReply(`Pong 🏓! (Round trip took: ${diff}ms. Heartbeat: ${ping}ms.)`);
		}
		return interaction.editReply("Failed to retrieve ping :(");
	}
}
