import { Command, CommandOptionsRunTypeEnum, CommandOptions } from "@sapphire/framework";
import { ApplyOptions } from "@sapphire/decorators";
import { supportLinkRow } from "../../lib/utils/errors";
import type { Message } from "discord.js";
import Embed from "../../lib/utils/custom-embed";

@ApplyOptions<CommandOptions>({
	name: "toggleBeta",
	cooldownDelay: 10_000,
	cooldownLimit: 2,
	requiredUserPermissions: ["ADMINISTRATOR"],
	runIn: CommandOptionsRunTypeEnum.GuildAny,
	enabled: false
})
export class BetaOptInCommand extends Command {
	public async messageRun(message: Message) {
		const { guild } = message;
		if (!guild) return;
		const [{ exists }] = await this.container.db<{ exists: boolean }[]>`
        SELECT EXISTS(SELECT 1 from beta_opt_in WHERE server_id = ${guild.id}) AS "exists" `;
		if (exists) {
			await this.container.db`
		DELETE FROM beta_opt_in WHERE server_id = ${guild.id}`;
			const embed = new Embed({
				title: "Disabled Beta.",
				description:
					"If there were any difficulties, or annoyances with the Beta or any feedback you'd like to give please tell me in the support server."
			});
			await message.channel.send({
				embeds: [embed],
				components: [supportLinkRow]
			});
		} else {
			await this.container.db`
            INSERT INTO beta_opt_in VALUES (${guild.id})
			ON CONFLICT DO NOTHING`;
			const embed = new Embed({
				title: "Enabled Beta!",
				description: `If you wish to use jail commands please run \`/config\`
**IMPORTANT** Make sure to modify command permissions to your liking under your server settings. By default only people with moderate members can use jail, and manage events can use signups.`,
				image: { url: "https://i.imgur.com/XrZ9Q8x.png" }
			});
			await message.channel.send({
				embeds: [embed],
				components: [supportLinkRow]
			});
		}
	}
}
