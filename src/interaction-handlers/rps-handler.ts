import { InteractionHandler, InteractionHandlerTypes, PieceContext } from "@sapphire/framework";
import { ButtonInteraction, MessageActionRow, MessageButton, MessageEmbed } from "discord.js";

export default class extends InteractionHandler {
	constructor(ctx: PieceContext) {
		super(ctx, { interactionHandlerType: InteractionHandlerTypes.Button });
	}

	public async run(interaction: ButtonInteraction, parsedData: InteractionHandler.ParseResult<this>) {
		const { option, player1, player2 } = parsedData;
		const isPlayer1 = interaction.user.id === player1.id;
		const isPlayer2 = interaction.user.id === player2.id;
		if (!isPlayer1 && !isPlayer2) {
			return interaction.reply({ content: "You're not part of this game!", ephemeral: true });
		}

		if ((isPlayer1 && player1.picked !== "x") || (isPlayer2 && player2.picked !== "x")) {
			return interaction.reply({ content: "No changing your mind!", ephemeral: true });
		}

		const row = interaction.message.components?.at(0);
		const newRow = new MessageActionRow();
		if (!row) return;
		if (isPlayer1 && player1.picked === "x") {
			player1.picked = option;
		} else if (isPlayer2 && player2.picked === "x") {
			player2.picked = option;
		}
		for (const [i, component] of row.components.entries()) {
			if (component.type !== "BUTTON") return;
			const newButton = new MessageButton(component);
			newButton.customId = `rps;${player1.id}${player1.picked};${player2.id}${player2.picked};${i}`;
			newRow.addComponents(newButton);
		}
		const embed = new MessageEmbed(interaction.message.embeds[0]);
		const emojis = ["✊", "✋", "✌"];
		const results = ["\u270a **rock** smashes **scissors**", "\u270b **paper** covers **rock**", "\u270c **scissors** cuts **paper**"];
		let gameOver = false;
		if (player1.picked !== "x" && player2.picked !== "x") {
			gameOver = true;
			const p1 = parseInt(player1.picked, 10);
			const p2 = parseInt(player2.picked, 10);
			embed.fields[0].value = emojis[p1];
			embed.fields[2].value = emojis[p2];
			// Player 2 won
			if ((p1 + 1) % 3 === p2) {
				const mem = interaction.guild?.members.cache.get(player2.id);
				if (mem) {
					embed.setThumbnail(mem.displayAvatarURL());
				}
				embed.fields[3].value = `<@${player2.id}> wins!\n${results[p2]}\n`;
			}
			// It's a draw
			else if (p1 === p2) {
				embed.fields[3].value = "It's a draw!";
			}
			// Player 1 won
			else {
				const mem = interaction.guild?.members.cache.get(player1.id);
				if (mem) {
					embed.setThumbnail(mem.displayAvatarURL());
				}
				embed.fields[3].value = `<@${player1.id}> wins!\n${results[p1]}\n`;
			}
		} else if (isPlayer1 && player1.picked !== "x") {
			embed.fields[0].value = "||No Peeking!||";
		} else if (isPlayer2 && player2.picked !== "x") {
			embed.fields[2].value = "||No Peeking!||";
		}
		await interaction.update({ components: gameOver ? [] : [newRow], embeds: [embed] });
	}

	public parse(interaction: ButtonInteraction) {
		if (!interaction.guild) return this.none();
		const split = interaction.customId.split(";");
		if (split[0] === "rps") {
			return this.some({
				player1: { id: split[1].slice(0, -1), picked: split[1].slice(-1) },
				player2: { id: split[2].slice(0, -1), picked: split[2].slice(-1) },
				option: split[3]
			});
		}
		return this.none();
	}
}
