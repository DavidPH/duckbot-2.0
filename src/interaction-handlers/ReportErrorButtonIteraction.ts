import { InteractionHandler, InteractionHandlerTypes, PieceContext } from "@sapphire/framework";
import { ButtonInteraction, MessageActionRow, MessageButton, MessageEmbed, Modal, TextInputComponent } from "discord.js";
import { GeneralErrors } from "../lib/utils/errors";

export default class extends InteractionHandler {
	constructor(ctx: PieceContext) {
		super(ctx, { interactionHandlerType: InteractionHandlerTypes.Button });
	}

	public async run(interaction: ButtonInteraction, parsedData: InteractionHandler.ParseResult<this>) {
		const isOwner = interaction.user.id === this.container.env.ownerID;
		switch (parsedData.action) {
			case "ignore":
				if (typeof parsedData.args[0] === "string") {
					const { count } = await this.container.db`
					INSERT INTO ignore_list VALUES(${parsedData.args[0]}) 
					ON CONFLICT(snowflake_id) DO NOTHING`;
					await interaction.reply({ ephemeral: true, content: count > 0 ? "Ignored user" : "User is already ignored" });
				}
				break;
			case "addMsg":
				if (parsedData.args[0] === "dev" && !isOwner) {
					throw GeneralErrors.RESTRICTED_COMMAND;
				}
				await this.addMsg(interaction, parsedData.args[0] === "dev", parsedData.args[1]);
				break;
			case "fix":
				if (!isOwner) {
					throw GeneralErrors.RESTRICTED_COMMAND;
				}
				await this.toggleFixed(interaction, parsedData.args[0] === "true", parsedData.args[1]);
				break;
			case "info":
				if (!isOwner) {
					throw GeneralErrors.RESTRICTED_COMMAND;
				}
				await this.toggleReporting(interaction, parsedData.args[0] === "true", parsedData.args[1]);
				break;
		}
	}

	public parse(interaction: ButtonInteraction) {
		const opts = interaction.customId.split(";");
		if (opts[0] === "ignore") {
			if (interaction.user.id !== this.container.env.ownerID) {
				throw GeneralErrors.RESTRICTED_COMMAND;
			}
			return this.some({ action: opts[0], args: [opts[2]] });
		}
		if (opts.length >= 3 && opts[0] === "error") {
			const [, action, ...args] = opts;
			return this.some({ action, args });
		}
		return this.none();
	}

	private async addMsg(interaction: ButtonInteraction, devMsg: boolean, errorID: string) {
		const modal = new Modal({
			customId: `error;${errorID}`,
			title: "Add Information",
			components: []
		});
		if (devMsg) {
			modal.addComponents(
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: "dev",
							style: "PARAGRAPH",
							label: "Add Error Info"
						})
					]
				})
			);
		} else {
			modal.addComponents(
				new MessageActionRow({
					components: [
						new TextInputComponent({
							customId: "user",
							style: "PARAGRAPH",
							label: "Submit additional information",
							placeholder: "Steps you took that led to this error occurring, or other useful info about it you'd like to submit."
						})
					]
				})
			);
		}
		await interaction.showModal(modal);
	}

	private async toggleFixed(interaction: ButtonInteraction, fixed: boolean, errorID: string) {
		await this.container.db`
		UPDATE errors SET fixed=${fixed} WHERE error_id = ${errorID}`;
		const [embed] = interaction.message.embeds;
		const newEmbed = new MessageEmbed(embed).setColor(fixed ? "#4BB543" : "#FF3C00");
		(interaction.message.components?.at(0)?.components.at(0) as MessageButton).setCustomId(`error;fix;${!fixed};${errorID}`);
		const row = new MessageActionRow(interaction.message.components?.at(0));
		await interaction.update({ embeds: [newEmbed], components: [row] });
	}

	private async toggleReporting(interaction: ButtonInteraction, report: boolean, errorID: string) {
		await this.container.db`
		UPDATE errors SET want_info=${report} WHERE error_id = ${errorID}`;
		(interaction.message.components?.at(0)?.components.at(1) as MessageButton).setCustomId(`error;info;${!report};${errorID}`);
		const row = new MessageActionRow(interaction.message.components?.at(0));
		const embed = new MessageEmbed({ color: "#4BB543" });
		embed.setDescription(`**${report ? "Enabled" : "Disabled"} additional reports for this error.**`);
		await interaction.update({ components: [row] });
		await interaction.followUp({ ephemeral: true, embeds: [embed] });
	}
}
