import { InteractionHandler, InteractionHandlerTypes, PieceContext } from "@sapphire/framework";
import type { ButtonInteraction } from "discord.js";
import Signup from "../lib/utils/signup-utils";

export default class extends InteractionHandler {
	constructor(ctx: PieceContext) {
		super(ctx, { interactionHandlerType: InteractionHandlerTypes.Button });
	}

	public async run(interaction: ButtonInteraction, parsedData: InteractionHandler.ParseResult<this>) {
		await Signup.addToSignup(parsedData, interaction);
	}

	public parse(interaction: ButtonInteraction) {
		if (!interaction.guild) return this.none();
		const match = interaction.customId.match(/(?<=^section)[0-4]/);
		if (match) {
			return this.some(parseInt(match[0], 10));
		}
		return this.none();
	}
}
