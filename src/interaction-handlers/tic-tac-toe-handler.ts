import { InteractionHandler, InteractionHandlerTypes, PieceContext } from "@sapphire/framework";
import { ButtonInteraction, MessageActionRow, MessageButton } from "discord.js";
import _sample from "lodash/sample";

export default class extends InteractionHandler {
	constructor(ctx: PieceContext) {
		super(ctx, { interactionHandlerType: InteractionHandlerTypes.Button });
	}

	public async run(interaction: ButtonInteraction, parsedData: InteractionHandler.ParseResult<this>) {
		const components = interaction.message.components!;
		const rows: MessageActionRow[] = [];
		let index = 0;
		if (interaction.user.id !== parsedData.player1) {
			return interaction.reply({ ephemeral: true, content: `It is <@${parsedData.player1}>'s turn.` });
		}
		let success = false;
		const boardState: string[] = [];
		for (const row of components) {
			rows.push(new MessageActionRow());
			for (const component of row.components) {
				if (component.type !== "BUTTON") return;
				const newButton = new MessageButton(component);
				let newID = newButton.customId!;
				if (parsedData.player2 !== this.container.client.user!.id) {
					newID = `ttt;${parsedData.player2};${parsedData.player1};${index};${parsedData.piece === "x" ? "o" : "x"}`;
				}
				let oldPiece = (newButton.customId ?? "")[0];
				if (oldPiece !== "x" && oldPiece !== "o") newButton.setCustomId(newID);
				if (index === parsedData.index && !newButton.disabled) {
					oldPiece = parsedData.piece;
					newButton.setStyle("PRIMARY");
					newButton.setDisabled(true);
					newButton.setEmoji(oldPiece === "x" ? "❌" : "⭕");
					newButton.setCustomId(parsedData.piece + index);
					success = true;
				}
				boardState.push(oldPiece === "x" || oldPiece === "o" ? oldPiece : "");
				rows[rows.length - 1].addComponents(newButton);

				index++;
			}
		}

		if (!success) return;
		let result = checkWin(boardState, rows.length, parsedData.index, parsedData.piece);
		let winner: null | string = result === true ? parsedData.player1 : null;
		// bot move
		if (result === null && parsedData.player2 === this.container.client.user!.id) {
			let bestScore = -Infinity;
			let move: number | null = null;
			const moves: number[] = [];
			for (const [i, board] of getPossibleMoves(boardState, "o")) {
				if (rows.length === 3) {
					const score = minimax(board, rows.length, i, false);
					if (score > bestScore) {
						bestScore = score;
						move = i;
					}
				} else {
					moves.push(i);
				}
			}
			if (moves.length > 0) {
				move = _sample(moves) ?? null;
			}
			if (move !== null) {
				boardState[move] = "o";
				const x = Math.floor(move / rows.length);
				const y = move % rows.length;
				const button = rows[x].components[y] as MessageButton;
				button.setStyle("PRIMARY");
				button.setDisabled(true);
				button.setEmoji("⭕");
				button.setCustomId(`o${move}`);
				result = checkWin(boardState, rows.length, move, "o");
			}
			if (result === true) {
				winner = parsedData.player2;
			}
		}

		let resultText = "";
		if (result !== null) {
			if (result) {
				rows.forEach((r) => r.components.forEach((r) => (r as MessageButton).setDisabled(true)));
				resultText = `\n<@${winner}> wins!`;
			} else {
				resultText = "\nIt's a tie!";
			}
		}

		await interaction.update({
			content: `${interaction.message.content}${resultText}`,
			components: rows
		});
	}

	public parse(interaction: ButtonInteraction) {
		const split = interaction.customId.split(";");
		// `ttt;${interaction.user.id};${challenged.id};${rows.length - 1};${i};x`
		if (split[0] === "ttt") {
			return this.some({
				player1: split[1],
				player2: split[2],
				index: parseInt(split[3], 10),
				piece: split[4]
			});
		}
		return this.none();
	}
}

function* getPossibleMoves(board: string[], player: string): Generator<[number, string[]]> {
	for (const [i, piece] of board.entries()) {
		if (piece === "") {
			const temp = board.slice();
			temp[i] = player;
			yield [i, temp];
		}
	}
}

function checkWin(board: string[], n: number, index: number, player: string): boolean | null {
	const x = Math.floor(index / n);
	const y = index % n;
	const counts = [0, 0, 0, 0];

	for (let i = 0; i < n; i++) {
		if (board[x * n + i] === player) {
			counts[0]++;
		}
		if (board[i * n + y] === player) {
			counts[1]++;
		}
		if (board[i * n + i] === player) {
			counts[2]++;
		}
		if (board[i * n + (n - (i + 1))] === player) {
			counts[3]++;
		}
	}

	for (const count of counts) {
		if (count === n) {
			return true;
		}
	}
	for (const square of board) {
		if (square === "") return null;
	}

	return false;
}

function minimax(board: string[], n: number, index: number, isMaximizing: boolean, alpha = -Infinity, beta = Infinity): number {
	board = board.slice();
	const piece = isMaximizing ? "x" : "o";
	const result = checkWin(board, n, index, piece);

	if (result !== null) {
		return result ? (isMaximizing ? -10 : 10) : 0;
	}

	if (isMaximizing) {
		let bestScore = -Infinity;
		for (const [i, newBoard] of getPossibleMoves(board, "o")) {
			const score = minimax(newBoard, n, i, false, alpha, beta);
			bestScore = Math.max(score, bestScore);
			alpha = Math.max(alpha, score);
			if (beta <= alpha) {
				break;
			}
		}
		return bestScore;
	}
	// Is Minimizing
	let bestScore = Infinity;
	for (const [i, newBoard] of getPossibleMoves(board, "x")) {
		const score = minimax(newBoard, n, i, true, alpha, beta);
		bestScore = Math.min(score, bestScore);
		beta = Math.min(beta, score);
		if (beta <= alpha) {
			break;
		}
	}
	return bestScore;
}
