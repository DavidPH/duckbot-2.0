import { InteractionHandler, InteractionHandlerTypes, PieceContext } from "@sapphire/framework";
import { MessageActionRow, MessageButton, MessageEmbed, ModalSubmitInteraction, TextChannel } from "discord.js";
import { EvalCommand, eval_opts } from "../commands/misc/eval";
import { GeneralErrors } from "../lib/utils/errors";

export default class extends InteractionHandler {
	constructor(ctx: PieceContext) {
		super(ctx, { interactionHandlerType: InteractionHandlerTypes.ModalSubmit });
	}

	public async run(interaction: ModalSubmitInteraction, parsedData: InteractionHandler.ParseResult<this>) {
		const feedbackEmbed = new MessageEmbed({
			color: "ORANGE",
			fields: [
				{ name: "User ID", value: interaction.user.id, inline: true },
				{ name: "\u200b", value: "\u200b", inline: true },
				{ name: "Username", value: interaction.user.tag, inline: true },
				{ name: "Guild ID", value: interaction.guild?.id ?? "\u200b", inline: true },
				{ name: "\u200b", value: "\u200b", inline: true },
				{ name: "Guild Name", value: interaction.guild?.name ?? "\u200b", inline: true },
				{ name: "Channel ID", value: interaction.channel?.id ?? "\u200b", inline: true },
				{ name: "\u200b", value: "\u200b", inline: true },
				{ name: "Channel Name", value: (interaction.channel as TextChannel)?.name ?? "\u200b", inline: true }
			]
		});

		const feedbackSuccessEmbed = new MessageEmbed({
			color: "#4BB543",
			fields: [
				{
					name: "Feedback message submitted, thank you!",
					value: "Please note that feedback messages are always read, and intentional abuse may lead to being placed in an ignore-list."
				}
			]
		});

		if (parsedData.action === "feedback") {
			const trimmedVal = interaction.components[0].components[0].value.trim().replaceAll("```", "");
			if (trimmedVal !== "") {
				try {
					const feedback_channel = (await this.container.client.channels.fetch(this.container.env.feedbackChannelID)) as TextChannel;
					feedbackEmbed.setDescription(`**Message:**\n\`\`\`\n${trimmedVal}\n\`\`\``);
					feedbackSuccessEmbed.setDescription(`**Message:**\n\`\`\`\n${trimmedVal}\n\`\`\``);
					await feedback_channel.send({
						embeds: [feedbackEmbed],
						components: [
							new MessageActionRow({
								components: [
									new MessageButton({ customId: `ignore;user;${interaction.user.id}`, label: "Ignore User", style: "DANGER" })
								]
							})
						]
					});
					await interaction.reply({ ephemeral: true, embeds: [feedbackSuccessEmbed] });
				} catch (e) {
					this.container.logger.error("Something went wrong submitting feedback", e);
				}
			}
		}

		if (parsedData.action === "error") {
			const { value, customId } = interaction.components[0].components[0];
			const errorID = parsedData.args[0];
			const trimmedVal = value.trim().replaceAll("```", "");
			if (customId === "dev") {
				await this.container.db`UPDATE errors SET comment=${trimmedVal || null} WHERE error_id=${errorID}`;
				const embed = new MessageEmbed({ color: "#4BB543" });
				if (trimmedVal === "") {
					embed.setDescription(`**Removed comment from error \`${errorID}\`**`);
					await interaction.reply({ embeds: [embed], ephemeral: true });
				} else {
					embed.setDescription(`**Modified comment of error \`${errorID}\`** \`\`\`\n${trimmedVal}\n\`\`\``);
					await interaction.reply({ embeds: [embed], ephemeral: true });
				}
				return;
			}
			if (customId === "user" && trimmedVal !== "") {
				await interaction.deferReply({ ephemeral: true });
				const { message_id } =
					(
						await this.container.db<({ message_id: string } | undefined)[]>`
						SELECT report_message_id as message_id FROM errors 
						WHERE error_id = ${errorID}`
					)[0] || {};
				if (message_id === undefined) {
					this.container.logger.error("Couldn't fetch message for error ", errorID);
					throw GeneralErrors.FEEDBACK_SUBMIT;
				}
				try {
					const err_channel = (await this.container.client.channels.fetch(this.container.env.errorChannelID)) as TextChannel;
					const err_message = await err_channel.messages.fetch(message_id);
					if (err_message.hasThread) {
						feedbackEmbed.setDescription(`**Message:**\n\`\`\`\n${trimmedVal}\n\`\`\``);
						feedbackSuccessEmbed.setDescription(`**Message:**\n\`\`\`\n${trimmedVal}\n\`\`\``);
						await err_message.thread?.send({
							embeds: [feedbackEmbed]
						});

						await interaction.followUp({
							ephemeral: true,
							embeds: [feedbackSuccessEmbed]
						});
					}
				} catch (e) {
					this.container.logger.error("Failed to send feedback ", e);
				}
			}
		}
		if (parsedData.action === "eval") {
			if (interaction.user.id !== this.container.env.ownerID) return;
			const opts: eval_opts = JSON.parse(parsedData.args[1]);
			await interaction.deferReply({ ephemeral: opts.e });
			const code = interaction.components[0].components[0].value;
			const isSQL = parsedData.args[0] === "sql";
			if (isSQL) {
				await this.container.redis.setex("evalsql", 120, code);
				const { success, time, result } = await EvalCommand.sql(code);
				await interaction.followUp(EvalCommand.formatOutput(isSQL, opts.e, true, code, success, result, time, opts.f));
			} else {
				await this.container.redis.setex("evaljs", 120, code);
				const { success, time, result } = await EvalCommand.timedEval(code, opts, opts.t, interaction);
				await interaction.followUp(EvalCommand.formatOutput(isSQL, opts.e, true, code, success, result, time, opts.f));
			}
		}
	}

	public parse(interaction: ModalSubmitInteraction) {
		const [action, ...args] = interaction.customId.split(";");
		if (args.length >= 1 || action === "feedback") {
			return this.some({ action, args });
		}
		return this.none();
	}
}
