import {
	MessageEmbed,
	MessageEmbedOptions,
	ColorResolvable,
	CommandInteraction,
	MessageActionRow,
	Message,
	MessageButton,
	MessageComponentInteraction,
	MessageEditOptions,
	EmbedFooterData
} from "discord.js";
import _assign from "lodash/assign";
import { container } from "@sapphire/pieces";

export const defaultColor = "#fef200";

export interface field {
	name: string;
	value: string;
	inline?: boolean;
}

export default class CustomEmbed extends MessageEmbed {
	public defaultColor: ColorResolvable = defaultColor;
	public defaultText: string;
	constructor(data?: MessageEmbed | MessageEmbedOptions | undefined) {
		super(data);
		this.defaultText = "For a full set of commands see /help";
		// this.setFooter(this.defaultText);
		this.setColor(this.defaultColor);
	}

	public setFooter(options: string | EmbedFooterData | null): this {
		if (options === null) {
			super.footer = null;
			return this;
		}
		if (typeof options === "string") {
			options = { text: options };
		}
		if (options === null) {
			options = { text: this.defaultText };
		}
		if (options.iconURL === undefined) {
			options.iconURL = container.client.user?.displayAvatarURL();
		}
		super.setFooter(options);
		return this;
	}

	public static emptyField(inline?: boolean): field {
		inline = inline ?? true;
		return { name: "\u200b", value: "\u200b", inline };
	}

	public static field(name: string, value: string, inline?: boolean): field {
		if (name === "") {
			name = "\u200b";
		}
		if (value === "") {
			value = "\u200b";
		}
		return { name, value, inline };
	}

	public static paddedField(name: string, value: string, inline?: boolean): field {
		return { name, value: `${value}\n\u200b`, inline };
	}
}

export class PaginatedEmbed {
	public embeds: CustomEmbed[];
	public fieldsPageLimit: number;
	private data: MessageEmbed | MessageEmbedOptions | undefined;
	constructor(data?: MessageEmbed | MessageEmbedOptions | undefined) {
		this.fieldsPageLimit = 7;
		this.data = data;
		this.embeds = [new CustomEmbed(data)];
	}

	public set thumbnail(url: string) {
		for (const embed of this.embeds) {
			embed.setThumbnail(url);
		}
	}

	public set footer(text: string) {
		for (const embed of this.embeds) {
			embed.setFooter(text);
		}
		this.setPageCount();
	}

	public normalizePageNo(i: number): number {
		// Wrap around if out of bounds
		return ((i % this.pageCount) + this.pageCount) % this.pageCount;
	}

	public get first(): CustomEmbed {
		return this.embeds[0];
	}

	public get last(): CustomEmbed {
		return this.embeds[this.embeds.length - 1];
	}

	public get pageCount(): number {
		return this.embeds.length;
	}

	public getCurrentPage(footer: string): number | undefined {
		const match = footer.match(/\d{1,}(?= \/ \d{1,} •)/);
		if (!match) return undefined;
		return parseInt(match[0], 10) - 1;
	}

	public setPageCount() {
		if (this.pageCount === 1) return;
		for (const [i, embed] of this.embeds.entries()) {
			let footer = embed.footer?.text;
			const pageCount = `${i + 1} / ${this.pageCount} • `;
			if (footer !== undefined && footer !== "") {
				footer = footer.replace(/\d{1,} \/ \d{1,} • /, "");
				footer = pageCount + footer;
			} else {
				footer = pageCount;
			}
			embed.setFooter(footer);
		}
	}

	public addField(name: string, value: string, inline?: boolean): boolean {
		const len = name.length + value.length;
		let addedPage = false;
		if (
			len + this.last.length >= 1000 ||
			this.last.fields.length >= this.fieldsPageLimit ||
			this.last.fields.length >= 25 ||
			value.length >= 1024 ||
			(this.last.description !== null && this.last.description.length >= 1024)
		) {
			if ((this.last.description !== null && this.last.description !== "") || this.last.fields.length > 0) {
				addedPage = true;
				this.addPage();
			}
		}
		if (value.length < 1024) {
			this.last.addField(name, value, inline);
		} else {
			this.last.setDescription(`\n**${name}**\n${value}`);
		}
		return addedPage;
	}

	public addPage(embed?: CustomEmbed) {
		this.embeds.push(embed || new CustomEmbed(this.data));
		this.setPageCount();
	}

	public async paginate(
		interaction: CommandInteraction,
		options: {
			rows?: MessageActionRow[];
			callback?: (
				interaction: MessageComponentInteraction,
				ctx: { controlsRow: MessageActionRow; customRows?: MessageActionRow[]; page: number }
			) => Promise<MessageEditOptions | null>;
			midBtn?: { btn: MessageButton; index: number };
			ephemeral?: boolean;
			timeout?: number;
		}
	) {
		const control_row = new MessageActionRow().addComponents(
			new MessageButton({ customId: "first", emoji: "\u23EE", style: "PRIMARY" }),
			new MessageButton({ customId: "-1", emoji: "\u25C0", style: "PRIMARY" }),
			new MessageButton({ customId: "+1", emoji: "\u25B6", style: "PRIMARY" }),
			new MessageButton({ customId: "last", emoji: "\u23ED", style: "PRIMARY" })
		);
		if (options.midBtn) {
			control_row.spliceComponents(options.midBtn.index, 0, options.midBtn.btn);
		}
		const rows = options.rows ? [...options.rows, control_row] : [control_row];

		const opts = { ephemeral: options.ephemeral, embeds: [this.embeds[0]], components: this.pageCount > 1 ? rows : undefined };
		if (interaction.replied) {
			await interaction.editReply(opts);
		} else {
			await interaction.reply(opts);
		}
		if (this.pageCount === 1) {
			return;
		}
		const msg = (await interaction.fetchReply()) as Message;
		const collector = msg.createMessageComponentCollector({
			filter: (i) => {
				return i.user.id === interaction.user.id;
			},
			idle: (options.timeout ?? 0) || 25_000
		});

		let page = 0;
		collector.on("collect", async (i) => {
			try {
				page = (this.getCurrentPage(i.message.embeds[0].footer?.text ?? "") ?? 0) || page;
				switch (i.customId) {
					case "+1":
						page++;
						break;
					case "-1":
						page--;
						break;
					case "last":
						page = this.pageCount - 1;
						break;
					case "first":
						page = 0;
						break;
				}
				// Callback returns true if handled
				page = this.normalizePageNo(page);
				let cbOptions: MessageEditOptions | null = {};
				if (options.callback) {
					cbOptions = await options.callback(i, { controlsRow: control_row, customRows: options.rows, page });
					if (cbOptions === null) {
						return;
					}
				}
				await i.update(_assign({ embeds: [this.embeds[page]] }, cbOptions));
			} catch {}
		});

		collector.on("end", async () => {
			for (const row of rows) {
				for (const btn of row.components) {
					btn.setDisabled(true);
				}
			}
			await interaction.editReply({ components: rows });
		});
	}
}
