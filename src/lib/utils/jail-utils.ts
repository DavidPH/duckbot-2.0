import { isTextBasedChannel } from "@sapphire/discord.js-utilities";
import { Collection, DiscordAPIError, Guild, GuildMember, MessageAttachment, Role, User } from "discord.js";
import { DurationFormatter, Time } from "@sapphire/time-utilities";
import { DuckBotError, JailErrors } from "./errors";
import { container } from "@sapphire/framework";
import Embed from "./custom-embed";
import Canvas from "canvas";

export interface inJail {
	hasRole: [Role, Role] | null;
	isInDB: jailPayload | null;
}

export interface jailPayload {
	server_id: string;
	member_id: string;
	jailed_by: string;
	role_list: string[];
	jailed_at: Date;
	release_at: Date | null;
	reason: string | null;
}

export interface jailSettingsPayload {
	server_id: string;
	jail_message: string | null;
	jail_roles: Array<[string, string]>;
	log_channel: string | null;
	ephemeral: boolean;
	replace: boolean;
	timeout: boolean;
	enforce: boolean;
}

export interface historyPayload extends Omit<jailPayload, "release_at" | "role_list"> {
	released_at: Date | null;
	released_by: string | null;
}

export interface jailResult {
	type: "UPDATE" | "INSERT";
	jailEmbed: Embed;
	jailPic: MessageAttachment;
	jailDM: string | null;
	settings: jailSettingsPayload;
}

export class Jail {
	/**
	 * Gets the configured member/jail role pairs of a guild.
	 * Removes any configured roles that don't exist anymore.
	 * @param {string} guildID The guild to check.
	 * @returns {(Promise<Array<[Role, Role]> | null>)} An array of [memRole, jailRole], or null if guild has no config.
	 */
	public async getSetup(guildID: string): Promise<Array<[Role, Role]> | null> {
		const guild = container.client.guilds.cache.get(guildID);
		const roles: Array<[Role, Role]> = [];
		if (guild) {
			const query = await container.db<{ jail_roles: Array<[string, string]> }[]>`
            SELECT jail_roles FROM jail_settings WHERE server_id = ${guild.id}`;
			if (query.count > 0) {
				const roleIDs = query.flatMap(({ jail_roles }) => jail_roles);
				for (const [memRoleID, jailRoleID] of roleIDs) {
					const memRole = await guild.roles.fetch(memRoleID);
					const jailRole = await guild.roles.fetch(jailRoleID);
					if (memRole && jailRole) {
						roles.push([memRole, jailRole]);
					}
				}
				// If any roles weren't found
				if (roles.length !== roleIDs.length || true) {
					const roleIDs = `{${roles.map(([r1, r2]) => `{${[r1.id, r2.id]}}`)}}`;
					await container.db`UPDATE jail_settings SET jail_roles = ${roleIDs}
					WHERE server_id = ${guildID}`;
				}
			}
		}
		if (roles.length > 0) return roles;
		return null;
	}

	/**
	 * Checks if a member has a jail role and if they are also in the jail database
	 * @param {GuildMember} member The member to check
	 * @param {?Array<[Role, Role]>} [rolePairs] The configured role pairs, or if not specified then they are fetched using {@link Jail.getSetup}
	 * @param {jailPayload} payload Optional db payload if fetched outside.
	 * @returns {Promise<inJail>} {@link inJail}
	 */
	public async isMemberJailed(member: GuildMember, rolePairs?: Array<[Role, Role]>, payload?: jailPayload): Promise<inJail> {
		const result: inJail = { hasRole: null, isInDB: null };
		if (!rolePairs) {
			rolePairs = (await this.getSetup(member.guild.id)) ?? undefined;
		}
		if (rolePairs) {
			for (const [memRole, jailRole] of rolePairs) {
				if (member.roles.cache.has(jailRole.id)) {
					result.hasRole = [memRole, jailRole];
					break;
				}
			}
		}
		if (payload) {
			result.isInDB = payload;
		} else {
			const query = await container.db<jailPayload[]>`SELECT * FROM jail WHERE server_id = ${member.guild.id} AND member_id = ${member.id}`;
			if (query.count > 0) {
				[result.isInDB] = query;
			}
		}
		return result;
	}

	/**
	 * Get the appropriate jail for a member
	 * @param {Role} member
	 * @param {Array<[Role, Role]>} rolePairs The configured role pairs for the member's guild.
	 * @returns {([Role, Role] | null)} Returns the appropriate jail role pair, or null if none found.
	 */
	public getJailRole(member: GuildMember, rolePairs: Array<[Role, Role]>): [Role, Role] | null {
		for (const [mRole, jRole] of rolePairs) {
			if (member.roles.cache.has(mRole.id)) {
				return [mRole, jRole];
			}
		}
		return null;
	}

	/**
	 * Sends someone to jail. If a duration is specified they are added to the Collection.
	 * @param {GuildMember} jailer The person responsible for the jail.
	 * @param {GuildMember} member The member to be jailed.
	 * @param {(number | null)} duration The duration to be jailed for, or null.
	 * @param {(string | null)} reason The reason given, or null.
	 * @returns {(Promise<jailResult | DuckBotError>)} {@link jailResult} | {@link DuckBotError}
	 */
	public async sendToJail(
		jailer: GuildMember,
		member: GuildMember,
		duration: number | null,
		reason: string | null
	): Promise<jailResult | DuckBotError> {
		const { guild } = member;
		const releaseAt = duration === null ? null : new Date(Date.now() + duration);
		let update = false;
		let new_roles = member.roles.cache;
		reason = (reason?.trim() ?? "") || null;
		const jailRoles = await this.getSetup(guild.id);
		if (jailRoles) {
			const { hasRole, isInDB } = await this.isMemberJailed(member, jailRoles);
			// Check if it's an update
			if (hasRole && isInDB) {
				if (isInDB.release_at !== releaseAt || reason !== isInDB.reason) {
					update = true;
				} else {
					return JailErrors.IN_JAIL;
				}
			}
			// If not an update but user has role throw error
			else if (hasRole && !isInDB) {
				return JailErrors.IN_DB;
			}
			// Delete db entry if user doesn't have role, then proceed as normal
			else if (!hasRole && isInDB) {
				await container.db`DELETE FROM jail WHERE member_id = ${member.id} AND server_id = ${guild.id}`;
			}
			const [settings] = await container.db<jailSettingsPayload[]>`SELECT * FROM jail_settings WHERE server_id = ${guild.id}`;
			const oldRoles = member.roles.cache.filter((role) => !role.managed && role.id !== guild.id).map((r) => r.id);
			const auditLogMsg = `${member.user.tag} was jailed by ${jailer.user.tag} ${
				duration === null ? "" : `for ${new DurationFormatter().format(duration)}.`
			}`;
			if (!update) {
				const rolePair = this.getJailRole(member, jailRoles);
				if (rolePair) {
					try {
						const [mRole, jRole] = rolePair;
						if (settings.replace) {
							// Don't try replaced managed roles
							new_roles = new_roles.filter((role) => role.managed).set(jRole.id, jRole);
							await member.roles.set(new_roles, auditLogMsg);
						} else {
							if (!mRole.managed && mRole.id !== guild.roles.everyone.id) {
								await member.roles.remove(mRole, auditLogMsg);
							}
							await member.roles.add(jRole, auditLogMsg);
						}
					} catch (error) {
						if (error instanceof DiscordAPIError && error.code === 50013) {
							return JailErrors.NOT_MANAGEABLE;
						}
						throw error;
					}
				} else {
					return JailErrors.NO_MEMBER_ROLE;
				}
			}
			if (settings.timeout) {
				member.timeout(duration === null ? null : Math.min(duration, Time.Day * 28), reason ?? auditLogMsg).catch(() => {});
			}
			// Everything went fine, insert into database and prepare result object
			const jailPayload: Omit<jailPayload, "role_list"> & { role_list: string } = {
				server_id: guild.id,
				member_id: member.id,
				jailed_at: new Date(),
				jailed_by: jailer.id,
				release_at: releaseAt,
				role_list: `{${oldRoles}}`,
				reason
			};
			await container.db<jailPayload[]>`
				INSERT INTO jail ${container.db(jailPayload)}
				ON CONFLICT (server_id, member_id) DO UPDATE SET 
					release_at = EXCLUDED.release_at,
					jailed_by = EXCLUDED.jailed_by,
					reason = EXCLUDED.reason
				`;
			const { embed, attach } = await makeJailEmbed(member, reason, duration, update);
			// TODO make this more informative
			if (settings.log_channel !== null && !update) {
				const channel = jailer.guild.channels.cache.get(settings.log_channel);
				if (isTextBasedChannel(channel)) {
					channel.send({ content: auditLogMsg }).catch(() => {});
				}
			}
			return {
				type: update ? "UPDATE" : "INSERT",
				jailEmbed: embed,
				jailPic: attach,
				jailDM: makeJailMessage(settings.jail_message, member, jailer, duration, reason),
				settings
			};
		}
		return JailErrors.NO_CONFIG;
	}

	/**
	 * Remove a user from jail db and remove any jail role they might have.
	 * @param {GuildMember} jailer The person responsible for freeing.
	 * @param {GuildMember | jailPayload} opt {@link GuildMember} or {@link jailPayload} of the member to be freed.
	 * @returns {(Promise<DuckBotError | jailPayload | null>)} {@link jailPayload} if the user was in db, or {@link DuckBotError} if failed.
	 */
	public async freeFromJail(jailer: User, opt: GuildMember | jailPayload): Promise<DuckBotError | jailPayload | null> {
		let guild: Guild;
		let member: GuildMember;
		let isPayload = false;
		if ("server_id" in opt) {
			isPayload = true;
			guild = container.client.guilds.cache.get(opt.server_id)!;
			try {
				member = await guild.members.fetch(opt.member_id);
			} catch {
				return null;
			}
		} else {
			member = opt;
			guild = member.guild;
		}
		const rolePairs = await this.getSetup(guild.id);
		if (!rolePairs) return JailErrors.NO_CONFIG;
		const { hasRole, isInDB } = await this.isMemberJailed(member, rolePairs, isPayload ? (opt as jailPayload) : undefined);
		if (!hasRole) {
			if (isInDB) {
				await container.db`DELETE FROM jail WHERE member_id = ${member.id} AND server_id = ${guild.id}`;
			}
			return JailErrors.NOT_IN_JAIL;
		}
		if (!isInDB) {
			await member.roles.remove(hasRole[1]);
			if (!hasRole[0].managed && hasRole[0].id !== guild.roles.everyone.id) {
				await member.roles.add(hasRole[0]);
			}
			return null;
		}
		const now = new Date();
		const early = isInDB.release_at === null ? false : isInDB.release_at < now ? false : true;
		const roles = isInDB.role_list.map((rID) => guild.roles.cache.get(rID)!).filter((r) => r !== undefined);
		const newRoles = member.roles.cache.filter((r) => r.id !== hasRole[1].id).concat(new Collection(roles.map((r) => [r.id, r])));
		const auditLogMsg = `${member.user.tag} was freed by ${jailer.tag} ${
			early ? `${new DurationFormatter().format(isInDB.release_at!.getTime() - now.getTime(), 2)} early.` : ""
		}`;
		await member.roles.set(newRoles, auditLogMsg);
		await container.db`
		UPDATE jail_history
		SET released_at = ${now}, released_by = ${jailer.id}
		WHERE member_id = ${isInDB.member_id} AND server_id = ${isInDB.server_id} AND jailed_at = ${isInDB.jailed_at}`;
		await container.db`DELETE FROM jail WHERE member_id = ${member.id} AND server_id = ${guild.id}`;
		container.db<jailSettingsPayload[]>`SELECT * FROM jail_settings WHERE server_id = ${guild.id}`
			.then(async ([settings]) => {
				if (settings.timeout) {
					await member.timeout(null, auditLogMsg);
				}
				const channel = guild.channels.cache.get(settings.log_channel ?? "");
				if (isTextBasedChannel(channel)) {
					await channel.send(auditLogMsg);
				}
			})
			.catch(() => {});
		return isInDB;
	}
}

export async function makeJailPic(member: GuildMember) {
	const images = [await Canvas.loadImage(member.displayAvatarURL({ format: "png" })), await Canvas.loadImage("resources/images/bars.png")];
	const canvas = Canvas.createCanvas(128, 128);
	const ctx = canvas.getContext("2d");
	for (const img of images) {
		ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
	}
	return new MessageAttachment(canvas.toBuffer(), "jail_image.png");
}

function makeJailMessage(
	jailMessage: jailSettingsPayload["jail_message"],
	member: GuildMember,
	author: GuildMember,
	time_left: number | null,
	reason: string | null
): string | null {
	const reps: { [key: string]: string } = {
		server: member.guild.name,
		time_left: time_left === null ? "Indefinite" : new DurationFormatter().format(time_left),
		reason: (reason ?? "") || "N/A",
		jailer: author.displayName,
		user: member.displayName
	};

	if (jailMessage === null || jailMessage === "") {
		return null;
	}
	return jailMessage.replace(/\{(\w+)\}/g, (s, key) => {
		return reps[key] || s;
	});
}

async function makeJailEmbed(member: GuildMember, reason: string | null, duration: number | null, update: boolean) {
	const embed = new Embed()
		.setThumbnail("attachment://jail_image.png")
		.setTitle(update ? `Updated ${member.displayName}` : `Sent ${member.displayName} to jail.`)
		.setDescription(`To free them ${duration === null ? "" : "early"} type \`/free @${member.displayName}\`\n\u200b`)
		.addFields(
			Embed.paddedField(
				"Time remaining:".padEnd(20, "\u200b "),
				`${duration === null ? "Indefinite" : `${new DurationFormatter().format(duration)}`}`,
				true
			),
			Embed.emptyField(),
			Embed.paddedField("Reason:".padEnd(50, "\u200b "), `${reason ?? "N/A"}`, true)
		)
		.setFooter(`See /view jail info @${member.displayName} for more.`);

	return { embed, attach: await makeJailPic(member) };
}
