import { ButtonInteraction, ExcludeEnum, Guild, GuildMember, MessageButton, User } from "discord.js";
import type { MessageButtonStyles } from "discord.js/typings/enums";
import _IsInteger from "lodash/isInteger";
import _inRange from "lodash/inRange";
import _truncate from "lodash/truncate";
import Embed, { field } from "./custom-embed";
import { container } from "@sapphire/pieces";
import { AsyncQueue } from "@sapphire/async-queue";
import { FormattedCustomEmojiWithGroups, TwemojiRegex } from "@sapphire/discord-utilities";
// @ts-ignore No types file
import { pack, unpack } from "compressed";
// @ts-ignore No types file
import emoji_name from "emoji-name-map";
import { SignupErrors } from "./errors";

export interface signup_button {
	label: string;
	emote: string | undefined;
	style: ExcludeEnum<typeof MessageButtonStyles, "LINK">;
}
// export type signup_button = { label?: string; emote: string; style: string } | { label: string; emote?: string; style: string };

export interface signup_message {
	title: string;
	limit?: number;
	requires?: string[];
}

export interface signup_section extends signup_message {
	inline: boolean;
	button?: signup_button;
}

export interface signup_options {
	text: string;
	nicknames: boolean;
	messages: signup_message[];
	sections: signup_section[];
}

// members[0] -> first message, members[0][0] -> first section of first message, members[0][0][0] -> first member
type signup_members = Array<Array<string[]>>;

function titleReplace(title: string, total: number, limit: number | null, date: null | Date) {
	title = title.replaceAll(/\[([tTdDfFR])(?::date)\]/g, date && date.getTime() ? `<t:${Math.floor(date.getTime() / 1000)}:$1>` : "");
	title = title.replaceAll("[date]", date && date.getTime() ? `<t:${Math.floor(date.getTime() / 1000)}:f>` : "");
	title = title.replaceAll("[total]", `(${total}${limit ?? 0 ? `/${limit}` : ""})`);
	return title;
}

export default class Signup {
	public members: signup_members;
	public options: signup_options;
	private inline_format: boolean;

	constructor(options?: signup_options) {
		this.inline_format = false;
		if (options === undefined) {
			options = this.defaultOptions();
		} else {
			this.inline_format = options.sections.some((s) => s.inline);
		}
		this.options = options;
		this.members = this.makeArray();
	}

	public get text(): string {
		return this.options.text;
	}

	public set text(message: string) {
		this.options.text = message;
	}

	public get isNicknames(): boolean {
		return this.options.nicknames;
	}

	public set isNicknames(nick: boolean) {
		this.options.nicknames = nick;
	}

	public get buttons(): MessageButton[] {
		const buttons = [];
		for (const [i, { button }] of this.options.sections.entries()) {
			if (!button) continue;
			const b = Signup.validateButtonStr(`[${button?.emote ?? ""}][${button?.label || ""}][${button?.style ?? ""}]`).button;
			if (b) {
				buttons.push(
					new MessageButton({
						label: b.label || undefined,
						emoji: b.emote,
						customId: `section${i}`,
						disabled: false,
						style: b.style as ExcludeEnum<typeof MessageButtonStyles, "LINK">
					})
				);
			}
		}
		return buttons;
	}

	public get sections(): signup_options["sections"] {
		return this.options.sections;
	}

	public get messages(): signup_options["messages"] {
		return this.options.messages;
	}

	public popSection(): boolean {
		if (this.options.sections.length > 1) {
			this.options.sections.pop();
			this.members = this.makeArray();
			this.inline_format = this.options.sections.some((s) => s.inline);

			return true;
		}
		return false;
	}

	public popMessage(): boolean {
		if (this.options.messages.length > 1) {
			this.options.messages.pop();
			this.members = this.makeArray();
			return true;
		}
		return false;
	}

	public editSection(index: number, options: signup_section) {
		if (!options.inline && this.options.sections[index].inline) {
			this.options.sections[index].inline = false;
			this.inline_format = this.options.sections.some((s) => s.inline);
		}
		if (_inRange(index, this.sections.length)) {
			this.addSection(options, index);
		}
	}

	public addSection(options: signup_section, edit?: number) {
		if (edit === undefined && this.sections.length >= 5) {
			return;
		}
		if (!_IsInteger(options.limit) || options.limit! < 1) {
			options.limit = undefined;
		}
		this.inline_format = this.inline_format || options.inline;
		if (edit === undefined) {
			this.options.sections.push(options);
			this.members = this.makeArray();
		} else {
			this.options.sections[edit] = options;
		}
	}

	public editMessage(index: number, options: signup_message) {
		if (!_IsInteger(options.limit) || options.limit! < 1) {
			options.limit = undefined;
		}
		if (_inRange(index, this.options.messages.length)) {
			this.options.messages[index] = options;
		}
	}

	public addMessage(options: signup_message) {
		if (this.messages.length >= 5) {
			return;
		}
		if (!_IsInteger(options.limit) || options.limit! < 1) {
			options.limit = undefined;
		}
		this.options.messages.push(options);
		this.members = this.makeArray();
	}

	public makeArray(): signup_members {
		return Array(this.options.messages.length)
			.fill(null)
			.map(() =>
				Array(this.options.sections.length)
					.fill(null)
					.map(() => [])
			);
	}

	public async getEmbeds(guild: Guild, date: Date | null, members?: signup_members): Promise<Embed[]> {
		const embeds: Embed[] = [];
		if (!members) {
			members = this.members;
		}
		for (const [i, messageArr] of members.entries()) {
			const fields: field[] = [];
			let description = "";
			let title = "";
			let messageTotal = 0;
			for (const [j, sectionArr] of messageArr.entries()) {
				const section = this.options.sections[j];
				let sectionTotal = 0;
				let value = "";
				title = section.title;
				for (const mem_id of sectionArr) {
					let member: GuildMember | undefined = undefined;
					try {
						member = await guild.members.fetch(mem_id);
					} catch {}
					const mem_string = member ? (this.isNicknames ? member.displayName : member.user.tag) : mem_id;
					value += `+ ${mem_string}\n`;
					sectionTotal++;
				}
				value += `${" ".repeat(35)}\n\`\`\``;
				messageTotal += sectionTotal;
				title = titleReplace(title, sectionTotal, section.limit ?? null, date);
				if (this.inline_format) {
					value = `\`\`\`\n${value}`;
					fields.push(Embed.field(title, value, section.inline));
				} else {
					value = `${title ? `${j > 0 ? "\n" : ""}${title}\n` : `${j > 0 ? "\n" : ""}`}\`\`\`\n${value}`;
				}
				description += value;
			}
			const message = this.options.messages[i];
			embeds.push(
				new Embed({
					title: titleReplace(message.title, messageTotal, message.limit ?? null, date) || undefined
					// title: message.title.replaceAll("[total]", `(${messageTotal}${message.limit ? `/${message.limit}` : ""})`) || undefined
				}).setFooter(null)
			);
			if (this.inline_format) {
				embeds[embeds.length - 1].addFields(fields);
			} else {
				embeds[embeds.length - 1].setDescription(description);
			}
		}
		return embeds;
	}

	public defaultOptions(): signup_options {
		return {
			text: "",
			nicknames: true,
			messages: [{ title: "" }],
			sections: [{ title: "", inline: false, button: Signup.DEFAULT_BUTTON }]
		};
	}

	public async export(): Promise<string> {
		const sections = JSON.parse(JSON.stringify(this.options.sections));
		for (const section of sections) {
			if (section.button !== undefined) {
				section.button = Signup.buttonToString(section.button);
			}
		}
		const packedStr: string = await pack({ text: this.options.text, nicknames: this.isNicknames, messages: this.options.messages, sections });
		if (packedStr.length > 4000) {
			return pack({ text: "", nicknames: this.isNicknames, messages: this.options.messages, sections });
		}
		return packedStr;
	}

	public async testFill(guild: Guild) {
		this.members = this.makeArray();
		const mems = await guild.members.fetch();
		for (const messageArr of this.members) {
			for (const [j, sectionArr] of messageArr.entries()) {
				const section = this.options.sections[j];
				let limit = Math.floor(Math.random() * (section.limit ?? 20)) + 1;
				limit = Math.min(Math.max(limit, 1), 99);
				sectionArr.push(...mems.randomKey(limit));
			}
		}
	}

	public hasUser(user: string | GuildMember | User): [number, number, number] {
		if (typeof user !== "string") {
			user = user.id;
		}
		for (const [m, message] of this.members.entries()) {
			for (const [s, section] of message.entries()) {
				const index = section.indexOf(user);
				if (index !== -1) {
					return [m, s, index];
				}
			}
		}
		return [-1, -1, -1];
	}

	private static BUTTON_STYLES: string[] = ["PRIMARY", "SECONDARY", "SUCCESS", "DANGER"];

	private static DEFAULT_BUTTON: signup_button = { emote: "✅", label: "", style: "SUCCESS" };

	public static validateRoles(roles: string[], guild: Guild): { roles: string[]; err: string } {
		let i = roles.length;
		let error = "";
		while (i--) {
			if (roles[i] && !guild.roles.cache.get(roles[i])) {
				error += `Unknown role ID: \`${roles[i]}\`\n`;
				roles.splice(i, 1);
			}
		}
		roles = roles.filter((r, i, self) => r !== "" && self.indexOf(r) === i).slice(0, 23); // Remove duplicates and cap amount.
		return { roles, err: error };
	}

	public static validateButtonStr(buttonStr: string | undefined): { error: string; button: signup_button | undefined } {
		const buttonFormatRegex = new RegExp(/(?:\[(?<emoji>.*?)\])(?:\[(?<label>.*?)\])?(?:\[(?<style>.*?)\])?/);
		const buttonFormat = buttonStr?.match(buttonFormatRegex);
		let error = "";
		if (buttonFormat) {
			const emoji_label = _truncate(buttonFormat.groups?.label ?? "", { length: 80 });
			let style = buttonFormat.groups?.style?.toUpperCase();
			style = Signup.BUTTON_STYLES.some((s) => s === style) ? style : "SUCCESS";
			let emoji = buttonFormat.groups?.emoji;
			if (emoji !== undefined) {
				const re = new RegExp(TwemojiRegex);
				const tweEmoji = emoji.match(re);
				if (tweEmoji) {
					emoji = tweEmoji[0]!;
				} else {
					const customEmoji = emoji.match(FormattedCustomEmojiWithGroups);
					if (customEmoji && customEmoji.groups?.id !== undefined) {
						emoji = container.client.emojis.cache.get(customEmoji.groups.id)?.id;
					} else {
						emoji = container.client.emojis.cache.get(emoji)?.id;
					}
					if (emoji === undefined) {
						emoji = emoji_name.get(buttonFormat.groups!.emoji);
						if (emoji === undefined || (emoji && !re.test(emoji))) {
							if (!(buttonFormat.groups?.label ?? "")) {
								error = `Invalid emoji: \`${buttonFormat.groups!.emoji}\`\n`;
								emoji = undefined;
							}
						}
					}
				}
			}
			if (buttonFormat.groups?.style !== undefined && buttonFormat.groups.style.toUpperCase() !== style) {
				error += `Invalid style: \`${buttonFormat.groups.style}\`\n`;
			}
			if ((emoji === undefined || emoji === "") && emoji_label === "") {
				emoji = this.DEFAULT_BUTTON.emote;
			}
			return { error, button: { label: emoji_label, emote: emoji, style: style as signup_button["style"] } };
		}

		return { error: "", button: undefined };
	}

	public static async import(importStr: string, guild: Guild, isJSON?: boolean): Promise<[null | Signup, string]> {
		let parsedStr: signup_options;
		try {
			if (isJSON ?? false) parsedStr = JSON.parse(importStr);
			else parsedStr = JSON.parse(await unpack(importStr));
		} catch {
			return [null, "Error parsing import string."];
		}
		// Check if it doesn't have messages or sections arrays
		if (
			!(parsedStr.messages instanceof Array || !(parsedStr.sections instanceof Array)) ||
			typeof parsedStr.nicknames !== "boolean" ||
			(parsedStr.text && typeof parsedStr.text !== "string")
		) {
			return [null, "Invalid signup format"];
		}
		let error = "";
		for (const [i, component] of [parsedStr.messages, parsedStr.sections].entries()) {
			// Check if between 1 and 5 sections / messages
			if (!_inRange(component.length, 1, 6)) {
				return [null, "Signups must not exceed 5 sections or 5 messages."];
			}
			for (const item of component) {
				// section & message check
				if (typeof item.title !== "string") {
					return [null, "Invalid title format"];
				}
				if (item.title.length > 40) {
					item.title = _truncate(item.title, { length: 40 });
					error += "Title length must not exceed 40 characters.\n";
				}
				if (item.limit !== undefined) {
					if (typeof item.limit !== "number") {
						return [null, "Invalid limit format."];
					}
					if (item.limit > 99) {
						item.limit = 99;
						error += "Limit must not exceed 99.\n";
					}
				}
				if (item.requires !== undefined) {
					if (!(item.requires instanceof Array)) {
						return [null, "Invalid requires format."];
					}
					if (item.requires.some((el: any) => typeof el !== "string")) {
						return [null, "Invalid requires format."];
					}
					const { err, roles } = Signup.validateRoles(item.requires, guild);
					error += err;
					item.requires = roles;
				}
				// section only check
				if (i === 1) {
					const section = item as signup_section;
					let button_str = section.button as any; // Can be either string "[emote][label][style]" or if JSON then as a button object
					if (typeof section.inline !== "boolean") {
						section.inline = false;
					}
					if ((isJSON ?? false) && button_str !== undefined) {
						button_str = `[${button_str.emote ?? ""}][${button_str.label ?? ""}][${button_str.style ?? ""}]`;
					} else if (typeof button_str !== "string" && button_str !== undefined) {
						return [null, "Invalid Button format."];
					}
					const { error: err, button } = Signup.validateButtonStr(button_str);
					error += err;
					section.button = button;
				}
			}
		}
		return [
			new Signup({
				text: parsedStr.text || "",
				nicknames: parsedStr.nicknames,
				messages: parsedStr.messages,
				sections: parsedStr.sections
			}),
			error
		];
	}

	public static buttonToString(button: signup_button | undefined): string {
		if (button) {
			let emote = button?.emote;
			if (emote !== undefined && !new RegExp(TwemojiRegex).test(emote)) {
				emote = container.client.emojis.cache.get(emote)?.toString();
			}
			return `[${emote ?? ""}][${button.label ?? ""}][${button.style}]`;
		}
		return "";
	}

	public static async toggleFromSignup(
		signup_json: string,
		guild: Guild,
		m_index: number,
		toToggle: { s_index: number; users: string[] }[],
		bypass?: boolean
	): Promise<[signup: Signup | null, info: string, edited: Set<number>]> {
		const toModify: { [key: string]: { add: boolean; s_index?: number; old_s?: number } } = {};
		const signup: Signup = Object.assign(new Signup(), JSON.parse(signup_json));
		let info = "";
		const editedMessages: Set<number> = new Set();
		// Check all the users that are to be toggled, regardless of section.
		for (const { s_index, users } of toToggle) {
			for (const user_id of users) {
				const [msg, sec, index] = signup.hasUser(user_id);
				// If user was in signup
				if (msg !== -1 && sec !== -1 && index !== -1) {
					signup.members[msg][sec].splice(index, 1);
					// Allow for moving sections within signup
					if (s_index === sec && msg === m_index) {
						toModify[user_id] = { add: false };
					} else {
						toModify[user_id] = { s_index, old_s: sec, add: true };
					}
					editedMessages.add(msg);
				}
				// If user is not in signup
				else {
					toModify[user_id] = { s_index, add: true };
					editedMessages.add(m_index);
				}
			}
		}
		const removed_users: (GuildMember | string)[] = [];
		const added_users: (GuildMember | string)[] = [];
		const moved_users: (GuildMember | string)[] = [];
		// Add users to signup
		const message = signup.messages[m_index];
		for (const [user_id, { add, s_index, old_s }] of Object.entries(toModify)) {
			let member: GuildMember | undefined = undefined;
			try {
				member = await guild.members.fetch(user_id);
			} catch {
				if (!(bypass ?? false)) {
					continue;
				}
			}
			if (!add || s_index === undefined) {
				removed_users.push(member || user_id);
				continue;
			}
			for (let i = s_index; i < signup.sections.length; i++) {
				const section = signup.sections[i];
				const sectionArr = signup.members[m_index][i];
				// Check if limits allow for signup
				if ((bypass ?? false) || section.limit === undefined || sectionArr.length < section.limit) {
					// Check if user has required roles
					if (
						((bypass ?? false) ||
							!message.requires ||
							message.requires.length <= 0 ||
							(member?.roles.cache.hasAny(...message.requires) ?? false)) &&
						((bypass ?? false) ||
							!section.requires ||
							section.requires.length <= 0 ||
							(member?.roles.cache.hasAny(...section.requires) ?? false))
					) {
						if (old_s === undefined) {
							added_users.push(member || user_id);
						} else {
							editedMessages.add(m_index);
							moved_users.push(member || user_id);
						}
						sectionArr.push(user_id);
					}
					// Doesn't meet required roles
					else {
						if (old_s !== undefined) removed_users.push(member || user_id);
						info = "You do not have a required role to sign up for this.";
					}
					break;
				}
				// Check if next section has no button (i.e, could overflow)
				else if (i + 1 < signup.sections.length && !signup.sections[i + 1].button) {
					continue;
				}
				// No overflow available || limits did not allow
				else {
					// If user wanted to move, but no place to move to.
					if (old_s !== undefined) removed_users.push(user_id);
					info = "The message or section you tried to sign up for is full.";
					break;
				}
			}
		}
		let warning = false;
		const format_user = (sep: string, user: GuildMember | string) => {
			if (typeof user === "string") {
				warning = true;
				return `\* ${user}`;
			}
			return `${sep} ${user.user.tag}`;
		};
		if (bypass ?? false) {
			info = `${added_users.length > 0 ? `**Add:**\n\`\`\`diff\n${added_users.map((u) => format_user("+", u)).join("\n")}\`\`\`` : ""}${
				removed_users.length > 0 ? `**Remove:** \n\`\`\`diff\n${removed_users.map((u) => format_user("-", u)).join("\n")}\`\`\`` : ""
			}${moved_users.length > 0 ? `**Update:** \n\`\`\`fix\n${moved_users.map((u) => format_user("+", u)).join("\n")}\`\`\`` : ""}${
				warning ? "*\\*Discord user not found, name will not be tied to anyone.*" : ""
			}`;
		}
		if (added_users.length <= 0 && moved_users.length <= 0 && removed_users.length <= 0) {
			return [null, info, editedMessages];
		}
		if (!(bypass ?? false) && info === "") {
			if (removed_users.length === 1) {
				info = "Unsigned <:tick:729059775173754960>";
			} else if (moved_users.length === 1) {
				info = "Moved sign up <:tick:729059775173754960>";
			} else if (added_users.length === 1) {
				info = "Signed up <:tick:729059775173754960>";
			}
		}
		return [signup, info, editedMessages];
	}

	public static async addToSignup(s_index: number, interaction: ButtonInteraction) {
		await interaction.deferReply({ ephemeral: true });
		const message_id = interaction.message.id;
		const guild = interaction.guild!;

		const signup_id = (
			await container.db<({ signup_id: number } | undefined)[]>`
		SELECT signup_id FROM signup_messages WHERE message_id = ${message_id} AND server_id = ${guild.id}`
		)[0]?.signup_id;

		if (signup_id === undefined) {
			throw SignupErrors.SIGNUP_NOT_FOUND;
		}

		const key = `${guild.id}${signup_id}`;
		let queue = container.signup_queue[key];
		if (queue) {
			clearTimeout(queue[1]);
			queue[1] = setTimeout(() => delete container.signup_queue[key], 15 * 60_000);
		} else {
			container.signup_queue[key] = [new AsyncQueue(), setTimeout(() => delete container.signup_queue[key], 15 * 60_000)];
			queue = container.signup_queue[key]!;
		}
		await queue[0].wait();

		const rows = await container.db<
			{ message_index: number; message_id: string; signup_id: number; signup_json: string; signup_expire: Date | null }[]
		>`SELECT DISTINCT ON (m.message_index) 
			message_index, message_id, s.*
			FROM signups as s LEFT JOIN signup_messages as m
			ON s.signup_id = (SELECT signup_id FROM signup_messages WHERE message_id = ${message_id} AND server_id = ${guild.id})
			WHERE m.signup_id = s.signup_id ORDER BY m.message_index ASC`;
		if (rows.length <= 0) {
			queue[0].shift();
			return;
		}
		try {
			if (rows[0].signup_expire && typeof rows[0].signup_expire.getTime() === "number") {
				if (new Date().getTime() >= rows[0].signup_expire.getTime()) {
					await interaction.followUp({ ephemeral: true, content: "Error: this signup has closed." });
					queue[0].shift();
					return;
				}
			}
			const m_index = rows.find((r) => r.message_id === message_id)?.message_index;
			if (m_index === undefined) {
				queue[0].shift();
				return;
			}
			const [signup, info, editedMessages] = await Signup.toggleFromSignup(rows[0].signup_json, guild, m_index, [
				{ s_index, users: [interaction.user.id] }
			]);
			if (signup) {
				const embeds = await signup.getEmbeds(guild, rows[0].signup_expire);
				await container.db`UPDATE signups SET signup_json = ${container.db.json(JSON.stringify(signup))}
				WHERE signup_id = ${rows[0].signup_id} AND server_id = ${guild.id}`;
				queue[0].shift();
				await interaction.followUp({ ephemeral: true, content: info || "\u200b" });
				for (const i of editedMessages) {
					// This is sometimes slow
					try {
						interaction
							.channel!.messages.fetch(rows[i].message_id)
							.then((msg) => {
								msg.edit({ embeds: [embeds[i]] }).catch(() => {});
							})
							.catch(async () => {
								await interaction.followUp({
									ephemeral: true,
									content: `Something went wrong fetching signup message \`${rows[i]?.message_id}\``
								});
							});
					} catch {}
				}
			} else {
				queue[0].shift();
				await interaction.followUp({ ephemeral: true, content: info });
			}
		} catch {
			queue[0].shift();
		}
	}
}
