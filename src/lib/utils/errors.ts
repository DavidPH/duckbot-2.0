import { DiscordAPIError, Interaction, MessageActionRow, MessageButton, MessageEmbed } from "discord.js";
import { codeBlock } from "@sapphire/utilities";
import { DiscordSnowflake } from "@sapphire/snowflake";
import { container } from "@sapphire/framework";
import { defaultColor } from "./custom-embed";

const enum ERROR_CODE {
	IN_JAIL = 10,
	IN_DB,
	NO_MEMBER_ROLE,
	NO_CONFIG,
	INVALID_DURATION,
	MAX_DURATION,
	MIN_DURATION,
	NOT_IN_JAIL,
	NOT_MANAGEABLE,
	NOT_A_SIGNUP,
	SIGNUP_NOT_FOUND,
	BOT_MISSING_PERM,
	INVALID_DATETIME,
	PAST_DATETIME,
	FORMAT_NOT_FOUND,
	INVALID_SIGNUP_IMPORT,
	POSTING_SIGNUP,
	FEEDBACK_SUBMIT,
	RESTRICTED_COMMAND,
	ERROR_NOT_FOUND,
	UNKNOWN_ERROR_NOT_FOUND
}

interface errorOptions {
	message: string;
	description: string;
	help: string;
	errorCode: `${string}-${ERROR_CODE}`;
}

export const errorColor = "#FF3C00";
export const supportLinkRow = new MessageActionRow().addComponents(
	new MessageButton({
		label: "Support Server",
		style: "LINK",
		url: container.env.duckbotServerInvite
	})
);

export class UnknownErrorEmbed {
	private errCode: string;
	private comment: string | null;
	private fixed: boolean;
	constructor(errCode: string, fixed: boolean, comment: string | null) {
		this.errCode = errCode;
		this.comment = comment;
		this.fixed = fixed;
	}

	public get embed(): [MessageEmbed, MessageActionRow] {
		const embed = new MessageEmbed({
			color: defaultColor,
			description: `**Error Code: \`${this.errCode}\`**${codeBlock(
				"fix",
				"This is an 'unknown error', this means it's either an expected error that's missing a label, or a bug."
			)}`,
			fields: [
				{
					name: "Status",
					value: this.fixed ? "Fixed <:tick:729059775173754960>" : "Fix in progress",
					inline: true
				}
			]
		});
		if (this.comment !== null) {
			embed.addField("Dev Comment", this.comment, true);
		}
		return [embed, supportLinkRow];
	}
}

export class DuckBotError extends Error {
	private options: errorOptions;
	constructor(options: errorOptions) {
		super(options.message);
		Object.setPrototypeOf(this, DuckBotError.prototype);
		this.options = options;
	}

	public replaceStr(s: "message" | "description", ...args: [string, string][]): DuckBotError {
		for (const arg of args) {
			this.options[s] = this.options[s].replace(arg[0], arg[1]);
		}
		return this;
	}

	public get ERROR_CODE(): errorOptions["errorCode"] {
		return this.options.errorCode;
	}

	public get embed(): [MessageEmbed, MessageActionRow] {
		let footer = "Need some help? Join the support server.";
		if (this.options.help.length > 0) {
			footer = `See /info ${this.options.errorCode} or join the support server for more.`;
		}
		return [
			new MessageEmbed({
				color: errorColor,
				author: { name: `Error: ${this.options.errorCode}`, iconURL: `https://cdn.discordapp.com/emojis/${container.emojis.warning}.png` },
				description: `${codeBlock("diff", `\n-${this.options.message}`)}${
					this.options.description ? `${codeBlock("fix", `\n${this.options.description}`)}` : ""
				}`,
				footer: { text: footer, iconURL: container.client.user?.displayAvatarURL() ?? "" }
			}),
			supportLinkRow
		];
	}

	public get infoEmbed(): [MessageEmbed, MessageActionRow] {
		const [embed, row] = this.embed;
		embed.setAuthor({ name: embed.author?.name ?? "" });
		embed.setColor(defaultColor);
		embed.setFooter(null);
		embed.description += this.options.help.length > 0 ? codeBlock("fix", this.options.help) : "";
		return [embed, row];
	}
}

// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export abstract class JailErrors {
	static [key: string]: DuckBotError | undefined;

	public static get [ERROR_CODE.IN_JAIL](): DuckBotError {
		return this.IN_JAIL;
	}

	public static get IN_JAIL(): DuckBotError {
		return new DuckBotError({
			message: "User is already in jail.",
			description: "You can update the reason or jail duration using this command. Or free the user using /free",
			help: "",
			errorCode: `J-${ERROR_CODE.IN_JAIL}`
		});
	}

	public static get [ERROR_CODE.IN_DB](): DuckBotError {
		return this.IN_DB;
	}

	public static get IN_DB(): DuckBotError {
		return new DuckBotError({
			message: "User has been manually jailed.",
			description: "User has a jail role but wasn't jailed using the /jail command. You must restore the roles before jailing.",
			help: `This error happens when the user has a jail role but the bot doesn't have any memory of having jailed the user (no roles saved)
Either remove the jail role manually and restore their old roles, or use the /free command to replace their roles with the corresponding member-role.
To prevent this error avoid giving a jail-role to users outside of using the /jail command.`,
			errorCode: `J-${ERROR_CODE.IN_DB}`
		});
	}

	public static get [ERROR_CODE.NO_MEMBER_ROLE](): DuckBotError {
		return this.NO_MEMBER_ROLE;
	}

	public static get NO_MEMBER_ROLE(): DuckBotError {
		return new DuckBotError({
			message: "User has no member role.",
			description: "You can add a member-jail role pair with /config jail",
			help: `This error happens when you try to jail someone but the bot doesn't know what jail role to use.
Either the user is missing a 'member' role or you have not configured all the member-jail role pairs you want.`,
			errorCode: `J-${ERROR_CODE.NO_MEMBER_ROLE}`
		});
	}

	public static get [ERROR_CODE.NO_CONFIG](): DuckBotError {
		return this.NO_CONFIG;
	}

	public static get NO_CONFIG(): DuckBotError {
		return new DuckBotError({
			message: "No jail roles are configured.",
			description: "You can add a member-jail role pair using /config jail",
			help: `To use any jail related feature you must have at least one member-jail role pair configured.
The member role is used to know what jail role to give to the user.`,
			errorCode: `J-${ERROR_CODE.NO_CONFIG}`
		});
	}

	public static get [ERROR_CODE.INVALID_DURATION](): DuckBotError {
		return this.INVALID_DURATION;
	}

	public static get INVALID_DURATION(): DuckBotError {
		return new DuckBotError({
			message: "Invalid time format provided.",
			description: "Example of correct formats: '1d 8h 45m', or '8h 45m', or '45m'",
			help: "Durations must contain (at least) one or more numbers followed by a time identifier (d for days, h for hours, m for minutes)",
			errorCode: `J-${ERROR_CODE.INVALID_DURATION}`
		});
	}

	public static get [ERROR_CODE.MAX_DURATION](): DuckBotError {
		return this.MAX_DURATION;
	}

	public static get MAX_DURATION(): DuckBotError {
		return new DuckBotError({
			message: "Duration can not exceed $1.",
			description: "",
			help: "",
			errorCode: `J-${ERROR_CODE.MAX_DURATION}`
		});
	}

	public static get [ERROR_CODE.MIN_DURATION](): DuckBotError {
		return this.MIN_DURATION;
	}

	public static get MIN_DURATION(): DuckBotError {
		return new DuckBotError({
			message: "Duration must be a minimum of $1.",
			description: "",
			help: "",
			errorCode: `J-${ERROR_CODE.MIN_DURATION}`
		});
	}

	public static get [ERROR_CODE.NOT_IN_JAIL](): DuckBotError {
		return this.NOT_IN_JAIL;
	}

	public static get NOT_IN_JAIL(): DuckBotError {
		return new DuckBotError({
			message: "User is not in jail",
			description: "",
			help: "",
			errorCode: `J-${ERROR_CODE.NOT_IN_JAIL}`
		});
	}

	public static get [ERROR_CODE.NOT_MANAGEABLE](): DuckBotError {
		return this.NOT_MANAGEABLE;
	}

	public static get NOT_MANAGEABLE(): DuckBotError {
		return new DuckBotError({
			message: "Missing permissions.",
			description: "I can not modify this user's roles. Make sure my role is higher up in the role hierarchy.",
			help: "In your server settings make sure the bot's role is above all roles that you wish to be able to jail, including the jail role itself.",
			errorCode: `J-${ERROR_CODE.NOT_MANAGEABLE}`
		});
	}
}

// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export abstract class SignupErrors {
	static [key: string]: DuckBotError | undefined;

	public static get [ERROR_CODE.NOT_A_SIGNUP](): DuckBotError {
		return this.NOT_A_SIGNUP;
	}

	public static get NOT_A_SIGNUP(): DuckBotError {
		return new DuckBotError({
			message: "Not a signup.",
			description: "Context menu commands will only work on messages that contain a signup.",
			help: "",
			errorCode: `S-${ERROR_CODE.NOT_A_SIGNUP}`
		});
	}

	public static get [ERROR_CODE.SIGNUP_NOT_FOUND](): DuckBotError {
		return this.SIGNUP_NOT_FOUND;
	}

	public static get SIGNUP_NOT_FOUND(): DuckBotError {
		return new DuckBotError({
			message: "Signup not found.",
			description: "",
			help: "This occurs if the bot has no memory of a signup. E.g. if bot was kicked all previous signups are deleted from memory.",
			errorCode: `S-${ERROR_CODE.NOT_A_SIGNUP}`
		});
	}

	public static get [ERROR_CODE.INVALID_DATETIME](): DuckBotError {
		return this.INVALID_DATETIME;
	}

	public static get INVALID_DATETIME(): DuckBotError {
		return new DuckBotError({
			message: "Invalid datetime format.",
			description: "Example format: $1",
			help: "",
			errorCode: `S-${ERROR_CODE.INVALID_DATETIME}`
		});
	}

	public static get [ERROR_CODE.PAST_DATETIME](): DuckBotError {
		return this.PAST_DATETIME;
	}

	public static get PAST_DATETIME(): DuckBotError {
		return new DuckBotError({
			message: "Signup close date can not be in the past.",
			description: `The time be set using UTC
 e.g. 5 minutes from now: $1`,
			help: "",
			errorCode: `S-${ERROR_CODE.PAST_DATETIME}`
		});
	}

	public static get [ERROR_CODE.FORMAT_NOT_FOUND](): DuckBotError {
		return this.FORMAT_NOT_FOUND;
	}

	public static get FORMAT_NOT_FOUND(): DuckBotError {
		return new DuckBotError({
			message: "Couldn't find a format named: $1",
			description: "",
			help: "The autocomplete suggestions will show you the formats saved from /signup format create",
			errorCode: `S-${ERROR_CODE.FORMAT_NOT_FOUND}`
		});
	}

	public static get [ERROR_CODE.INVALID_SIGNUP_IMPORT](): DuckBotError {
		return this.INVALID_SIGNUP_IMPORT;
	}

	public static get INVALID_SIGNUP_IMPORT(): DuckBotError {
		return new DuckBotError({
			message: "Invalid signup import",
			description: "$1",
			help: "This error should not normally occur, if you see this consider reporting it in the support server.",
			errorCode: `S-${ERROR_CODE.INVALID_SIGNUP_IMPORT}`
		});
	}

	public static get [ERROR_CODE.POSTING_SIGNUP](): DuckBotError {
		return this.POSTING_SIGNUP;
	}

	public static get POSTING_SIGNUP(): DuckBotError {
		return new DuckBotError({
			message: "Could not create signup.",
			description: "Something went wrong trying to post the signup here.",
			help: "Check if I have sufficient permissions, or use /signup format edit to check for deleted emotes/roles",
			errorCode: `S-${ERROR_CODE.POSTING_SIGNUP}`
		});
	}
}

// eslint-disable-next-line @typescript-eslint/no-extraneous-class
export abstract class GeneralErrors {
	static [key: string]: DuckBotError | undefined;

	public static get [ERROR_CODE.BOT_MISSING_PERM](): DuckBotError {
		return this.BOT_MISSING_PERM;
	}

	public static get BOT_MISSING_PERM(): DuckBotError {
		return new DuckBotError({
			message: "Insufficient permissions.",
			description: "I don't have sufficient permissions to run this command.",
			help: `Check that no channel overrides are affecting my permissions. 
You can also click on the invite link again (no need to kick me) to restore the default permissions.
Common missing permissions are send/read messages in this channel.`,
			errorCode: `G-${ERROR_CODE.BOT_MISSING_PERM}`
		});
	}

	public static get [ERROR_CODE.FEEDBACK_SUBMIT](): DuckBotError {
		return this.FEEDBACK_SUBMIT;
	}

	public static get FEEDBACK_SUBMIT(): DuckBotError {
		return new DuckBotError({
			message: "Something went wrong submitting feedback.",
			description: "Consider joining the support server to submit it there.",
			help: "This shouldn't normally happen, this error indicates when the bot couldn't forward your feedback.",
			errorCode: `G-${ERROR_CODE.FEEDBACK_SUBMIT}`
		});
	}

	public static get [ERROR_CODE.RESTRICTED_COMMAND](): DuckBotError {
		return this.RESTRICTED_COMMAND;
	}

	public static get RESTRICTED_COMMAND(): DuckBotError {
		return new DuckBotError({
			message: "You do not have permissions to run this command.",
			description: "",
			help: "This command has additional permissions required that ignore the permissions set in the server settings",
			errorCode: `G-${ERROR_CODE.RESTRICTED_COMMAND}`
		});
	}

	public static get [ERROR_CODE.ERROR_NOT_FOUND](): DuckBotError {
		return this.ERROR_NOT_FOUND;
	}

	public static get ERROR_NOT_FOUND(): DuckBotError {
		return new DuckBotError({
			message: "Error code $1 not found.",
			description: `Try /info G-${ERROR_CODE.ERROR_NOT_FOUND} for an example.`,
			help: "Example extra 'useful' information about an error code.",
			errorCode: `G-${ERROR_CODE.ERROR_NOT_FOUND}`
		});
	}

	public static get [ERROR_CODE.UNKNOWN_ERROR_NOT_FOUND](): DuckBotError {
		return this.UNKNOWN_ERROR_NOT_FOUND;
	}

	public static get UNKNOWN_ERROR_NOT_FOUND(): DuckBotError {
		return new DuckBotError({
			message: "Error code $1 not found.",
			description: `Try /info ${container.client.user?.id} for an example.`,
			help: "This error occurs when you try to get help on an error code that does not exist.",
			errorCode: `G-${ERROR_CODE.UNKNOWN_ERROR_NOT_FOUND}`
		});
	}
}

export async function reportError(error: unknown, interaction: Interaction): Promise<boolean> {
	const replyType = interaction.isRepliable() && (interaction.replied || interaction.deferred) ? "followUp" : "reply";
	try {
		// If missing permissions to run command
		if (error instanceof DiscordAPIError && error.code === 50013) {
			if (interaction.isRepliable()) {
				const [embed, row] = GeneralErrors.BOT_MISSING_PERM.embed;
				await interaction[replyType]({ embeds: [embed], ephemeral: true, components: [row] });
			}
			return false;
		} else if (error instanceof DuckBotError) {
			const [embed, row] = error.embed;
			if (interaction.isRepliable()) {
				await interaction[replyType]({ embeds: [embed], ephemeral: true, components: [row] });
			}
			return false;
		} else if (error instanceof Error) {
			if (interaction.isRepliable()) {
				const reportEmbed = new MessageEmbed({
					color: errorColor,
					timestamp: new Date(),
					description: `\`\`\`css\n${error.name}\n\`\`\`\`\`\`fix\n${error.message}\n\`\`\`\`\`\`ml\n${
						(error.stack ?? "\u200b") || "\u200b"
					}\n\`\`\``
				});
				if (container.env.isDev) {
					await interaction[replyType]({ embeds: [reportEmbed], ephemeral: true });
					return true;
				}
				const error_json = JSON.stringify({ name: error.name, message: error.message, stack: error.stack });
				const { error_id, message_id, fixed, info } =
					(
						await container.db<({ error_id: string; message_id: string; fixed: boolean; info: boolean } | undefined)[]>`
						SELECT error_id, report_message_id as message_id, fixed, want_info as info
						FROM errors WHERE error_json::text=${container.db.json(error_json)}::text`
					)[0] || {};
				const fields = [
					{ name: "User ID", value: interaction.user.id, inline: true },
					{ name: "Guild ID", value: interaction.guild?.id ?? "\u200b", inline: true },
					{ name: "Channel ID", value: interaction.channelId ?? "\u200b", inline: true }
				];
				const new_id = error_id ?? DiscordSnowflake.generate().toString();
				container.client.channels
					.fetch(container.env.errorChannelID)
					.then(async (channel) => {
						if (!channel || !channel.isText()) return;
						if (error_id === undefined || fixed === true) {
							if (fixed === true && message_id !== undefined) {
								const link = (await channel.messages.fetch(message_id)).url;
								reportEmbed.addField("\u200b", `[**Previous Occurrence**](${link})`, false);
							}
							reportEmbed.addFields(fields);
							reportEmbed.setFooter({ text: `id: ${new_id}` });
							const message = await channel.send({
								embeds: [reportEmbed],
								components: [
									new MessageActionRow({
										components: [
											new MessageButton({ customId: `error;fix;true;${new_id}`, label: "Toggle Fixed", style: "SUCCESS" }),
											new MessageButton({
												customId: `error;info;false;${new_id}`,
												label: "Toggle Reporting",
												style: "PRIMARY"
											}),
											new MessageButton({ customId: `error;addMsg;dev;${new_id}`, label: "Set Comment", style: "PRIMARY" })
										]
									})
								]
							});
							await message
								.startThread({
									autoArchiveDuration: "MAX",
									name: `${error.name} [${new_id}]\\>`,
									reason: `Thread for error ${new_id}`
								})
								.then(async (thread) => {
									await thread.members.add(container.env.ownerID);
								})
								.catch(() => {});
							if (error_id === undefined) {
								await container.db`
								INSERT INTO errors VALUES(${new_id}, ${container.db.json(error_json)}, ${message.id})`;
							} else {
								await container.db`
								UPDATE errors SET last_occurred=${new Date()}, report_message_id=${message.id}, fixed=false, want_info=true, comment=null
								WHERE error_id = ${new_id}`;
							}
						} else if (message_id !== undefined) {
							const message = await channel.messages.fetch(message_id);
							reportEmbed.setDescription("");
							reportEmbed.addFields(fields);
							reportEmbed.setFooter({ text: "New Occurrence" });
							if (message.hasThread && info === true) {
								await message.thread?.send({ embeds: [reportEmbed] });
							}
							await container.db`
							UPDATE errors SET last_occurred=${new Date()} WHERE error_id=${new_id}`;
						}
					})
					.catch((e) => {
						container.client.logger.error("Error occurred while trying to report error: ", e);
					});
				const embed = new MessageEmbed({
					color: errorColor,
					author: {
						name: "Unknown Error",
						iconURL: `https://cdn.discordapp.com/emojis/${container.emojis.warning}.png`
					},
					description: `**\`Reference Code:\`** **\`${new_id}\`**
\`\`\`fix\nThis is either a bug or an unlabeled error and has been reported for fixing.\n\`\`\``,
					footer: { text: "For more info see /info or join the support server.", iconURL: container.client.user?.displayAvatarURL() ?? "" }
				});
				const row = new MessageActionRow(supportLinkRow).addComponents(
					new MessageButton({ customId: `error;addMsg;user;${new_id}`, label: "Add Comment", emoji: "🗒️", style: "PRIMARY" })
				);
				await interaction[replyType]({ ephemeral: true, embeds: [embed], components: [row] });
			}
		}
	} catch (e) {
		container.client.logger.error("An error occurred following up on an error:  ", e);
	}
	return true;
}
