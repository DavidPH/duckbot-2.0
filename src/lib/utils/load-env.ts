export interface bot_settings {
	ownerID: string;
	duckbotServerID: string;
	duckbotServerInvite: string;
	botInviteLink: string;
	latestChannelID: string;
	feedbackChannelID: string;
	errorChannelID: string;
	isDev: boolean;
}

export function getEnv(): bot_settings {
	const settings: bot_settings = {
		ownerID: process.env.OWNER_ID ?? "",
		duckbotServerID: process.env.DUCKBOT_GUILD ?? "",
		duckbotServerInvite: process.env.DUCKBOT_GUILD_LINK ?? "",
		botInviteLink: process.env.BOT_INVITE_LINK ?? "",
		latestChannelID: process.env.LATEST_CHANNEL ?? "",
		feedbackChannelID: process.env.FEEDBACK_CHANNEL ?? "",
		errorChannelID: process.env.ERROR_CHANNEL ?? "",
		isDev: process.env.NODE_ENV === "dev"
	};
	for (const [key, val] of Object.entries(settings)) {
		if (val === "") {
			throw new Error(`Couldn't load env ${key}`);
		}
	}
	return settings;
}
