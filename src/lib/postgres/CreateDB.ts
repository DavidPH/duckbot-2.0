import { container } from "@sapphire/pieces";

export default async function createDB() {
	const { db } = container;
	await db.begin(async (db) => {
		await db`CREATE TABLE IF NOT EXISTS "servers"
                (
                    server_id bigint NOT NULL,
                    server_name text COLLATE pg_catalog."default" NOT NULL,
                    server_prefix text[],
                    PRIMARY KEY (server_id)
                );`;

		await db`CREATE TABLE IF NOT EXISTS "beta_opt_in"
				(
					server_id bigint NOT NULL,
					PRIMARY KEY (server_id),
					FOREIGN KEY (server_id)
						REFERENCES "servers" (server_id)
						ON UPDATE CASCADE
						ON DELETE CASCADE
				);`;

		await db`CREATE TABLE IF NOT EXISTS "jail_settings"
		        (
		            server_id bigint NOT NULL,
		            jail_message text NULL DEFAULT 'You have been jailed in {server}\nDuration: {time_left}',
		            jail_roles bigint[] NOT NULL DEFAULT '{}'::bigint[],
					log_channel bigint,
					ephemeral boolean NOT NULL DEFAULT FALSE,
					replace boolean NOT NULL DEFAULT TRUE,
					timeout boolean NOT NULL DEFAULT FALSE,
					enforce boolean NOT NULL DEFAULT FALSE,
		            PRIMARY KEY (server_id),
		            FOREIGN KEY (server_id)
		                REFERENCES "servers" (server_id)
		                ON UPDATE CASCADE
		                ON DELETE CASCADE
		        );`;

		await db`CREATE TABLE IF NOT EXISTS "jail"
		        (
		            server_id bigint NOT NULL,
		            member_id bigint NOT NULL,
		            jailed_by bigint NOT NULL,
		            role_list bigint[] NOT NULL,
		            jailed_at timestamp with time zone NOT NULL,
		            release_at timestamp with time zone,
					reason text,
		            PRIMARY KEY (server_id, member_id),
		            FOREIGN KEY (server_id)
		                REFERENCES "servers" (server_id)
		                ON UPDATE CASCADE
		                ON DELETE CASCADE
		        );`;

		await db`CREATE TABLE IF NOT EXISTS "jail_history"
		        (
		            server_id bigint NOT NULL,
		            member_id bigint NOT NULL,
		            jailed_at timestamp with time zone NOT NULL,
		            released_at timestamp with time zone,
		            jailed_by bigint NOT NULL,
		            released_by bigint,
		            reason text,
		            PRIMARY KEY (server_id, member_id, jailed_at),
		            FOREIGN KEY (server_id)
		                REFERENCES public.servers (server_id)
		                ON UPDATE CASCADE
		                ON DELETE CASCADE
		        )`;

		await db`CREATE OR REPLACE FUNCTION jail_history_trigger_fnc()
			RETURNS trigger AS
			$$
			BEGIN
				    INSERT INTO "jail_history"
					VALUES(NEW."server_id", NEW."member_id", NEW."jailed_at", null, NEW."jailed_by", null, NEW."reason")
					ON CONFLICT (server_id, member_id, jailed_at) 
					DO UPDATE SET jailed_by = EXCLUDED.jailed_by, reason=EXCLUDED.reason;
				RETURN NEW;
			END;
			$$
			LANGUAGE 'plpgsql';`;

		await db`CREATE OR REPLACE TRIGGER history_insert_trigger
				AFTER INSERT OR UPDATE
				ON "jail"
				FOR EACH ROW
				EXECUTE PROCEDURE jail_history_trigger_fnc();
		`;

		// Adds a jail_settings row whenever a server is added
		await db`CREATE OR REPLACE FUNCTION jail_settings_trigger_fnc()
			RETURNS trigger AS
			$$
			BEGIN
				    INSERT INTO "jail_settings"
					         VALUES(NEW."server_id");
				RETURN NEW;
			END;
			$$
			LANGUAGE 'plpgsql';`;

		await db`CREATE OR REPLACE TRIGGER server_insert_trigger
				AFTER INSERT
				ON "servers"
				FOR EACH ROW
				EXECUTE PROCEDURE jail_settings_trigger_fnc();
		`;

		await db`CREATE TABLE IF NOT EXISTS "signup_formats"
		(
			server_id bigint NOT NULL,
			format_name text NOT NULL,
			format json NOT NULL,
			PRIMARY KEY (server_id, format_name),
			FOREIGN KEY (server_id)
				REFERENCES "servers" (server_id)
				ON UPDATE CASCADE
				ON DELETE CASCADE
		);`;

		await db`
		CREATE TABLE IF NOT EXISTS "signups"
		(
			signup_id int UNIQUE GENERATED ALWAYS AS IDENTITY,
			server_id bigint NOT NULL,
			signup_json json NOT NULL,
			signup_expire timestamp with time zone,
			PRIMARY KEY (server_id, signup_id),
			FOREIGN KEY (server_id)
				REFERENCES "servers" (server_id)
				ON UPDATE CASCADE
				ON DELETE CASCADE
		);`;

		await db`
		CREATE TABLE IF NOT EXISTS "signup_messages"
		(
			signup_id int NOT NULL,
			server_id bigint NOT NULL,
			message_id bigint NOT NULL,
			message_index int NOT NULL,
			PRIMARY KEY (server_id, message_id),
			FOREIGN KEY (server_id)
				REFERENCES "servers" (server_id)
				ON UPDATE CASCADE
				ON DELETE CASCADE,
			FOREIGN KEY (signup_id)
				REFERENCES "signups" (signup_id)
				ON UPDATE CASCADE
				ON DELETE CASCADE
		);`;

		await db`
		CREATE TABLE IF NOT EXISTS "errors"
		(
			error_id bigint UNIQUE NOT NULL,
			error_json json NOT NULL,
			report_message_id bigint NOT NULL,
			last_occurred timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
			want_info boolean NOT NULL DEFAULT true,
			fixed boolean NOT NULL DEFAULT false,
			comment text,
			PRIMARY KEY(error_id)
		);`;

		await db`
		CREATE TABLE IF NOT EXISTS "ignore_list"
		(
			snowflake_id bigint NOT NULL,
			banned_on timestamp with time zone NOT NULL DEFAULT CURRENT_TIMESTAMP,
			PRIMARY KEY(snowflake_id)
		);`;
	});
}
