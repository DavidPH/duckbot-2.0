import { AllFlowsPrecondition, PieceContext } from "@sapphire/framework";
import type { CommandInteraction, ContextMenuInteraction, Guild } from "discord.js";
export class inBetaPrecondition extends AllFlowsPrecondition {
	constructor(context: PieceContext) {
		super(context, { position: 0, enabled: false });
	}

	public override async messageRun() {
		return this.ok();
	}

	public override async chatInputRun(interaction: CommandInteraction) {
		if (interaction.commandName !== "feedback" && interaction.commandName !== "info" && interaction.commandName !== "latest") {
			const { guild } = interaction;
			return this.checkIfInBeta(guild);
		}
		return this.ok();
	}

	public override async contextMenuRun(interaction: ContextMenuInteraction) {
		const { guild } = interaction;
		return this.checkIfInBeta(guild);
	}

	private async checkIfInBeta(guild: Guild | null) {
		if (!guild) return this.error();
		// TODO remove beta related things
		const [{ exists }] = await this.container.db<{ exists: boolean }[]>`
		SELECT EXISTS(SELECT 1 from beta_opt_in WHERE server_id = ${guild.id}) AS "exists" `;
		return exists ? this.ok() : this.error({ message: "beta" });
	}
}

declare module "@sapphire/framework" {
	interface Preconditions {
		inBeta: never;
	}
}
