import { AllFlowsPrecondition } from "@sapphire/framework";
import type { CommandInteraction, ContextMenuInteraction, Message } from "discord.js";
export class NotIgnoredPrecondition extends AllFlowsPrecondition {
	public override async messageRun(message: Message) {
		return this.checkIfIgnored(message.author.id);
	}

	public override async chatInputRun(interaction: CommandInteraction) {
		return this.checkIfIgnored(interaction.user.id);
	}

	public override async contextMenuRun(interaction: ContextMenuInteraction) {
		return this.checkIfIgnored(interaction.user.id);
	}

	private async checkIfIgnored(userId: string) {
		const [{ exists }] = await this.container.db<{ exists: boolean }[]>`SELECT exists(select 1 from ignore_list WHERE snowflake_id = ${userId})`;
		return exists ? this.error({ message: "You are in the ignore list." }) : this.ok();
	}
}

declare module "@sapphire/framework" {
	interface Preconditions {
		NotIgnored: never;
	}
}
