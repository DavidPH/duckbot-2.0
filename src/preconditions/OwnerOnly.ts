import { AllFlowsPrecondition } from "@sapphire/framework";
import type { CommandInteraction, ContextMenuInteraction, Message } from "discord.js";
export class OwnerOnlyPrecondition extends AllFlowsPrecondition {
	public override async messageRun(message: Message) {
		return this.checkOwner(message.author.id);
	}

	public override async chatInputRun(interaction: CommandInteraction) {
		return this.checkOwner(interaction.user.id);
	}

	public override async contextMenuRun(interaction: ContextMenuInteraction) {
		return this.checkOwner(interaction.user.id);
	}

	private async checkOwner(userId: string) {
		return this.container.env.ownerID === userId ? this.ok() : this.error({ message: "Only the bot owner can use this command!" });
	}
}

declare module "@sapphire/framework" {
	interface Preconditions {
		OwnerOnly: never;
	}
}
