#!/bin/sh

# Helper scripts for docker-compose

ProgName=$(basename $0)
  
sub_help(){
    echo "Usage: $ProgName <subcommand> [options]\n"
    echo "Subcommands:"
    echo "    dev      docker-compose in development mode."
    echo "    prod     docker-compose in production mode."   
    echo "    logs     Logs for the bot container."
    echo "    reload   Rebuilds bot and re-runs it, without touching other containers."
}
  
sub_reload(){
    docker-compose build bot
    docker-compose up --no-deps -d bot
    docker image prune -f
}
  
sub_dev(){
    docker-compose -f docker-compose.yml -f docker-compose.dev.yml $@ 
}

sub_prod(){
    docker-compose $@
}

sub_logs() {
    docker logs duckbot $@
}
  
subcommand=$1
case $subcommand in
    "" | "-h" | "--help")
        sub_help
        ;;
    *)
        shift
        sub_${subcommand} $@
        if [ $? = 127 ]; then
            echo "Error: '$subcommand' is not a command." >&2
            echo "      Run '$ProgName --help' for a list of commands." >&2
            exit 1
        fi
        ;;
esac