# Duckbot-v2

[![Invite link](https://img.shields.io/badge/-invite%20bot-555?style=for-the-badge&logo=discord&logoColor=white&color=%237289DA)](https://discord.com/oauth2/authorize?client_id=592492103079297024&permissions=1634571119808&scope=bot+applications.commands) &nbsp; &nbsp;
[![Support Server](https://img.shields.io/discord/299906113833533442?color=%237289DA&label=SUPPORT%20SERVER&logo=discord&logoColor=white&style=for-the-badge)](https://discord.com/api/oauth2/authorize?client_id=592492103079297024&permissions=277361454272&scope=bot+applications.commands)

<img align="right" src="https://i.imgur.com/pudDCOz.png" width="20%">


[DuckBot](https://gitlab.com/DavidPH/discord-bot) re-written in TypeScript implementing slash commands and other discord interactions, plus additional new features. 

<hr>

## Features

**Key features:**
  - **Jail roles:**
    - Replaces a user's roles with a predetermined jail role. (e.g. member with role X in their roles will get them all removed and replaced with role Y). This can be used to easily mute members / or temporarily restrict their access.  

    - Optionally enforce jail roles if a jailed user leaves and rejoins.

    - Bot remembers the user's old roles and will restore them automatically (if chosen) or on calling the `/free` command.  
    - You can set any amount of jail roles, and their priority. The bot will pick the appropriate jail role for the user and apply it to them.  
    - Optional logs, and `/history` command.



  - **Custom signups:**

     - A signup consists of one or more messages which users can press a button to get their name added to it. DuckBot offers an easy to use interactive signup creator.

     - Can limit the amount of people who can register to an event, or make the event require the user to have a role, or set a date for when the signup will expire.
     - Create signups with multiple messages and sections. Every section can have individual requirements / titles set. Bot will make sure there are no duplicate users registered to the same event.
     - Assign your custom signup a name, for easier future usage. Ability to export signup formats to a short string for easy sharing.
     - Don't want to worry about customizing a signup? The quick post command lets you post one that is applicable in most cases!

<details><summary>Screenshots</summary>
<details><summary>Jail</summary>
<img src="https://i.imgur.com/Z8noNkV.png">
<img src="https://i.imgur.com/9amQ5Iz.png">
</details>
<details><summary>Signup</summary>
<img src="https://i.imgur.com/EkOlpy1.png">
<img src="https://i.imgur.com/TyJmCDe.png">
<img src="https://i.imgur.com/EvufSeD.png">
<img src="https://i.imgur.com/j971O8x.png">

</details>
</details>
<hr>

**Upcoming features:**
  - **Self-Assignable roles:**
    - Customizable button/select menu messages where users can self assign (configured) roles. Like old-fashioned react-roles, but better!
  
  - **Fun & Miscellaneous commands** 
    - Over 30 modular 'fun' commands. Disabled by default, moderators can enable/disable these individually at will.  
e.g. Connect4, Hangman, Trivia, Tic-Tac-Toe, 2048

  - **Auto-Role**
    - Add one or more roles when a user joins your server. A delay can be configured, or the roles can be given instantly.

<hr>

## How to run:

DuckBot is not (yet!) open source, and I would prefer if you did not run your own private instance of DuckBot.  
For a more stable experience, invite the bot to your server. For feature requests you can use the support server. DuckBot is always looking to improve and all feedback is appreciated!

That said if you insist here is how to run it.

**Requirements:**  
&nbsp; • &nbsp;[Docker](https://www.docker.com/)  
&nbsp; • &nbsp;[docker-compose](https://docs.docker.com/compose/)

<hr>

#### Production

Download the main `docker-compose.yml` file and the `.env` file: 
```bash
(mkdir ./DuckBot-v2 && cd DuckBot-v2 &&
curl -LO https://gitlab.com/DavidPH/duckbot-2.0/-/raw/main/.docker/docker-compose.yml && 
curl -o .env -L https://gitlab.com/DavidPH/duckbot-2.0/-/raw/main/.docker/.env.example)
``` 
This will use the latest stable image from Docker Hub, alternatively you can build your own DuckBot image by cloning from the dev branch and using the helper scripts under the .docker folder


Fill out the `.env` file and then run
```
docker-compose up -d
```
It's easy to integrate this into a CI/CD pipeline ([example](https://gitlab.com/DavidPH/duckbot-2.0/-/blob/main/.gitlab-ci.yml))

<hr>

#### Development
**Requirements:**  
&nbsp; • &nbsp;[Node v16+](https://nodejs.org/en/download/) 

Clone the dev branch
```
git clone -b dev --single-branch https://gitlab.com/DavidPH/duckbot-2.0.git
```

Install the node modules
```
npm install
```

Go into the `.docker` folder and run 
```
docker-compose -f docker-compose.yml -f docker-compose.dev.yml
```
Or use one of the helper scripts
```
./duckbot.sh dev up -d
```


